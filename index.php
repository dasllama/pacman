<?php

// get the basic services
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;

// get the init
require_once 'init.php';

// kick off PACMAN with the first hook
Service\HookService::execHook( 'index' );