-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `content`;
CREATE TABLE `content` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `template` varchar(255) NOT NULL,
  `path` text NOT NULL,
  `parent` bigint(20) unsigned NOT NULL,
  `inserted` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `content` (`id`, `type`, `template`, `path`, `parent`, `inserted`, `updated`) VALUES
(1, 'page', 'user-login', '/login/',  0,  '2015-09-16 13:44:30',  '2015-09-17 09:12:52'),
(2, 'page', 'user-register',  '/register/', 0,  '2015-09-17 11:13:45',  '2015-09-17 09:13:45'),
(3, 'page', 'user-register-thanks', '/register-thanks/',  0,  '2015-09-22 21:20:31',  '2015-09-22 19:20:31'),
(4, 'page', 'user-register-confirm',  '/register-confirm/', 0,  '2015-09-22 21:21:33',  '2015-09-22 19:22:11'),
(5, 'page', 'user-register-complete', '/register-complete/',  0,  '2015-09-22 21:55:32',  '2015-09-22 19:55:32'),
(6, 'page', 'user-forgot-password', '/forgot-password/',  0,  '2015-09-22 21:57:32',  '2015-09-22 19:57:32'),
(7, 'page', 'user-forgot-password-thanks',  '/forgot-password-thanks/', 0,  '2015-09-22 22:15:12',  '2015-09-22 20:15:12'),
(8, 'page', 'user-reset-password',  '/reset-password/', 0,  '2015-09-22 22:18:05',  '2015-09-22 20:18:05'),
(9, 'page', 'user-reset-password-finished', '/reset-password-finished/',  0,  '2015-09-22 22:28:43',  '2015-09-22 20:28:43'),
(10,  'page', 'user-profile', '/profile/',  0,  '2015-09-22 22:32:30',  '2015-09-22 20:32:30'),
(11,  'page', 'user-profile-change-email',  '/profile-change-email/', 0,  '0000-00-00 00:00:00',  '0000-00-00 00:00:00'),
(12,  'page', '', '/hallo-welt/', 0,  '2016-08-08 16:16:22',  '2016-08-08 14:16:28');

DROP TABLE IF EXISTS `content_data`;
CREATE TABLE `content_data` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` bigint(20) unsigned NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `content_data` (`ID`, `content_id`, `key`, `value`) VALUES
(1, 1,  'title',  'Login'),
(2, 2,  'title',  'Register'),
(3, 3,  'title',  'Thanks for signup!'),
(4, 4,  'title',  'Confirm Registration'),
(5, 5,  'title',  'Registration Complete!'),
(6, 6,  'title',  'Forgot Password'),
(7, 7,  'title',  'Awaiting Confirmation'),
(8, 8,  'title',  'Reset Password'),
(9, 9,  'title',  'Password resetted!'),
(10,  10, 'title',  'Profile'),
(11,  11, 'title',  'Change E-Mail'),
(12,  12, 'title',  'Hallo Welt'),
(13,  12, 'content',  'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.\r\n\r\nLorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.\r\n\r\nLorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.');

DROP TABLE IF EXISTS `content_relation`;
CREATE TABLE `content_relation` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `content_left` bigint(20) unsigned NOT NULL,
  `content_right` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `session` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `registered` datetime NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id`, `email`, `login`, `pass`, `session`, `key`, `registered`, `status`) VALUES
(1, 'hallo@wpcoding.de',  'admin',  '$P$BPrQoS0/Je.dj9tWeS0St74/1LJz5m1', '', '', '2015-08-09 11:37:41',  'approved');

DROP TABLE IF EXISTS `user_data`;
CREATE TABLE `user_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_data_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user_data` (`id`, `user_id`, `key`, `value`) VALUES
(1, 1,  'role', 'administrator'),
(2, 1,  'capabilities', 'a:2:{i:0;s:4:\"read\";i:1;s:11:\"admin_login\";}');

-- 2016-08-08 14:58:02