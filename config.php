<?php
// IMPORTANT NOTE
// ==============
// There is an admin user. It got the following credentials
// admin:123456

// database
define( 'DB_NAME', 'pacman' );
define( 'DB_USER', 'root' );
define( 'DB_PASSWORD', '' );
define( 'DB_HOST', 'localhost' );
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );

// debug stuff
define( 'PM_DEBUG', TRUE );
define( 'PM_DEBUG_LOG', FALSE );
define( 'PM_SCRIPT_DEBUG', TRUE );
define( 'PM_DEBUG_DISPLAY', TRUE );

// keys
define( 'PM_AUTH_KEY',         'sD[+VO@$u0q:Ou]lN4ZyN+y.9_!c=M*ec7(@rg[P|[)Rj|RYb9^xq;a=VZx3E &K' );
define( 'PM_AUTH_SALT',        't/<##!>u#?sBxSpJ-9vYzvq&9qJhl?1O~}@4H]C^%J2?,z+bjQ(bRz3[6/[I*_fE' );
define( 'PM_SECURE_AUTH_KEY',  '|`WyOTuD`x,i44@M ]0p}[+m$d>;B5|n_7W]N},I=Vwy#J$v|q-lgKfZlz+cpLDB' );
define( 'PM_SECURE_AUTH_SALT', 'f=7>&R0bh`&p=0pg|l=7Rm;n++B0~2WT-;8io:<)S|#Q NckufwmEeO{ypub0?][' );
define( 'PM_LOGGED_IN_KEY',    ',g?.F9x[|BuDIk{U6IyC$=5,fUzkwH6zHKAH5h$7P:-P)5/*]Hy&iB:Lrw6a+#Vo' );
define( 'PM_LOGGED_IN_SALT',   'h2Z/r. MDg%X_?TOZ,J#!0Bvf|Sz.zz*h$d7Kqby_,(FXj:o~]F-=)*0&mHo~A >' );
define( 'PM_NONCE_KEY',        'x,6}-go|++y8LQo@{M|gYF2i4@UU-O9v$.K.#vepzi+eWn@16LS[umgXt~^F^{0U' );
define( 'PM_NONCE_SALT',       '9FLAY2l#p7-7NQ!=nK hr1jGUAg,fN*5VQVM+1g{v4>b<tZe1(ZPnK|2T}%hKBg&' );
define( 'PM_SECRET_KEY',       'ifewifwf083=)U=FIOE3003IH=§WHOFNE3i0h3gpslg.rsgöswehf49HH)9h3hhf' );
define( 'PM_RANDOM_SEED',      'oküßu0wfhiH=(§UWRPINKFLOZ=§)Pnjfepjwöt4-sgnowu24iobtehiojOHE=f' );

// set the absolute path for this system
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname(__FILE__) . '/' );

// define needed constants
define( 'PM_PATH', 'pacman/' );