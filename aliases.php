<?php
// load the packages
use PACMAN\Helper\Access as Access;
use PACMAN\Helper\Content as Content;
use PACMAN\Helper\Language as Language;
use PACMAN\Helper\Template as Template;
use PACMAN\Helper\User as User;

/**
 * @see	Helper\Access::currentUserCan()
 */
function currentUserCan( $sCapability )
{
	return Access::currentUserCan( $sCapability );
}

/**
 * @see	Helper\Access::userCan()
 */
function userCan( $iUserId, $sCapability )
{
	return Access::userCan( $iUserId, $sCapability );
}

/**
 * @see	Helper\Content::getTitle()
 */
function getTitle()
{
	return Content::getTitle();
}

/**
 * @see	Helper\Content::getContent()
 */
function getContent()
{
	return Content::getContent();
}

/**
 * @see	Helper\Content::getContentData()
 */
function getContentData( $sDataName )
{
	return Content::getContentData( $sDataName );
}

/**
 * @see	getPageTitle()
 */
function thePageTitle()
{
	echo getPageTitle();
}

/**
 * @see	Helper\Template::getPageTitle()
 */
function getPageTitle()
{
	return Template::getPageTitle();
}