<?php
namespace PACMAN\Templates;
use PACMAN\AdminService as AdminService;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;
?>
<!Doctype html>
<!--[if IE 7]><html class="no-js ie ie7"  lang="<?php echo str_replace( '_', '-', Helper\Language::getLocale() ); ?>"><![endif]-->
<!--[if IE 8]><html class="no-js ie ie8"  lang="<?php echo str_replace( '_', '-', Helper\Language::getLocale() ); ?>"><![endif]-->
<!--[if !IE]><!--><html class="no-js" lang="<?php echo str_replace( '_', '-', Helper\Language::getLocale() ); ?>"><!--<![endif]-->
<head profile="http://gmpg.org/xfn/11">
	<title>PACMAN - Admin</title>
	<meta name="application-name" content="Llama">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<?php AdminService\TemplateService::loadHead(); ?>
</head>
	<body>
	<?php #echo Helper\User::getLoginForm( array( 'redirectTo' => Helper\Template::getAdminUrl( 'login' ) ), 'admin' => TRUE ); ?>
	<?php AdminService\TemplateService::loadFoot(); ?>
	</body>
</html>