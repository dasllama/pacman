<?php

// get the basic services
use PACMAN\AdminService as AdminService;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;

// set the admin flag to true
define( 'PM_ADMIN', TRUE );

// get the init
require_once '../init.php';

// load the user system
$oAdminService = new AdminService\InitService();
$oAdminService->initAdminSystem();

// kick off PACMAN with the first hook
Service\HookService::execHook( 'admin' );