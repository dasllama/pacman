<?php

// load the config
set_include_path( dirname( __FILE__ ) );
require_once 'config.php';
require_once 'settings.php';

// define needed constants
define( 'PM_URL', ( $_SERVER[ 'SERVER_PORT' ] == 443 ? 'https://' : 'http://' ) . $_SERVER[ 'HTTP_HOST' ] . '/' . PM_PATH );
define( 'PM_ADMIN_URL', PM_URL . 'admin/' );

// set the debug stuff
if ( PM_DEBUG ) {
	error_reporting( E_ALL );

	if ( PM_DEBUG_DISPLAY )
		ini_set( 'display_errors', 1 );
	else if ( null !== PM_DEBUG_DISPLAY )
		ini_set( 'display_errors', 0 );

	if ( PM_DEBUG_LOG ) {
		ini_set( 'log_errors', 1 );
		ini_set( 'error_log', PM_LOG_DIR . 'debug.log' );
	}
} else {
	error_reporting( E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_ERROR | E_WARNING | E_PARSE | E_USER_ERROR | E_USER_WARNING | E_RECOVERABLE_ERROR );
}

// get the autoloader
require __DIR__  . '/src/Helper/SplClassLoader.php';
$oClassLoader = new \SplClassLoader('PACMAN', __DIR__ . '/src');
$oClassLoader->register();

// get the basic services
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;

// initialize the roles and capabilities
$oAccessService = Service\AccessService::getInstance();
$oAccessService->setRolesAndCapabilities();

// define the needed settings
define( 'PLUGIN_DIR', __DIR__ . '/src/PACMAN/Plugins/' );

// get the instance of the hook class
$oSessionInstance = Helper\Session::getInstance();
$oSessionInstance->start();

// load the language service
$oLanguageService = new Service\LanguageService();
$oLanguageService->initLanguageSystem();

// load the plugin service
$oPluginService = new Service\PluginService();
$oPluginService->loadPlugins();
$oPluginService->initPluginRegistrations();
$oPluginService->initPlugins();

// load the user system
$oUserService = new Service\UserService();
$oUserService->initUserSystem();

// load the template system because ... we need it
$oTemplateService = new Service\TemplateService();
$oTemplateService->initTemplateSystem();

// load the aliases
require_once __DIR__ . '/aliases.php';

// init loaded, fire last hook
Service\HookService::execHook( 'init' );