<?php
namespace PACMAN\Helper;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;

class Language
{
	/**
	 * Gets the current locale based upon
	 * the browser language
	 * 
	 * @return	string the current loclae
	 */
	public static function getLocale()
	{
		// get the locale based upon the given server language
		$sAcceptLanguage = $_SERVER[ 'HTTP_ACCEPT_LANGUAGE' ];
		$sLocales = str_replace( '-', '_', substr( $sAcceptLanguage, 0, 5 ) );
		$aLocales = explode( '_', $sLocales );
		$sLocale = $aLocales[ 0 ] . '_' . strtoupper( $aLocales[ 1 ] );

		// let other plugins change this locale
		$sLocale = Service\HookService::applyHooks( 'locale', $sLocale );
		return $sLocale;
	}

	/**
	 * Loads the language string and returns it
	 * 
	 * @param	string $sLanguageString the current identification for the string
	 * @param	string $sTextdomain the textdomain in which context the string should be loaded
	 * @return	string the translated language string
	 */
	public static function __( $sLanguageString, $sTextdomain = '' )
	{
		global $aLang;

		// set textdomain
		if ( empty( $sTextdomain ) )
		{
			$sTextdomain = 'standard';
		}

		// get the locale
		$sLocale = self::getLocale();

		// check if we got the string in the $aLang
		if ( isset( $aLang[ $sLocale ][ $sTextdomain ][ $sLanguageString ] ) )
		{
			$sString = $aLang[ $sLocale ][ $sTextdomain ][ $sLanguageString ];
		}
		else
		{
			$sString = isset( $aLang[ 'en_US' ][ $sTextdomain ][ $sLanguageString ] ) ? $aLang[ 'en_US' ][ $sTextdomain ][ $sLanguageString ] : $sLanguageString;
		}

		// some fallbacks
		if ( ! $sString )
		{
			$sString = $aLang[ 'en_US' ][ 'standard' ][ $sLanguageString ];
		}

		if ( ! $sString )
		{
			$sString = $sLanguageString;
		}

		return $sString;
	}

	/**
	 * Loads the language string and echos it
	 * 
	 * @param	string $sLanguageString the current string
	 * @param	string $sTextdomain the textdomain in which context the string should be loaded
	 * @return	void
	 */
	public static function _e( $sLanguageString, $sTextdomain = '' )
	{
		echo self::__( $sLanguageString, $sTextdomain );
	}
}