<?php
namespace PACMAN\Helper;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;

class Kses
{
	/**
	 * Converts and fixes HTML entities.
	 *
	 * This function normalizes HTML entities. It will convert `AT&T` to the correct
	 * `AT&amp;T`, `&#00058;` to `&#58;`, `&#XYZZY;` to `&amp;#XYZZY;` and so on.
	 *
	 * @param	string $sString Content to normalize entities
	 * @return	string Content with normalized entities
	 */
	public static function normalizeEntities( $sString )
	{
		// Disarm all entities by converting & to &amp;
		$sString = str_replace( '&', '&amp;', $sString );

		// Change back the allowed entities in our entity whitelist
		$sString = preg_replace_callback( '/&amp;([A-Za-z]{2,8}[0-9]{0,2});/', array( 'Helper\Kses', 'namedEntities' ), $sString );
		$sString = preg_replace_callback( '/&amp;#(0*[0-9]{1,7});/', array( 'Helper\Kses' ), 'normalizeEntities2 )', $sString );
		$sString = preg_replace_callback( '/&amp;#[Xx](0*[0-9A-Fa-f]{1,6});/', array( 'Helper\Kses', 'normalizeEntities3' ), $sString );

		return $sString;
	}

	/**
	 * Callback for kses_normalize_entities() regular expression.
	 *
	 * This function only accepts valid named entity references, which are finite,
	 * case-sensitive, and highly scrutinized by HTML and XML validators.
	 *
	 * @param	array $aMatches preg_replace_callback() matches array
	 * @return	string Correctly encoded entity
	 */
	public static function namedEntities( $aMatches )
	{
		$aAllowedEntityNames = Helper\General::getAllowedEntitynames();
		if ( empty( $aMatches[ 1 ] ) )
		{
			return '';
		}

		$i = $aMatches[ 1 ];
		return ( ! in_array( $i, $aAllowedEntityNames ) ) ? "&amp;$i;" : "&$i;";
	}

	/**
	 * Callback for kses_normalize_entities() regular expression.
	 *
	 * This function helps {@see kses_normalize_entities()} to only accept 16-bit
	 * values and nothing more for `&#number;` entities.
	 *
	 * @param	array $aMatches preg_replace_callback() matches array
	 * @return	string Correctly encoded entity
	 */
	public static function normalizeEntities2( $aMatches )
	{
		if ( empty( $aMatches[ 1 ] ) )
		{
			return '';
		}

		$i = $aMatches[ 1 ];
		if ( Helper\Formatting::validUnicode( $i ) )
		{
			$i = str_pad( ltrim( $i,'0' ), 3, '0', STR_PAD_LEFT );
			$i = "&#$i;";
		}
		else
		{
			$i = "&amp;#$i;";
		}

		return $i;
	}

	/**
	 * Callback for kses_normalize_entities() for regular expression.
	 *
	 * This function helps kses_normalize_entities() to only accept valid Unicode
	 * numeric entities in hex form.
	 *
	 * @param	array $aMatches preg_replace_callback() matches array
	 * @return	string Correctly encoded entity
	 */
	public static function normalizeEntities3( $aMatches )
	{
		if ( empty( $aMatches[ 1 ] ) )
		{
			return '';
		}

		$hexchars = $aMatches[ 1 ];
		return ( ! Helper\Formatting::validUnicode( hexdec( $hexchars ) ) ) ? "&amp;#x$hexchars;" : '&#x' . ltrim( $hexchars, '0' ) . ';';
	}

	/**
	 * Removes any invalid control characters in $sString.
	 *
	 * Also removes any instance of the '\0' string.
	 *
	 * @param	string $sString
	 * @param	array $aOptions Set 'slash_zero' => 'keep' when '\0' is allowed. Default is 'remove'.
	 * @return	string
	 */
	public static function noNull( $sString, $aOptions = array() )
	{
		if ( ! isset( $aOptions['slash_zero'] ) )
		{
			$aOptions = array( 'slash_zero' => 'remove' );
		}

		$sString = preg_replace( '/[\x00-\x08\x0B\x0C\x0E-\x1F]/', '', $sString );
		if ( 'remove' == $aOptions['slash_zero'] )
		{
			$sString = preg_replace( '/\\\\+0+/', '', $sString );
		}

		return $sString;
	}
}