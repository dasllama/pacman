<?php
namespace PACMAN\Helper;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;

class User
{
	/**
	 * Checks if the current user is logged in
	 * 
	 * @return	bool
	 */
	public static function isUserLoggedIn()
	{
		return isset( $_SESSION[ 'isUserLoggedIn' ] ) ? $_SESSION[ 'isUserLoggedIn' ] : FALSE;
	}

	/**
	 * Gets the current currentUserId
	 * 
	 * @return	int
	 */
	public static function getCurrentUserId()
	{
		return isset( $_SESSION[ 'currentUserId' ] ) ? $_SESSION[ 'currentUserId' ] : 0;
	}

	/**
	 * Gets a user object by the id or the user login
	 * 
	 * @param	mixed $mIdOrField
	 * @param	string $sField
	 * @return	object the selected user
	 */
	public static function getUser( $mIdOrField, $sField = 'id' ) {

		// check static cache
		static $aUserStack;
		if ( isset( $aUserStack[ $mIdOrField ] ) )
		{
			return $aUserStack[ $mIdOrField ];
		}

		// validate input
		if ( empty( $mIdOrField ) )
		{
			// TODO throw error
			return FALSE;
		}

		// everything seems to be fine, so we can build a query
		$oDb = Helper\General::getDatabase();

		// check if this is an id or string
		if ( $sField == 'id' )
		{
			$oUser = $oDb->getRow( $oDb->prepare( 'SELECT * FROM user WHERE `id` = %d', $mIdOrField ) );
		}
		else if ( $sField == 'login' )
		{
			$oUser = $oDb->getRow( $oDb->prepare( 'SELECT * FROM user WHERE `login` = %s', $mIdOrField ) );
		}
		else if ( $sField == 'email' )
		{
			$oUser = $oDb->getRow( $oDb->prepare( 'SELECT * FROM user WHERE `email` = %s', $mIdOrField ) );
		}
		else if ( $sField == 'key' )
		{
			$oUser = $oDb->getRow( $oDb->prepare( 'SELECT * FROM user WHERE `key` = %s', $mIdOrField ) );
		}

		// we don't have anything
		if ( empty( $oUser ) )
		{
			return FALSE;
		}

		// get the field data and append it to the user
		$oFieldData = $oDb->getResults( $oDb->prepare( 'SELECT * FROM user_data WHERE `user_id` = %d', $oUser->id ) );
		if ( ! empty( $oFieldData ) )
		{
			foreach ( $oFieldData as $oField )
			{

				$sFieldName = $oField->key;
				$oUser->{$sFieldName} = Helper\Formatting::maybeUnserialize( $oField->value );
			}
		}

		// set the stack
		$aUserStack[ $mIdOrField ] = $oUser;
		return $oUser;
	}

	/**
	 * Gets the logout URL
	 * 
	 * @return	string
	 */
	public static function getLogoutUrl()
	{
		return Helper\General::getPostUrl() . '?action=logout';
	}

	/**
	 * Gets the login form
	 * 
	 * @param	array $aArgs
	 * @return	void
	 */
	public static function getLoginForm( $aArgs = array() ) {

		// set default arguments
		$aDefaultArgs = array(
			'showLogout' => FALSE,
			'redirectTo' => '',
			'admin'      => FALSE
		);
		$aArgs = Helper\Formatting::parseArgs( $aArgs, $aDefaultArgs );

		// check if the user is logged in
		if ( $aArgs[ 'showLogout' ] && Helper\User::isUserLoggedIn() )
		{
			echo '<p>logged in <a href="' . Helper\General::getPostUrl() . '?action=logout">logout</a></p>';
			return;
		}

		// return if the user is logged in
		if ( Helper\User::isUserLoggedIn() )
		{
			return;
		}
		?>
		<form action="<?php echo Helper\General::getPostUrl(); ?>" method="post">
	
			<fieldset>
				<legend>Login</legend>
				<?php Service\HookService::execHook( 'loginMessages' ); ?>
				<p>
					<label for="login">Username:</label>
					<input type="text" name="userLogin">
				</p>
				<p>
					<label for="pass">Password:</label>
					<input type="password" name="userPass">
				</p>
				<p>
					<label for="submit">&nbsp;</label>
					<input type="submit" value="absenden"> <a href="<?php echo Helper\Template::getUrl( 'forgot-password' ); ?>">Passwort vergessen?</a>
				</p>
			</fieldset>

			<input type="hidden" name="action" value="login">
			<input type="hidden" name="referrer" value="<?php echo Helper\General::getCurrentUrl(); ?>">
			<?php if ( ! empty( $aArgs[ 'redirectTo' ] ) ) : ?>
				<input type="hidden" name="redirectTo" value="<?php echo $aArgs[ 'redirectTo' ] ?>">
			<?php endif; ?>
			<?php Helper\Security::nonceField( 'userLogin', '_NonceUserLogin' ); ?>
		</form>
		<?php
	}

	/**
	 * Gets the forgot password form
	 * 
	 * @param	array $args
	 * @return	void
	 */
	public static function getForgotPasswordForm()
	{
		// return if the user is logged in
		if ( self::isUserLoggedIn() )
		{
			return;
		}

		// check if we got field inputs
		$aFeldInputs = Helper\Session::get( 'fields', 'forgotPasswordInput' );
		$sEmail = '';
		if ( isset( $aFeldInputs[ 'email' ] ) )
		{
			$sEmail = $aFeldInputs[ 'email' ];
		}
		?>
		<form action="<?php echo Helper\General::getPostUrl(); ?>" method="post">
			<fieldset>
				<legend>Forgot Password</legend>
				<?php Service\HookService::execHook( 'loginMessages' ); ?>
				<p>
					<label for="email">E-Mail:</label>
					<input type="text" name="email" value="<?php echo $sEmail; ?>">
				</p>

				<p>
					<label for="submit">&nbsp;</label>
					<input type="submit" value="Submit">
				</p>

				<input type="hidden" name="action" value="forgotPassword">
				<input type="hidden" name="referrer" value="<?php echo Helper\General::getCurrentUrl(); ?>">
				<?php Helper\Security::nonceField( 'userForgotPassword', '_NonceUserForgotPassword' ); ?>
			</fieldset>
		</form>
		<?php
	}

	/**
	 * Gets the reset password form
	 * 
	 * @param	array $args
	 * @return	void
	 */
	public static function getResetPasswordForm()
	{
		// return if the user is logged in
		if ( self::isUserLoggedIn() )
		{
			return;
		}

		// check if we have a user with this key
		if ( ! isset( $_GET[ 'key' ] ) || empty( $_GET[ 'key' ] ) )
		{
			echo '<div class="note error"><p><strong>You are not allowed to do this!</strong></p></div>';
			return;
		}

		$oUser = Helper\User::getUser( $_GET[ 'key' ], 'key' );
		if ( empty( $oUser ) || $oUser === FALSE )
		{
			echo '<div class="note error"><p><strong>You are not allowed to do this!</strong></p></div>';
			return;
		}

		?>
		<form action="<?php echo Helper\General::getPostUrl(); ?>" method="post">
			<fieldset>
				<legend>Reset Password</legend>
				<?php Service\HookService::execHook( 'loginMessages' ); ?>

				<p>
					<label for="pass">Password:</label>
					<input type="password" name="pass">
				</p>
				<p>
					<label for="pass_1">Password (again):</label>
					<input type="password" name="pass_1">
				</p>

				<p>
					<label for="submit">&nbsp;</label>
					<input type="submit" value="absenden">
				</p>

				<input type="hidden" name="action" value="resetPassword">
				<input type="hidden" name="key" value="<?php echo $_GET[ 'key' ]; ?>">
				<input type="hidden" name="referrer" value="<?php echo Helper\General::getCurrentUrl(); ?>">
				<?php Helper\Security::nonceField( 'userResetPassword', '_NonceUserResetPassword' ); ?>
			</fieldset>
		</form>
		<?php
	}

	/**
	 * Gets and echos the register form
	 * 
	 * @return	void
	 */
	public static function getRegisterForm()
	{
		// return if the user is logged in
		if ( self::isUserLoggedIn() )
		{
			return;
		}

		// check if we got field inputs
		$aFieldInputs = Helper\Session::get( 'fields', 'register_field_input' );
		?>
		<form action="<?php echo Helper\General::getPostUrl(); ?>" method="post">

			<fieldset>
				<legend>Register</legend>

				<?php Service\HookService::execHook( 'registerMessages' ); ?>

				<p>
					<label for="login">Username:</label>
					<input type="text" name="login" value="<?php echo isset( $aFieldInputs[ 'login' ] ) ? $aFieldInputs[ 'login' ] : ''; ?>">
				</p>
				<p>
					<label for="email">E-Mail:</label>
					<input type="text" name="email" value="<?php echo isset( $aFieldInputs[ 'email' ] ) ? $aFieldInputs[ 'email' ] : ''; ?>">
				</p>
				<p>
					<label for="pass">Password:</label>
					<input type="password" name="pass">
				</p>
				<p>
					<label for="pass_1">Password (again):</label>
					<input type="password" name="pass_1">
				</p>
				<p>
					<label for="submit">&nbsp;</label>
					<input type="submit" value="absenden">
				</p>
			</fieldset>

			<input type="hidden" name="action" value="register">
			<input type="hidden" name="referrer" value="<?php echo Helper\General::getCurrentUrl(); ?>">
			<?php Helper\Security::nonceField( 'userRegister', '_NonceUserRegister' ); ?>
		</form>
		<?php
	}

	/**
	 * Gets and echos the confirmation form
	 * 
	 * @return	void
	 */
	public static function getRegisterConfirmationForm()
	{
		// return if the user is logged in
		if ( self::isUserLoggedIn() )
		{
			return;
		}

		// check if we got a key so we can use it as form input
		$sKey = '';
		if ( isset( $_GET[ 'key' ] ) && ! empty( $_GET[ 'key' ] ) )
		{
			$sKey = $_GET[ 'key' ];
		}

		// check if we got field inputs
		$aFieldInputs = Helper\Session::get( 'fields', 'register_confirm_input' );
		if ( isset( $aFieldInputs[ 'key' ] ) )
		{
			$sKey = $aFieldInputs[ 'key' ];
		}
		?>
		<p>To complete the registration enter your key below.</p>
		<form action="<?php echo Helper\General::getPostUrl(); ?>" method="post">
			<fieldset>
				<legend>Confirm Registration</legend>
				<?php Service\HookService::execHook( 'registerMessages' ); ?>

				<p>
					<label for="key">Key:</label>
					<input type="text" name="key" value="<?php echo $sKey; ?>">
				</p>

				<p>
					<label for="submit">&nbsp;</label>
					<input type="submit" value="absenden">
				</p>

				<input type="hidden" name="action" value="confirmRegister">
				<input type="hidden" name="referrer" value="<?php echo Helper\General::getCurrentUrl(); ?>">
				<?php Helper\Security::nonceField( 'userConfirmRegister', '_NonceUserConfirmRegister' ); ?>
			</fieldset>
		</form>
		<?php
	}

	/**
	 * Gets and echos the profile form
	 * 
	 * @return	void
	 */
	public static function getProfileForm() {


		// return if the user is not logged in
		if ( ! Helper\User::isUserLoggedIn() )
		{
			return;
		}

		// check if we got field inputs
		$oUser = Helper\User::getUser( Helper\User::getCurrentUserId() );

		// check if we got a changed email
		$oDb = Helper\General::getDatabase();
		$oChangedEmail = $oDb->getRow( $oDb->prepare( 'SELECT * FROM user_data WHERE `user_id` = %d AND `key` = %s', Helper\User::getCurrentUserId(), 'new_email' ) );
		?>
		<form action="<?php echo Helper\General::getPostUrl(); ?>" method="post">

			<fieldset>
				<legend>Profile</legend>
				<?php Service\HookService::execHook( 'profileMessages' ); ?>
				<p>
					<label for="login">Username:</label>
					<input type="text" disabled="disabled" name="login" value="<?php echo $oUser->login; ?>">
					<br /><span class="description">Username cannot be changed.</span>
				</p>
				<p>
					<label for="email">E-Mail:</label>
					<input type="text" name="email" value="<?php echo $oUser->email; ?>"><br />
					<span class="description">If you want to change your E-Mail, we send a confirmation mail to the new one.</span><br />
					<?php if ( ! empty( $oChangedEmail ) ) : ?>
						<div class="note">There is a change request for your email. It will be changed to <strong><?php echo $oChangedEmail->value; ?></strong> if you confirm our email.</div>
					<?php endif; ?>
				</p>
				<hr />
				<p>
					<label for="pass">Password:</label>
					<input type="password" name="pass">
				</p>
				<p>
					<label for="pass_1">Password (again):</label>
					<input type="password" name="pass_1">
					<br /><span class="description">If you don't want to change your password, just leave the passwords blank.</span>
				</p>
				<hr />
				<p>
					<label for="submit">&nbsp;</label>
					<input type="submit" value="absenden">
				</p>
			</fieldset>

			<input type="hidden" name="action" value="profile">
			<input type="hidden" name="referrer" value="<?php echo Helper\General::getCurrentUrl(); ?>">
			<?php Helper\Security::nonceField( 'userProfile', '_NonceUserProfile' ); ?>
		</form>
		<?php
	}

	/**
	 * Gets and echos the confirmation form for the change of the email
	 * 
	 * @return	void
	 */
	public static function getChangeEmailConfirmationForm()
	{
		// return if the user is not logged in
		if ( ! self::isUserLoggedIn() )
		{
			return;
		}

		// check if we got a key so we can use it as form input
		$sKey = '';
		if ( isset( $_GET[ 'key' ] ) && ! empty( $_GET[ 'key' ] ) )
		{
			$sKey = $_GET[ 'key' ];
		}

		// check if we got field inputs
		$aFieldInputs = Helper\Session::get( 'fields', 'emailChangeConfirmInput' );
		if ( isset( $aFieldInputs[ 'key' ] ) )
		{
			$sKey = $aFieldInputs[ 'key' ];
		}
		?>
		<p>To complete the change of your email enter your key below.</p>
		<form action="<?php echo Helper\General::getPostUrl(); ?>" method="post">
			<fieldset>
				<legend>Confirm E-Mail Change</legend>
				<?php Service\HookService::execHook( 'profileMessages' ); ?>

				<p>
					<label for="key">Key:</label>
					<input type="text" name="key" value="<?php echo $sKey; ?>">
				</p>

				<p>
					<label for="submit">&nbsp;</label>
					<input type="submit" value="absenden">
				</p>

				<input type="hidden" name="action" value="confirmEmailChange">
				<input type="hidden" name="referrer" value="<?php echo Helper\General::getCurrentUrl(); ?>">
				<?php Helper\Security::nonceField( 'userConfirmEmailChange', '_NonceUserConfirmEmailChange' ); ?>
			</fieldset>
		</form>
		<?php
	}
}