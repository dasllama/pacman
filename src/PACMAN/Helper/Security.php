<?php
namespace PACMAN\Helper;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;
use PACMAN\Library as Library;

class Security
{
	/**
	 * Verify that correct nonce was used with time limit.
	 *
	 * The user is given an amount of time to use the token, so therefore, since the
	 * UID and $mAction remain the same, the independent variable is the time.
	 *
	 * @param	string     $sNonce  Nonce that was used in the form to verify
	 * @param	string|int $mAction Should give context to what is taking place and be the same when nonce was created.
	 * @return	false|int False if the nonce is invalid, 1 if the nonce is valid and generated between
	 *                    0-12 hours ago, 2 if the nonce is valid and generated between 12-24 hours ago.
	 */
	public static function verifyNonce( $sNonce, $mAction = -1 )
	{
		$sNonce = (string) $sNonce;
		$iUserId = (int) Helper\User::getCurrentUserId();
		if ( ! $iUserId )
		{
			/**
			 * Filter whether the user who generated the nonce is logged out.
			 *
			 * @param	int    $iUserId    ID of the nonce-owning user.
			 * @param	string $mAction The nonce action.
			 */
			$iUserId = Service\HookService::applyHooks( 'nonce_user_logged_out', $iUserId, $mAction );
		}

		if ( empty( $sNonce ) )
		{
			return false;
		}

		$i = self::nonceTick();

		// Nonce generated 0-12 hours ago
		$sExpected = substr( self::getHash( $i . '|' . $mAction . '|' . $iUserId, 'nonce' ), -12, 10 );
		if ( hash_equals( $sExpected, $sNonce ) )
		{
			return 1;
		}

		// Nonce generated 12-24 hours ago
		$sExpected = substr( self::getHash( ( $i - 1 ) . '|' . $mAction . '|' . $iUserId, 'nonce' ), -12, 10 );
		if ( hash_equals( $sExpected, $sNonce ) )
		{
			return 2;
		}

		// Invalid nonce
		return false;
	}

	/**
	 * Creates a cryptographic token tied to a specific action, user, and window of time.
	 *
	 * @param	string|int $mAction Scalar value to add context to the nonce.
	 * @return	string The token.
	 */
	public static function createNonce( $mAction = -1 )
	{
		$iUserId = (int) Helper\User::getCurrentUserId();
		if ( ! $iUserId )
		{
			$iUserId = Service\HookService::applyHooks( 'createNonce', $iUserId, $mAction );
		}

		$i = self::nonceTick();

		return substr( self::getHash( $i . '|' . $mAction . '|' . $iUserId, 'nonce' ), -12, 10 );
	}

	/**
	 * Get the time-dependent variable for nonce creation.
	 *
	 * A nonce has a lifespan of two ticks. Nonces in their second tick may be
	 * updated, e.g. by autosave.
	 *
	 * @return	float Float value rounded up to the next highest integer.
	 */
	public static function nonceTick()
	{
		/**
		 * Filter the lifespan of nonces in seconds.
		 *
		 * @param int $lifespan Lifespan of nonces in seconds. Default 86,400 seconds, or one day.
		 */
		$iNonceLife = Service\HookService::applyHooks( 'NonceLife', 86400 );

		return ceil( time() / ( $iNonceLife / 2 ) );
	}

	/**
	 * Retrieve or display nonce hidden field for forms.
	 *
	 * The nonce field is used to validate that the contents of the form came from
	 * the location on the current site and not somewhere else. The nonce does not
	 * offer absolute protection, but should protect against most cases. It is very
	 * important to use nonce field in forms.
	 *
	 * The $mAction and $sName are optional, but if you want to have better security,
	 * it is strongly suggested to set those two parameters. It is easier to just
	 * call the function without any parameters, because validation of the nonce
	 * doesn't require any parameters, but since crackers know what the default is
	 * it won't be difficult for them to find a way around your nonce and cause
	 * damage.
	 *
	 * The input name will be whatever $sName value you gave. The input value will be
	 * the nonce creation value.
	 *
	 * @param	int|string $mAction  Optional. Action name. Default -1.
	 * @param	string     $sName    Optional. Nonce name. Default '_nonce'.
	 * @param	bool       $bReferer Optional. Whether to set the referer field for validation. Default true.
	 * @param	bool       $bEcho    Optional. Whether to display or return hidden form field. Default true.
	 * @return	string Nonce field HTML markup.
	 */
	public static function nonceField( $mAction = -1, $sName = "_Nonce", $bReferer = true , $bEcho = true )
	{
		$sName = Helper\Formatting::escAttr( $sName );
		$sNonceField = '<input type="hidden" id="' . $sName . '" name="' . $sName . '" value="' . self::createNonce( $mAction ) . '" />';

		// do we also need a referrer field?
		if ( $bReferer )
		{
			$sNonceField .= self::refererField( false );
		}

		// check if we need to echo this field
		if ( $bEcho )
		{
			echo $sNonceField;
		}

		return $sNonceField;
	}

	/**
	 * Retrieve or display referer hidden field for forms.
	 *
	 * The referer link is the current Request URI from the server super global. The
	 * input name is 'httpReferer', in case you wanted to check manually.
	 *
	 * @param	bool $bEcho Optional. Whether to echo or return the referer field. Default true.
	 * @return	string Referer field HTML markup.
	 */
	public static function refererField( $bEcho = true )
	{
		$referer_field = '<input type="hidden" name="httpReferer" value="'. Helper\Formatting::escAttr( Helper\Formatting::stripslashesDeep( $_SERVER[ 'REQUEST_URI' ] ) ) . '" />';

		if ( $bEcho )
		{
			echo $referer_field;
		}
		return $referer_field;
	}

	/**
	 * Get hash of given string.
	 *
	 * @param	string $sData Plain text to hash
	 * @return	string Hash of $sData
	 */
	public static function getHash( $sData, $sScheme = 'auth' )
	{
		$salt = self::getSalt( $sScheme );

		return self::hashHmac( 'md5', $sData, $salt );
	}

	/**
	 * Get salt to add to hashes.
	 *
	 * Salts are created using secret keys. Secret keys are located in two places:
	 * in the database and in the config.php file.
	 *
	 * The secret keys in config.php should be updated to strong, random keys to maximize
	 * security. Below is an example of how the secret key constants are defined.
	 *
	 *     define( 'PM_AUTH_KEY',         ' Xakm<o xQy rw4EMsLKM-?!T+,PFF})H4lzcW57AF0U@N@< >M%G4Yt>f`z]MON' );
	 *     define( 'PM_SECURE_AUTH_KEY',  'LzJ}op]mr|6+![P}Ak:uNdJCJZd>(Hx.-Mh#Tz)pCIU#uGEnfFz|f ;;eU%/U^O~' );
	 *     define( 'PM_LOGGED_IN_KEY',    '|i|Ux`9<p-h$aFf(qnT:sDO:D1P^wZ$$/Ra@miTJi9G;ddp_<q}6H1)o|a +&JCM' );
	 *     define( 'PM_NONCE_KEY',        '%:R{[P|,s.KuMltH5}cI;/k<Gx~j!f0I)m_sIyu+&NJZ)-iO>z7X>QYR0Z_XnZ@|' );
	 *     define( 'PM_AUTH_SALT',        'eZyT)-Naw]F8CwA*VaW#q*|.)g@o}||wf~@C-YSt}(dh_r6EbI#A,y|nU2{B#JBW' );
	 *     define( 'PM_SECURE_AUTH_SALT', '!=oLUTXh,QW=H `}`L|9/^4-3 STz},T(w}W<I`.JjPi)<Bmf1v,HpGe}T1:Xt7n' );
	 *     define( 'PM_LOGGED_IN_SALT',   '+XSqHc;@Q*K_b|Z?NC[3H!!EONbh.n<+=uKR:>*c(u`g~EJBf#8u#R{mUEZrozmm' );
	 *     define( 'PM_NONCE_SALT',       'h`GXHhD>SLWVfg1(1(N{;.V!MoE(SfbA_ksP@&`+AycHcAV$+?@3q+rxV{%^VyKT' );
	 *
	 * Salting passwords helps against tools which has stored hashed values of
	 * common dictionary strings. The added values makes it harder to crack.
	 *
	 * @param	string $sScheme Authentication scheme (auth, secure_auth, logged_in, nonce)
	 * @static	var array $aCachedSalts
	 * @static	var array $aDuplicatedKeys
	 * @return	string Salt value
	 */
	public static function getSalt( $sScheme = 'auth' )
	{
		static $aCachedSalts = array();
		if ( isset( $aCachedSalts[ $sScheme ] ) )
		{
			/**
			 * Filter the salt.
			 *
			 * @param	string $cached_salt Cached salt for the given scheme.
			 * @param	string $sScheme      Authentication scheme. Values include 'auth',
			 *                              'secure_auth', 'logged_in', and 'nonce'.
			 */
			return Service\HookService::applyHooks( 'salt', $aCachedSalts[ $sScheme ], $sScheme );
		}

		static $aDuplicatedKeys;
		if ( $aDuplicatedKeys === NULL )
		{
			$aDuplicatedKeys = array( 'put your unique phrase here' => TRUE );
			foreach ( array( 'PM_AUTH', 'PM_SECURE_AUTH', 'PM_LOGGED_IN', 'PM_NONCE', 'PM_SECRET' ) as $sFirst )
			{
				foreach ( array( 'KEY', 'SALT' ) as $sSecond )
				{
					if ( ! defined( "PM_{$sFirst}_{$sSecond}" ) )
					{
						continue;
					}

					$sValue = constant( "{$sFirst}_{$sSecond}" );
					$aDuplicatedKeys[ $sValue ] = isset( $aDuplicatedKeys[ $sValue ] );
				}
			}
		}

		$aValues = array(
			'key' => '',
			'salt' => ''
		);
		if ( defined( 'PM_SECRET_KEY' ) && PM_SECRET_KEY && empty( $aDuplicatedKeys[ PM_SECRET_KEY ] ) )
		{
			$aValues[ 'key' ] = PM_SECRET_KEY;
		}

		if ( 'auth' == $sScheme && defined( 'SECRET_SALT' ) && SECRET_SALT && empty( $aDuplicatedKeys[ SECRET_SALT ] ) )
		{
			$aValues[ 'salt' ] = SECRET_SALT;
		}

		if ( in_array( $sScheme, array( 'auth', 'secure_auth', 'logged_in', 'nonce' ) ) )
		{
			foreach ( array( 'key', 'salt' ) as $sType )
			{
				$const = strtoupper( "PM_{$sScheme}_{$sType}" );
				if ( defined( $const ) && constant( $const ) && empty( $aDuplicatedKeys[ constant( $const ) ] ) )
				{
					$aValues[ $sType ] = constant( $const );
				}
				elseif ( ! $aValues[ $sType ] )
				{
					$aValues[ $sType ] = self::getRandomPassword( 64, TRUE, TRUE );
				}
			}
		}
		else
		{
			if ( ! $aValues[ 'key' ] )
			{
				$aValues[ 'key' ] = self::getRandomPassword( 64, TRUE, TRUE );
			}
			$aValues[ 'salt' ] = self::hashHmac( 'md5', $sScheme, $aValues[ 'key' ] );
		}

		$aCachedSalts[ $sScheme ] = $aValues[ 'key' ] . $aValues[ 'salt' ];

		return Service\HookService::applyHooks( 'getSalt', $aCachedSalts[ $sScheme ], $sScheme );
	}

	/**
	 * Internal compat function to mimic hash_hmac().
	 *
	 * @param string $algo       Hash algorithm. Accepts 'md5' or 'sha1'.
	 * @param string $data       Data to be hashed.
	 * @param string $key        Secret key to use for generating the hash.
	 * @param bool   $raw_output Optional. Whether to output raw binary data (true),
	 *                           or lowercase hexits (false). Default false.
	 * @return string|false The hash in output determined by `$raw_output`. False if `$algo`
	 *                      is unknown or invalid.
	 */
	public static function hashHmac( $algo, $data, $key, $raw_output = FALSE )
	{
		$packs = array(
			'md5'  => 'H32',
			'sha1' => 'H40'
		);

		if ( ! isset( $packs[ $algo ] ) )
		{
			return FALSE;
		}

		$pack = $packs[ $algo ];

		if ( strlen( $key ) > 64 )
		{
			$key = pack( $pack, $algo( $key ) );
		}

		$key = str_pad( $key, 64, chr( 0 ) );

		$ipad = ( substr( $key, 0, 64 ) ^ str_repeat( chr( 0x36 ), 64 ) );
		$opad = ( substr( $key, 0, 64 ) ^ str_repeat( chr( 0x5C ), 64 ) );

		$hmac = $algo( $opad . pack( $pack, $algo( $ipad . $data ) ) );

		if ( $raw_output )
		{
			return pack( $pack, $hmac );
		}
		return $hmac;
	}

	/**
	 * Generates a random password drawn from the defined set of characters.
	 *
	 * @param	int  $iLength            Optional. The length of password to generate. Default 12.
	 * @param	bool $bSpecialChars      Optional. Whether to include standard special characters.
	 *                                   Default TRUE.
	 * @param	bool $bExtraSpecialChars Optional. Whether to include other special characters.
	 *                                   Used when generating secret keys and salts. Default false.
	 * @return	string The random password.
	 */
	public static function getRandomPassword( $iLength = 12, $bSpecialChars = TRUE, $bExtraSpecialChars = false )
	{
		$sChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		if ( $bSpecialChars )
		{
			$sChars .= '!@#$%^&*()';
		}
		if ( $bExtraSpecialChars )
		{
			$sChars .= '-_ []{}<>~`+=,.;:/?|';
		}

		$sPassword = '';
		for ( $i = 0; $i < $iLength; $i++ )
		{
			$sPassword .= substr( $sChars, self::getRand( 0, strlen( $sChars ) - 1 ), 1 );
		}

		/**
		 * Filter the randomly-generated password.
		 *
		 * @param	string $sPassword The generated password.
		 */
		return Service\HookService::applyHooks( 'getRandomPassword', $sPassword );
	}

	/**
	 * Generates a random number
	 *
	 * @param	int $iMin Lower limit for the generated number
	 * @param	int $iMax Upper limit for the generated number
	 * @return	int A random number between min and max
	 */
	public static function getRand( $iMin = 0, $iMax = 0 )
	{
		// Reset $sRndValue after 14 uses
		// 32(md5) + 40(sha1) + 40(sha1) / 8 = 14 random numbers from $sRndValue
		$sSeed = PM_RANDOM_SEED;
		$sRndValue = md5( uniqid( microtime() . mt_rand(), TRUE ) . $sSeed );
		$sRndValue .= sha1( $sRndValue );
		$sRndValue .= sha1( $sRndValue . $sSeed );
		$sSeed = md5( $sSeed . $sRndValue );

		// Take the first 8 digits for our value
		$sValue = substr( $sRndValue, 0, 8 );

		// Strip the first eight, leaving the remainder for the next call to getRand().
		$sRndValue = substr( $sRndValue, 8 );
		$sValue = abs( hexdec( $sValue ) );

		// Some misconfigured 32bit environments (Entropy PHP, for example) truncate integers larger than PHP_INT_MAX to PHP_INT_MAX rather than overflowing them to floats.
		$iMaxRandomNumber = 3000000000 === 2147483647 ? (float) "4294967295" : 4294967295; // 4294967295 = 0xffffffff

		// Reduce the value to be within the min - max range
		if ( $iMax != 0 )
		{
			$sValue = $iMin + ( $iMax - $iMin + 1 ) * $sValue / ( $iMaxRandomNumber + 1 );
		}

		return abs( intval( $sValue ) );
	}

	/**
	 * Create a hash (encrypt) of a plain text password.
	 *
	 * For integration with other applications, this function can be overwritten to
	 * instead use the other package password checking algorithm.
	 *
	 * @param	string $sPassword Plain text user password to hash
	 * @return	string The hash string of the password
	 */
	public static function getPasswordHash( $sPassword ) {
		// get the hasher
		$oHasher = self::getHasher();

		// hash the password
		return $oHasher->HashPassword( trim( $sPassword ) );
	}

	/**
	 * Checks the plaintext password against the encrypted Password.
	 *
	 * Maintains compatibility between old version and the new cookie authentication
	 * protocol using PHPass library. The $sHash parameter is the encrypted password
	 * and the function compares the plain text password when encrypted similarly
	 * against the already encrypted password to see if they match.
	 *
	 * For integration with other applications, this function can be overwritten to
	 * instead use the other package password checking algorithm.
	 *
	 * @param	string $sPassword Plaintext user's password
	 * @param	string $sHash     Hash of the user's password to check against.
	 * @return	bool False, if the $sPassword does not match the hashed password
	 */
	public static function checkPassword( $sPassword, $sHash, $iUserId = '' )
	{
		// get the hasher
		$oHasher = self::getHasher();

		// check the password
		$oCheck = $oHasher->CheckPassword( $sPassword, $sHash );
		return Service\HookService::applyHooks( 'checkPassword', $oCheck, $sPassword, $sHash, $iUserId );
	}

	/**
	 * Gets the object from the PHPass
	 * 
	 * @return	object the PHPass object
	 */
	public static function getHasher()
	{
		$hasher = new Library\PasswordHash( 8, TRUE );
		return $hasher;
	}
}