<?php
namespace PACMAN\Helper;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;

class Content
{
	/**
	 * Gets the title from the current content object and
	 * formats it
	 * 
	 * @return	string
	 */
	public static function getTitle()
	{
		$oContent = Service\ContentService::getCurrentContent();
		if ( ! isset( $oContent->title ) )
		{
			return '';
		}
		else
		{
			return Service\HookService::applyHooks( 'getTitle', $oContent->title );
		}
	}

	/**
	 * Gets the content from the current content object and
	 * formats it
	 * 
	 * @return	string
	 */
	public static function getContent()
	{
		$oContent = Service\ContentService::getCurrentContent();
		if ( ! isset( $oContent->content ) )
		{
			return '';
		}
		else
		{
			$sContent = Helper\Formatting::formatContent( $oContent->content );
			$sContent = str_replace( ']]>', ']]&gt;', $sContent );
			return Service\HookService::applyHooks( 'getContent', $sContent, $oContent );
		}
	}

	/**
	 * Gets the data from the current content element
	 * 
	 * @param	string $sDataName
	 * @return	mixed
	 */
	public static function getContentData( $sDataName )
	{
		$oContent = Service\ContentService::getCurrentContent();
		if ( ! isset( $oContent->{$sDataName} ) )
		{
			return FALSE;
		}
		else
		{
			return Service\HookService::applyHooks( 'getContentData', $oContent->{$sDataName}, $oContent );
		}
	}
}