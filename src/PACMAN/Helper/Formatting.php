<?php
namespace PACMAN\Helper;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;

class Formatting
{
	/**
	 * Unserialize value only if it was serialized.
	 *
	 * @param	string $sOriginal Maybe unserialized original, if is needed.
	 * @return	mixed Unserialized data can be any type.
	 */
	public static function maybeUnserialize( $sOriginal )
	{
		// don't attempt to unserialize data that wasn't serialized going in
		if ( self::isSerialized( $sOriginal ) )
		{
			return @unserialize( $sOriginal );
		}
		return $sOriginal;
	}

	/**
	 * Check value to find if it was serialized.
	 *
	 * If $sData is not an string, then returned value will always be false.
	 * Serialized sData is always a string.
	 *
	 * @param	string $sData   Value to check to see if was serialized.
	 * @param	bool   $bStrict Optional. Whether to be strict about the end of the string. Default true.
	 * @return	bool False if not serialized and true if it was.
	 */
	public static function isSerialized( $sData, $bStrict = TRUE )
	{
		// if it isn't a string, it isn't serialized.
		if ( ! is_string( $sData ) )
		{
			return FALSE;
		}

		$sData = trim( $sData );
	 	if ( 'N;' == $sData )
	 	{
			return TRUE;
		}
		if ( strlen( $sData ) < 4 )
		{
			return FALSE;
		}
		if ( ':' !== $sData[1] )
		{
			return FALSE;
		}
		if ( $bStrict )
		{
			$sLastc = substr( $sData, -1 );
			if ( ';' !== $sLastc && '}' !== $sLastc )
			{
				return FALSE;
			}
		}
		else
		{
			$sSemicolon = strpos( $sData, ';' );
			$sBrace     = strpos( $sData, '}' );
			// Either ; or } must exist.
			if ( FALSE === $sSemicolon && FALSE === $sBrace )
			{
				return FALSE;
			}
			// But neither must be in the first X characters.
			if ( FALSE !== $sSemicolon && $sSemicolon < 3 )
			{
				return FALSE;
			}
			if ( FALSE !== $sBrace && $sBrace < 4 )
			{
				return FALSE;
			}
		}

		$sToken = $sData[ 0 ];
		switch ( $sToken )
		{
			case 's' :
				if ( $bStrict )
				{
					if ( '"' !== substr( $sData, -2, 1 ) )
					{
						return FALSE;
					}
				}
				elseif ( FALSE === strpos( $sData, '"' ) )
				{
					return FALSE;
				}
				// or else fall through
			case 'a' :
			case 'O' :
				return (bool) preg_match( "/^{$sToken}:[0-9]+:/s", $sData );
			case 'b' :
			case 'i' :
			case 'd' :
				$sEnd = $bStrict ? '$' : '';
				return (bool) preg_match( "/^{$sToken}:[0-9.E-]+;$sEnd/", $sData );
		}
		return FALSE;
	}

	/**
	 * Helper function to determine if a Unicode value is valid.
	 *
	 * @param	int $i Unicode value
	 * @return	bool True if the value was a valid Unicode number
	 */
	public static function validUnicode( $i )
	{
		return ( $i == 0x9 || $i == 0xa || $i == 0xd ||
			   ( $i >= 0x20 && $i <= 0xd7ff ) ||
			   ( $i >= 0xe000 && $i <= 0xfffd ) ||
			   ( $i >= 0x10000 && $i <= 0x10ffff ) );
	}

	/**
	 * Merge user defined arguments into defaults array.
	 *
	 * This function is used to allow for both string or array
	 * to be merged into another array.
	 *
	 * @param	string|array $aArgs     Value to merge with $aDefaults
	 * @param	array        $aDefaults Optional. Array that serves as the defaults. Default empty.
	 * @return	array Merged user defined values with defaults.
	 */
	public static function parseArgs( $aArgs, $aDefaults = array() ) {

		if ( is_object( $aArgs ) )
			$r = get_object_vars( $aArgs );
		elseif ( is_array( $aArgs ) )
			$r =& $aArgs;
		else
			self::parseString( $aArgs, $r );

		if ( is_array( $aDefaults ) )
			return array_merge( $aDefaults, $r );
		return $r;
	}

	/**
	 * Parses a string into variables to be stored in an array.
	 *
	 * @param	string $sString The string to be parsed.
	 * @param	array  $aArray  Variables will be stored in this array.
	 */
	public static function parseString( $sString, &$aArray )
	{
		parse_str( $sString, $aArray );
		if ( get_magic_quotes_gpc() )
		{
			$aArray = self::stripslashesDeep( $aArray );
		}
		/**
		 * Filter the array of variables derived from a parsed string.
		 *
		 * @param	array $aArray The array populated with variables.
		 */
		$aArray = Service\HookService::applyHooks( 'parseString', $aArray );
	}

	/**
	 * Navigates through an array and encodes the values to be used in a URL.
	 *
	 * @param	array|string $mValue The array or string to be encoded.
	 * @return	array|string $mValue The encoded array (or string from the callback).
	 */
	public static function urlencodeDeep( $mValue )
	{
		return is_array( $mValue ) ? array_map( array( 'PACMAN\Helper\Formatting', 'stripslashesDeep' ), $mValue ) : urlencode( $mValue );
	}

	/**
	 * Navigates through an array and removes slashes from the values.
	 *
	 * If an array is passed, the array_map() function causes a callback to pass the
	 * value back to the function. The slashes from this value will removed.
	 *
	 * @param	mixed $mValue The value to be stripped.
	 * @return	mixed Stripped value.
	 */
	public static function stripslashesDeep( $mValue )
	{
		if ( is_array( $mValue ) )
		{
			$mValue = array_map( array( 'Helper\Formatting', 'stripslashesDeep' ), $mValue );
		}
		else if ( is_object( $mValue ) )
		{
			$aVars = get_object_vars( $mValue );
			foreach ( $aVars as $sKey => $mData )
			{
				$mValue->{$sKey} = self::stripslashesDeep( $mData );
			}
		}
		else if ( is_string( $mValue ) )
		{
			$mValue = stripslashes( $mValue );
		}

		return $mValue;
	}

	/**
	 * Escaping for HTML attributes.
	 *
	 * @param	string $sText
	 * @return	string
	 */
	public static function escAttr( $sText )
	{
		$sSafeText = self::checkInvalidUtf8( $sText );
		$sSafeText = self::specialChars( $sSafeText, ENT_QUOTES );
		/**
		 * Filter a string cleaned and escaped for output in an HTML attribute.
		 *
		 * Text passed to esc_attr() is stripped of invalid or special characters
		 * before output.
		 *
		 * @since 2.0.6
		 *
		 * @param string $sSafeText The text after it has been escaped.
	 	 * @param string $sText      The text prior to being escaped.
		 */
		return Service\HookService::applyHooks( 'escAttr', $sSafeText, $sText );
	}

	/**
	 * Checks for invalid UTF8 in a string.
	 *
	 * @param	string  $sString The text which is to be checked.
	 * @param	bool    $bStrip Optional. Whether to attempt to strip out invalid UTF8. Default is false.
	 * @return	string The checked text.
	 */
	public static function checkInvalidUtf8( $sString, $bStrip = false )
	{
		$sString = (string) $sString;
		if ( 0 === strlen( $sString ) )
		{
			return '';
		}

		// Store the site charset as a static to avoid multiple calls to get_option()
		static $bIsUtf8 = null;
		if ( ! isset( $bIsUtf8 ) )
		{
			$bIsUtf8 = in_array( SITE_CHARSET, array( 'utf8', 'utf-8', 'UTF8', 'UTF-8' ) );
		}
		if ( ! $bIsUtf8 )
		{
			return $sString;
		}

		// Check for support for utf8 in the installed PCRE library once and store the result in a static
		static $bItf8Pcre = null;
		if ( ! isset( $bItf8Pcre ) )
		{
			$bItf8Pcre = @preg_match( '/^./u', 'a' );
		}

		// We can't demand utf8 in the PCRE installation, so just return the string in those cases
		if ( ! $bItf8Pcre )
		{
			return $sString;
		}

		// preg_match fails when it encounters invalid UTF8 in $sString
		if ( 1 === @preg_match( '/^./us', $sString ) )
		{
			return $sString;
		}

		// Attempt to strip the bad chars if requested (not recommended)
		if ( $bStrip && function_exists( 'iconv' ) )
		{
			return iconv( 'utf-8', 'utf-8', $sString );
		}

		return '';
	}

	/**
	 * Converts a number of special characters into their HTML entities.
	 *
	 * Specifically deals with: &, <, >, ", and '.
	 *
	 * $sQuoteStyle can be set to ENT_COMPAT to encode " to
	 * &quot;, or ENT_QUOTES to do both. Default is ENT_NOQUOTES where no quotes are encoded.
	 *
	 * @param	string $sString         The text which is to be encoded.
	 * @param	string $sQuoteStyle    Optional. Converts double quotes if set to ENT_COMPAT, both single and double if set to ENT_QUOTES or none if set to ENT_NOQUOTES. Also compatible with old values; converting single quotes if set to 'single', double if set to 'double' or both if otherwise set. Default is ENT_NOQUOTES.
	 * @param	string $sCharset        Optional. The character encoding of the string. Default is false.
	 * @param	bool   $bDoubleEncode  Optional. Whether to encode existing html entities. Default is false.
	 * @return	string The encoded text with HTML entities.
	 */
	public static function specialChars( $sString, $sQuoteStyle = ENT_NOQUOTES, $sCharset = false, $bDoubleEncode = false )
	{
		$sString = (string) $sString;

		if ( 0 === strlen( $sString ) )
		{
			return '';
		}

		// Don't bother if there are no specialchars - saves some processing
		if ( ! preg_match( '/[&<>"\']/', $sString ) )
		{
			return $sString;
		}

		// Account for the previous behaviour of the function when the $sQuoteStyle is not an accepted value
		if ( empty( $sQuoteStyle ) )
		{
			$sQuoteStyle = ENT_NOQUOTES;
		}
		else if ( ! in_array( $sQuoteStyle, array( 0, 2, 3, 'single', 'double' ), true ) )
		{
			$sQuoteStyle = ENT_QUOTES;
		}

		// Store the site charset as a static
		if ( ! $sCharset )
		{
			static $_sCharset = null;
			if ( ! isset( $_sCharset ) )
			{
				$_sCharset = defined( SITE_CHARSET ) ? SITE_CHARSET : '';
			}
			$sCharset = $_sCharset;
		}

		if ( in_array( $sCharset, array( 'utf8', 'utf-8', 'UTF8' ) ) )
		{
			$sCharset = 'UTF-8';
		}

		$_sQuoteStyle = $sQuoteStyle;

		if ( $sQuoteStyle === 'double' )
		{
			$sQuoteStyle = ENT_COMPAT;
			$_sQuoteStyle = ENT_COMPAT;
		}
		elseif ( $sQuoteStyle === 'single' )
		{
			$sQuoteStyle = ENT_NOQUOTES;
		}

		if ( ! $bDoubleEncode )
		{
			// Guarantee every &entity; is valid, convert &garbage; into &amp;garbage;
			// This is required for PHP < 5.4.0 because ENT_HTML401 flag is unavailable.
			$sString = Helper\Kses::normalizeEntities( $sString );
		}

		$sString = @htmlspecialchars( $sString, $sQuoteStyle, $sCharset, $bDoubleEncode );

		// Backwards compatibility
		if ( 'single' === $_sQuoteStyle )
		{
			$sString = str_replace( "'", '&#039;', $sString );
		}

		return $sString;
	}

	/**
	 * Sanitizes a URL for use in a redirect.
	 *
	 * @return	string redirect-sanitized URL
	 **/
	public static function sanitizeRedirect( $sLocation )
	{
		$sRegex = '/
			(
				(?: [\xC2-\xDF][\x80-\xBF]        # double-byte sequences   110xxxxx 10xxxxxx
				|   \xE0[\xA0-\xBF][\x80-\xBF]    # triple-byte sequences   1110xxxx 10xxxxxx * 2
				|   [\xE1-\xEC][\x80-\xBF]{2}
				|   \xED[\x80-\x9F][\x80-\xBF]
				|   [\xEE-\xEF][\x80-\xBF]{2}
				|   \xF0[\x90-\xBF][\x80-\xBF]{2} # four-byte sequences   11110xxx 10xxxxxx * 3
				|   [\xF1-\xF3][\x80-\xBF]{3}
				|   \xF4[\x80-\x8F][\x80-\xBF]{2}
			){1,40}                              # ...one or more times
			)/x';
		$sLocation = preg_replace_callback( $sRegex, array( 'PACMAN\Helper\Formatting', 'sanitizeUtf8InRedirect' ), $sLocation );
		$sLocation = preg_replace( '|[^a-z0-9-~+_.?#=&;,/:%!*\[\]()]|i', '', $sLocation );
		$sLocation = Helper\Kses::noNull( $sLocation );

		// remove %0d and %0a from location
		$aStrip = array( '%0d', '%0a', '%0D', '%0A' );
		return self::deepReplace( $aStrip, $sLocation );
	}

	/**
	 * URL encode UTF-8 characters in a URL.
	 *
	 * @param	array $aMatches
	 * @return	string the URL
	 */
	public static function sanitizeUtf8InRedirect( $aMatches )
	{
		return urlencode( $aMatches[ 0 ] );
	}

	/**
	 * Perform a deep string replace operation to ensure the values in $sSearch are no longer present
	 *
	 * Repeats the replacement operation until it no longer replaces anything so as to remove "nested" values
	 * e.g. $sSubject = '%0%0%0DDD', $sSearch ='%0D', $result ='' rather than the '%0%0DD' that
	 * str_replace would return
	 *
	 * @param	string|array $sSearch  The value being searched for, otherwise known as the needle.
	 *                                An array may be used to designate multiple needles.
	 * @param	string       $sSubject The string being searched and replaced on, otherwise known as the haystack.
	 * @return	string The string with the replaced svalues.
	 */
	public static function deepReplace( $sSearch, $sSubject )
	{
		$sSubject = (string) $sSubject;

		$iCount = 1;
		while ( $iCount )
		{
			$sSubject = str_replace( $sSearch, '', $sSubject, $iCount );
		}

		return $sSubject;
	}

	/**
	 * Formats the content
	 *
	 * @todo	apply more formatting and sanitizing
	 * 
	 * @param	string $sContent
	 * @return	string
	 */
	public static function formatContent( $sContent )
	{
		$sContent = self::autoP( $sContent );
		return Service\HookService::applyHooks( 'formatContent', $sContent );
	}

	/**
	 * Replaces double line-breaks with paragraph elements.
	 *
	 * A group of regex replaces used to identify text formatted with newlines and
	 * replace double line-breaks with HTML paragraph tags. The remaining line-breaks
	 * after conversion become <<br />> tags, unless $bBr is set to '0' or 'false'.
	 *
	 * @param	string $sPee The text which has to be formatted.
	 * @param	bool   $bBr  Optional. If set, this will convert all remaining line-breaks
	 *                    after paragraphing. Default true.
	 * @return	string Text which has been converted into correct paragraph tags.
	 */
	public static function autoP( $sPee, $bBr = TRUE )
	{
		$aPreTags = array();

		if ( trim( $sPee ) === '' )
		{
			return '';
		}

		// Just to make things a little easier, pad the end.
		$sPee = $sPee . "\n";

		/*
		 * Pre tags shouldn't be touched by autop.
		 * Replace pre tags with placeholders and bring them back after autop.
		 */
		if ( strpos( $sPee, '<pre' ) !== FALSE )
		{
			$aPeeParts = explode( '</pre>', $sPee );
			$sLastPee = array_pop( $aPeeParts );
			$sPee = '';
			$i = 0;

			foreach ( $aPeeParts as $sPeePart ) {
				$bStart = strpos( $sPeePart, '<pre' );

				// Malformed html?
				if ( $bStart === FALSE )
				{
					$sPee .= $sPeePart;
					continue;
				}

				$sName = "<pre wp-pre-tag-$i></pre>";
				$aPreTags[ $sName ] = substr( $sPeePart, $bStart ) . '</pre>';

				$sPee .= substr( $sPeePart, 0, $bStart ) . $sName;
				$i++;
			}

			$sPee .= $sLastPee;
		}

		// Change multiple <br>s into two line breaks, which will turn into paragraphs.
		$sPee = preg_replace( '|<br\s*/?>\s*<br\s*/?>|', "\n\n", $sPee );

		$sAllBlocks = '(?:table|thead|tfoot|caption|col|colgroup|tbody|tr|td|th|div|dl|dd|dt|ul|ol|li|pre|form|map|area|blockquote|address|math|style|p|h[1-6]|hr|fieldset|legend|section|article|aside|hgroup|header|footer|nav|figure|figcaption|details|menu|summary)';

		// Add a single line break above block-level opening tags.
		$sPee = preg_replace( '!(<' . $sAllBlocks . '[\s/>])!', "\n$1", $sPee );

		// Add a double line break below block-level closing tags.
		$sPee = preg_replace( '!(</' . $sAllBlocks . '>)!', "$1\n\n", $sPee );

		// Standardize newline characters to "\n".
		$sPee = str_replace( array( "\r\n", "\r" ), "\n", $sPee );

		// Collapse line breaks before and after <option> elements so they don't get autop'd.
		if ( strpos( $sPee, '<option' ) !== false ) {
			$sPee = preg_replace( '|\s*<option|', '<option', $sPee );
			$sPee = preg_replace( '|</option>\s*|', '</option>', $sPee );
		}

		// Collapse line breaks inside <object> elements, before <param> and <embed> elements
		// so they don't get autop'd.
		if ( strpos( $sPee, '</object>' ) !== false ) {
			$sPee = preg_replace( '|(<object[^>]*>)\s*|', '$1', $sPee );
			$sPee = preg_replace( '|\s*</object>|', '</object>', $sPee );
			$sPee = preg_replace( '%\s*(</?(?:param|embed)[^>]*>)\s*%', '$1', $sPee );
		}

		// Collapse line breaks inside <audio> and <video> elements,
		// before and after <source> and <track> elements.
		if ( strpos( $sPee, '<source' ) !== false || strpos( $sPee, '<track' ) !== false ) {
			$sPee = preg_replace( '%([<\[](?:audio|video)[^>\]]*[>\]])\s*%', '$1', $sPee );
			$sPee = preg_replace( '%\s*([<\[]/(?:audio|video)[>\]])%', '$1', $sPee );
			$sPee = preg_replace( '%\s*(<(?:source|track)[^>]*>)\s*%', '$1', $sPee );
		}

		// Remove more than two contiguous line breaks.
		$sPee = preg_replace( "/\n\n+/", "\n\n", $sPee );

		// Split up the contents into an array of strings, separated by double line breaks.
		$sPees = preg_split( '/\n\s*\n/', $sPee, -1, PREG_SPLIT_NO_EMPTY );

		// Reset $sPee prior to rebuilding.
		$sPee = '';

		// Rebuild the content as a string, wrapping every bit with a <p>.
		foreach ( $sPees as $tinkle )
		{
			$sPee .= '<p>' . trim( $tinkle, "\n" ) . "</p>\n";
		}

		// Under certain strange conditions it could create a P of entirely whitespace.
		$sPee = preg_replace( '|<p>\s*</p>|', '', $sPee );

		// Add a closing <p> inside <div>, <address>, or <form> tag if missing.
		$sPee = preg_replace( '!<p>([^<]+)</(div|address|form)>!', "<p>$1</p></$2>", $sPee );

		// If an opening or closing block element tag is wrapped in a <p>, unwrap it.
		$sPee = preg_replace( '!<p>\s*(</?' . $sAllBlocks . '[^>]*>)\s*</p>!', "$1", $sPee );

		// In some cases <li> may get wrapped in <p>, fix them.
		$sPee = preg_replace( "|<p>(<li.+?)</p>|", "$1", $sPee );

		// If a <blockquote> is wrapped with a <p>, move it inside the <blockquote>.
		$sPee = preg_replace( '|<p><blockquote([^>]*)>|i', "<blockquote$1><p>", $sPee );
		$sPee = str_replace( '</blockquote></p>', '</p></blockquote>', $sPee );

		// If an opening or closing block element tag is preceded by an opening <p> tag, remove it.
		$sPee = preg_replace( '!<p>\s*(</?' . $sAllBlocks . '[^>]*>)!', "$1", $sPee );

		// If an opening or closing block element tag is followed by a closing <p> tag, remove it.
		$sPee = preg_replace( '!(</?' . $sAllBlocks . '[^>]*>)\s*</p>!', "$1", $sPee );

		// Optionally insert line breaks.
		if ( $bBr )
		{
			// Normalize <br>
			$sPee = str_replace( array( '<br>', '<br/>' ), '<br />', $sPee );

			// Replace any new line characters that aren't preceded by a <br /> with a <br />.
			$sPee = preg_replace( '|(?<!<br />)\s*\n|', "<br />\n", $sPee );

			// Replace newline placeholders with newlines.
			$sPee = str_replace( '<WPPreserveNewline />', "\n", $sPee );
		}

		// If a <br /> tag is after an opening or closing block tag, remove it.
		$sPee = preg_replace( '!(</?' . $sAllBlocks . '[^>]*>)\s*<br />!', "$1", $sPee );

		// If a <br /> tag is before a subset of opening or closing block tags, remove it.
		$sPee = preg_replace( '!<br />(\s*</?(?:p|li|div|dl|dd|dt|th|pre|td|ul|ol)[^>]*>)!', '$1', $sPee );
		$sPee = preg_replace( "|\n</p>$|", '</p>', $sPee );

		// Replace placeholder <pre> tags with their original content.
		if ( ! empty( $aPreTags ) )
		{
			$sPee = str_replace( array_keys( $aPreTags ), array_values( $aPreTags ), $sPee );
		}

		return $sPee;
	}
}