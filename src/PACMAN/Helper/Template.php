<?php
namespace PACMAN\Helper;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;

class Template
{
	/**
	 * Loads a template file
	 * 
	 * @param	string $sTemplateFile
	 * @return	void
	 */
	public static function loadTemplate( $sTemplateFile )
	{
		// set the filename
		$sTemplateToLoad = $sTemplateFile . '.php';

		// set up the path to the template and load it
		$sPathToTemplateFolder = Helper\General::getAssetDirectory( 'templates' );
		$sTemplateFile = $sPathToTemplateFolder . $sTemplateToLoad;
		require_once $sTemplateFile;
	}

	/**
	 * Gets the current content path based upon the
	 * given URL
	 * 
	 * @return	string
	 */
	public static function getCurrentPath()
	{
		// get the current query string
		$sQueryString = isset( $_GET[ 'path' ] ) ? $_GET[ 'path' ] : '';

		// explode the query string on '/' to determinate if
		// the last element is an int so it is a paged archive
		$aQueryVars = explode( '/', $sQueryString );

		// we have many dump stuff in the array to we clear the
		// empty ones to get a clear one
		$aNewQueryVars = array();
		foreach ( $aQueryVars as $iIndex => $sQueryVar )
		{
			if ( ! empty( $sQueryVar ) )
			{
				$aNewQueryVars[] = $sQueryVar;
			}
		}

		// get the last element
		$aLastQueryElement = array_pop( $aQueryVars );
		if ( is_int( $aLastQueryElement ) )
		{
			return $aNewQueryVars[ 0 ];
		}
		else
		{
			return $sQueryString;
		}
	}

	/**
	 * Gets the home URL of this website
	 * 
	 * @return	string
	 */
	public static function getHomeUrl()
	{
		return PM_URL;
	}

	/**
	 * Builds the URL for the current site with a given
	 * path so that there will be a correct URL
	 *
	 * @param	string $sPath
	 * @return	string URL
	 */
	public static function getUrl( $sPath )
	{
		$sBaseUrl = PM_URL . 'index.php';
		$sBaseUrl = Helper\General::addQueryArg( 'path', '/' . $sPath . '/', $sBaseUrl );

		return $sBaseUrl;
	}

	/**
	 * Builds the admin URL for the current site with a given
	 * path so that there will be a correct URL
	 *
	 * @param	string $sPath
	 * @return	string URL
	 */
	public static function getAdminUrl( $sPath )
	{
		$sBaseUrl = PM_ADMIN_URL . 'index.php';
		$sBaseUrl = Helper\General::addQueryArg( 'path', '/' . $sPath . '/', $sBaseUrl );

		return $sBaseUrl;
	}

	/**
	 * Redirects to another page.
	 *
	 * @param	string $sLocation The path to redirect to.
	 * @param	int    $iStatus   Status code to use.
	 * @return	bool False if $sLocation is not provided, TRUE otherwise.
	 */
	public static function redirect( $sLocation, $iStatus = 302 )
	{
		/**
		 * Filter the redirect location.
		 *
		 * @param	string $sLocation The path to redirect to.
		 * @param	int    $iStatus   Status code to use.
		 */
		$sLocation = Service\HookService::applyHooks( 'redirect', $sLocation, $iStatus );

		/**
		 * Filter the redirect status code.
		 *
		 * @param	int    $iStatus   Status code to use.
		 * @param	string $sLocation The path to redirect to.
		 */
		$iStatus = Service\HookService::applyHooks( 'redirectStatus', $iStatus, $sLocation );

		if ( ! $sLocation )
		{
			return FALSE;
		}

		// do some formatting stuff with the URL
		$sLocation = Helper\Formatting::sanitizeRedirect( $sLocation );
		$sLocation = urldecode( $sLocation );

		header( "Location: $sLocation", TRUE, $iStatus );

		return TRUE;
	}

	/**
	 * Loads the title depending on the current page
	 *
	 * @todo	add filter
	 * @todo	add argument for the seperator
	 * @todo	add argument for appending or prepending
	 * @todo	check if title exists and append / prepend it
	 * 
	 * @return	void
	 */
	public static function getPageTitle()
	{
		// build the G!GAlarm Title
		$sTitle = SITE_TITLE;

		// get the current content element
		$oCurrentContent = Service\ContentService::getCurrentContent();

		// get the title
		$sTitle .= ' &raquo; ' . Helper\Content::getTitle();

		return '<title>' . $sTitle . '</title>';
	}

	/**
	 * Loads all the needed header, additional meta data
	 * and scripts, styles and so on
	 * 
	 * @return	void
	 */
	public static function loadHead()
	{
		// load the scripts
		$oScriptService = Service\ScriptService::getInstance();
		$aScripts = $oScriptService->getScripts( 'header' );
		if ( ! empty( $aScripts ) )
		{
			$aScriptsToLoad = array();
			foreach ( $aScripts as $sHandle => $aScriptData )
			{
				// add the script
				$aScriptsToLoad[ $sHandle ] = $aScriptData;

				// check if we have deps
				if ( ! empty( $aScriptData[ 'deps' ] ) )
				{
					Service\ScriptService::loadScriptDepencies( $aScriptData[ 'deps' ], $aScriptsToLoad, 'header' );
				}
			}

			$aScriptsToLoad = Service\HookService::applyHooks( 'scriptsToLoad', $aScriptsToLoad );
			foreach ( $aScriptsToLoad as $aScript )
			{
				echo '<script type="text/javascript" src="' . $aScript[ 'src' ] . '"></script>';
			}
		}

		// load the styles
		$oStyleService = Service\StyleService::getInstance();
		$aStyles = $oStyleService->getStyles( 'header' );
		if ( ! empty( $aStyles ) )
		{
			$aStylesToLoad = array();
			foreach ( $aStyles as $sHandle => $aStyleData )
			{
				// add the style
				$aStylesToLoad[ $sHandle ] = $aStyleData;

				// check if we have deps
				if ( ! empty( $aStyleData[ 'deps' ] ) )
				{
					Service\StyleService::loadStyleDepencies( $aStyleData[ 'deps' ], $aStylesToLoad, 'header' );
				}
			}

			$aStylesToLoad = Service\HookService::applyHooks( 'stylesToLoad', $aStylesToLoad );
			foreach ( $aStylesToLoad as $aStyle )
			{
				echo '<link rel="stylesheet" type="text/css" href="' . $aStyle[ 'src' ] . '">';
			}
		}

		// let other modules hook to this
		Service\HookService::execHook( 'loadHead' );
	}

	/**
	 * Loads all the needed footer, additional meta data
	 * and scripts, styles and so on
	 * 
	 * @return	void
	 */
	public static function loadFoot()
	{
		// load the scripts
		$oScriptService = Service\ScriptService::getInstance();
		$aScripts = $oScriptService->getScripts( 'footer' );
		if ( ! empty( $aScripts ) )
		{
			$aScriptsToLoad = array();
			foreach ( $aScripts as $sHandle => $aScriptData )
			{
				// add the script
				$aScriptsToLoad[ $sHandle ] = $aScriptData;

				// check if we have deps
				if ( ! empty( $aScriptData[ 'deps' ] ) )
				{
					Service\ScriptService::loadScriptDepencies( $aScriptData[ 'deps' ], $aScriptsToLoad, 'footer' );
				}
			}

			$aScriptsToLoad = Service\HookService::applyHooks( 'scriptsToLoad', $aScriptsToLoad );
			foreach ( $aScriptsToLoad as $aScript )
			{
				echo '<script type="text/javascript" src="' . $aScript[ 'src' ] . '"></script>';
			}
		}

		// let other modules hook to this
		Service\HookService::execHook( 'loadFoot' );
	}
}