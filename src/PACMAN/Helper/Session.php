<?php
namespace PACMAN\Helper;

class Session
{
	/**
	 * The instance of this class
	 * 
	 * @var	__CLASS__
	 */
	public static $iInstance;

	/**
	 * Checks if there is an instance of this class
	 * available. If not we init one.
	 * 
	 * @return	__CLASS__
	 */
	public static function getInstance()
	{
		if ( NULL === self::$iInstance )
		{
			self::$iInstance = new self;
		}
		return self::$iInstance;
	}

	/**
	 * Starts a session if we need to
	 * 
	 * @return	void
	 */
	public function start()
	{
		if ( ( session_id() == '' || ! isset( $_SESSION ) ) && ! headers_sent() )
		{
			session_start();
		}
	}

	/**
	 * Gets a value from a session entry
	 * 
	 * @param	string $sKey       key for the session
	 * @param	string $sNamespace optional namespace of the session
	 * @return	mixed
	 */
	public static function get( $sKey, $sNamespace = NULL )
	{
		// get the instance of the hook class
		$oSessionServiceInstance = self::getInstance();
		$oSessionServiceInstance->start();

		// check if we got a namespace
		if ( is_null( $sNamespace ) )
		{
			return isset( $_SESSION[ $sKey ] ) ? $_SESSION[ $sKey ] : '';
		}
		else
		{
			return isset( $_SESSION[ $sNamespace ][ $sKey ] ) ? $_SESSION[ $sNamespace ][ $sKey ] : '';
		}
	}

	/**
	 * Checks if a value in a session is set
	 * 
	 * @param	string $sKey       key for the session
	 * @param	string $sNamespace optional namespace of the session
	 * @return	boolean
	 */
	public static function setted( $sKey, $sNamespace = NULL )
	{
		// get the instance of the hook class
		$oSessionServiceInstance = self::getInstance();
		$oSessionServiceInstance->start();
		// check if we got a namespace
		if ( is_null( $sNamespace ) )
		{
			return isset( $_SESSION[ $sKey ] );
		}
		else
		{
			return isset( $_SESSION[ $sNamespace ][ $sKey ] );
		}
	}

	/**
	 * Removes a value to from session
	 * 
	 * @param	string $sKey       key for the session
	 * @param	string $sNamespace optional namespace of the session
	 * @return	void
	 */
	public static function remove( $sKey, $sNamespace = NULL )
	{
		// get the instance of the hook class
		$oSessionServiceInstance = self::getInstance();
		$oSessionServiceInstance->start();

		// check if we got a namespace
		if ( is_null( $sNamespace ) )
		{
			unset( $_SESSION[ $sKey ] );
		}
		else
		{
			unset( $_SESSION[ $sNamespace ][ $sKey ] );
		}
	}


	/**
	 * Adds a session entry to the session system
	 * 
	 * @param	string  $sKey       key for the session
	 * @param	string  $mValue	    value for the session entry
	 * @param	string  $sNamespace optional namespace of the session
	 *
	 * @return	Session[]
	 */
	public static function add( $sKey, $mValue, $sNamespace = NULL )
	{
		// get the instance of the hook class
		$oSessionServiceInstance = self::getInstance();
		$oSessionServiceInstance->start();

		// add the session to the global session object
		if ( is_null( $sNamespace ) )
		{
			$_SESSION[ $sKey ] = $mValue;
		}
		else
		{
			$_SESSION[ $sNamespace ][ $sKey ] = $mValue;
		}

		// return the session entry, maybe someone want to use it
		return $mValue;
	}
}