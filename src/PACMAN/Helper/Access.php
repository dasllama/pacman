<?php
namespace PACMAN\Helper;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;

class Access
{
	/**
	 * Checks if the current user has the given capability
	 *
	 * @see		self::userCan
	 * @param	string $sCapability
	 * @return	boolean
	 */
	public static function currentUserCan( $sCapability )
	{
		$iCurrentUserId = Helper\User::getCurrentUserId();
		if ( $iCurrentUserId == 0 )
			return FALSE;
		return self::userCan( $iCurrentUserId, $sCapability );
	}

	/**
	 * Checks if the given user has the given capability
	 *
	 * @param	int $iUserId
	 * @param	string $sCapability
	 * @return	boolean
	 */
	public static function userCan( $iUserId, $sCapability )
	{
		// get the access service to get the registered capabilities
		$oAccessService = Service\AccessService::getInstance();
		$aRolesAndCapabilities = $oAccessService->getRolesAndCapabilities();

		// get the role of the given user to check the capability
		$oUserData = Helper\User::getUser( $iUserId );

		// administrators are allowed to do everything
		if ( $oUserData->role == 'administrator' )
		{
			return TRUE;
		}

		// get the queried capabilities
		// if the role does not exists, just return FALSE
		$bPreCheck = FALSE;
		if ( ! isset( $aRolesAndCapabilities[ $oUserData->role ] ) )
		{
			$bPreCheck = FALSE;
		}
		$aQueriedCapabilities = $aRolesAndCapabilities[ $oUserData->role ];

		// check if the capability is in the array
		if ( ! in_array( $sCapability, $aQueriedCapabilities ) )
		{
			$bPreCheck = FALSE;
		}
		else
		{
			$bPreCheck = TRUE;
		}

		// if the capability is not in the list of the role check
		// if the user has the capability as special capability
		if ( $bPreCheck === FALSE )
		{
			if ( in_array( $sCapability, $oUserData->capabilities ) )
			{
				$bPreCheck = TRUE;
			}
		}

		return Service\HookService::applyHooks( 'userCan', $bPreCheck, $iUserId );
	}
}