<?php
namespace PACMAN\Helper;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;
use PACMAN\Library as Library;

class General
{
	/**
	 * Checks if a given object is an entity of Error
	 * 
	 * @param	mixed $mObject
	 * @return	boolean
	 */
	public static function isError( $mObject )
	{
		return ( is_a( $mObject, 'Error' ) ? TRUE : FALSE );
	}

	/**
	 * Gets the database object
	 *
	 * @return	object the database
	 */
	public static function getDatabase()
	{
		// get the database service
		$oDatabaseService = new Service\MySQLDatabaseService( DB_USER, DB_PASSWORD, DB_NAME, DB_HOST );
		return $oDatabaseService;
	}

	/**
	 * Gets the specific asset directory url
	 *
	 * @param	string $sPath the relative path to the wanted subdirectory. If
	 *				no path is selected, the root asset directory will be returned
	 * @return	string the url of the wcpd asset directory
	 */
	public static function getAssetDirectoryUrl( $sPath = '' )
	{
		// set base url
		$sAssetsUrl = PM_URL . 'assets/';
		if ( $sPath != '' )
		{
			$sAssetsUrl .= $sPath . '/';
		}
		return $sAssetsUrl;
	}

	/**
	 * Gets the specific asset directory path
	 *
	 * @param	string $sPath the relative path to the wanted subdirectory. If
	 *				no path is selected, the root asset directory will be returned
	 * @return	string the url of the wcpd asset directory
	 */
	public static function getAssetDirectory( $sPath = '' )
	{
		// set base url
		$sAssets = ABSPATH . 'assets/';
		if ( $sPath != '' )
		{
			$sAssets .= $sPath . '/';
		}
		return $sAssets;
	}

	/**
	 * Gets the specific asset directory url
	 *
	 * @param	string $sPath the relative path to the wanted subdirectory. If
	 *				no path is selected, the root asset directory will be returned
	 * @return	string the url of the wcpd asset directory
	 */
	public static function getAdminAssetDirectoryUrl( $sPath = '' )
	{
		// set base url
		$sAssetsUrl = PM_URL . 'admin/assets/';
		if ( $sPath != '' )
		{
			$sAssetsUrl .= $sPath . '/';
		}
		return $sAssetsUrl;
	}

	/**
	 * Gets the specific asset directory path
	 *
	 * @param	string $sPath the relative path to the wanted subdirectory. If
	 *				no path is selected, the root asset directory will be returned
	 * @return	string the url of the wcpd asset directory
	 */
	public static function getAdminAssetDirectory( $sPath = '' )
	{
		// set base url
		$sAssets = ABSPATH . 'admin/assets/';
		if ( $sPath != '' )
		{
			$sAssets .= $sPath . '/';
		}
		return $sAssets;
	}

	/**
	 * Gets the URL to the post
	 * 
	 * @return	string
	 */
	public static function getPostUrl()
	{
		return PM_URL . 'post.php';	
	}

	/**
	 * Gets the current URL
	 * 	
	 * @return	string
	 */
	public static function getCurrentUrl()
	{
		// TODO
		return PM_URL . 'index.php?' . $_SERVER[ 'QUERY_STRING' ];
	}

	/**
	 * Retrieve a modified URL query string.
	 *
	 * You can rebuild the URL and append a new query variable to the URL query by
	 * using this function. You can also retrieve the full URL with query data.
	 *
	 * Adding a single key & value or an associative array. Setting a key value to
	 * an empty string removes the key. Omitting oldquery_or_uri uses the $_SERVER
	 * value. Additional values provided are expected to be encoded appropriately
	 * with urlencode() or rawurlencode().
	 *
	 * @param	string|array $param1 Either newkey or an associative_array.
	 * @param	string       $param2 Either newvalue or oldquery or URI.
	 * @param	string       $param3 Optional. Old query or URI.
	 * @return	string New URL query string.
	 */
	public static function addQueryArg() {

		$aArgs = func_get_args();
		if ( is_array( $aArgs[ 0 ] ) )
		{
			if ( count( $aArgs ) < 2 || FALSE === $aArgs[ 1 ] )
			{
				$sUri = $_SERVER[ 'REQUEST_URI' ];
			}
			else
			{
				$sUri = $aArgs[ 1 ];
			}
		}
		else
		{
			if ( count( $aArgs ) < 3 || FALSE === $aArgs[ 2 ] )
			{
				$sUri = $_SERVER[ 'REQUEST_URI' ];
			}
			else
			{
				$sUri = $aArgs[ 2 ];
			}
		}

		if ( $sFrag = strstr( $sUri, '#' ) )
		{
			$sUri = substr( $sUri, 0, - strlen( $sFrag ) );
		}
		else
		{
			$sFrag = '';
		}

		if ( 0 === stripos( $sUri, 'http://' ) )
		{
			$sProtocol = 'http://';
			$sUri = substr( $sUri, 7 );
		}
		elseif ( 0 === stripos( $sUri, 'https://' ) )
		{
			$sProtocol = 'https://';
			$sUri = substr( $sUri, 8 );
		}
		else
		{
			$sProtocol = '';
		}

		if ( strpos( $sUri, '?' ) !== FALSE )
		{
			list( $sBase, $sQuery ) = explode( '?', $sUri, 2 );
			$sBase .= '?';
		}
		else if ( $sProtocol || strpos( $sUri, '=' ) === FALSE )
		{
			$sBase = $sUri . '?';
			$sQuery = '';
		}
		else {
			$sBase = '';
			$sQuery = $sUri;
		}

		Helper\Formatting::parseString( $sQuery, $aQs );
		$aQs = Helper\Formatting::urlencodeDeep( $aQs ); // this re-URL-encodes things that were already in the query string
		if ( is_array( $aArgs[ 0 ] ) )
		{
			foreach ( $aArgs[ 0 ] as $sK => $sV )
			{
				$aQs[ $sK ] = $sV;
			}
		}
		else
		{
			$aQs[ $aArgs[ 0 ] ] = $aArgs[ 1 ];
		}

		foreach ( $aQs as $sK => $sV )
		{
			if ( $sV === FALSE )
			{
				unset( $aQs[$sK] );
			}
		}

		$sReturnValue = self::buildQuery( $aQs );
		$sReturnValue = trim( $sReturnValue, '?' );
		$sReturnValue = preg_replace( '#=(&|$)#', '$1', $sReturnValue );
		$sReturnValue = $sProtocol . $sBase . $sReturnValue . $sFrag;
		$sReturnValue = rtrim( $sReturnValue, '?' );
		return $sReturnValue;
	}

	/**
	 * Build URL query based on an associative and, or indexed array.
	 *
	 * This is a convenient function for easily building url queries. It sets the
	 * separator to '&' and uses self::_httpBuildQuery() function.
	 *
	 * @param	array $aData URL-encode key/value pairs.
	 * @return	string URL-encoded string.
	 */
	public static function buildQuery( $aData )
	{
		return self::httpBuildQuery( $aData, NULL, '&', '', FALSE );
	}

	/**
	 * From php.net (modified by Mark Jaquith to behave like the native PHP5 function).
	 *
	 * @param	array|object  $aData       An array or object of data. Converted to array.
	 * @param	string        $sPrefix     Optional. Numeric index. If set, start parameter numbering with it.
	 *                                     Default null.
	 * @param	string        $sSep        Optional. Argument separator; defaults to 'arg_separator.output'.
	 *                                     Default null.
	 * @param	string        $sKey        Optional. Used to prefix key name. Default empty.
	 * @param	bool          $bUrlencode  Optional. Whether to use urlencode() in the result. Default TRUE.
	 * @return	string The query string.
	 */
	public static function httpBuildQuery( $aData, $sPrefix = NULL, $sSep = NULL, $sKey = '', $bUrlencode = TRUE )
	{
		$aReturnValues = array();

		foreach ( (array) $aData as $sK => $sV )
		{
			if ( $bUrlencode)
			{
				$sK = urlencode( $sK );
			}

			if ( is_int( $sK ) && $sPrefix != NULL )
			{
				$sK = $sPrefix . $sK;
			}

			if ( !empty( $sKey ) )
			{
				$sK = $sKey . '%5B' . $sK . '%5D';
			}

			if ( $sV === NULL )
			{
				continue;
			}
			else if ( $sV === FALSE )
			{
				$sV = '0';
			}

			if ( is_array( $sV ) || is_object( $sV ) )
			{
				array_push( $aReturnValues, self::httpBuildQuery( $sV, '', $sSep, $sK, $bUrlencode ) );
			}
			else if ( $bUrlencode )
			{
				array_push( $aReturnValues, $sK . '=' . urlencode( $sV ) );
			}
			else
			{
				array_push( $aReturnValues, $sK . '=' . $sV );
			}
		}

		if ( NULL === $sSep )
		{
			$sSep = ini_get( 'arg_separator.output' );
		}

		return implode( $sSep, $aReturnValues );
	}

	/**
	 * Gets the allowed entitynames
	 * 
	 * @return	array
	 */
	public static function getAllowedEntitynames()
	{
		return Service\HookService::applyHooks( 'getAllowedEntitynames', array(
			'nbsp',    'iexcl',  'cent',    'pound',  'curren', 'yen',
			'brvbar',  'sect',   'uml',     'copy',   'ordf',   'laquo',
			'not',     'shy',    'reg',     'macr',   'deg',    'plusmn',
			'acute',   'micro',  'para',    'middot', 'cedil',  'ordm',
			'raquo',   'iquest', 'Agrave',  'Aacute', 'Acirc',  'Atilde',
			'Auml',    'Aring',  'AElig',   'Ccedil', 'Egrave', 'Eacute',
			'Ecirc',   'Euml',   'Igrave',  'Iacute', 'Icirc',  'Iuml',
			'ETH',     'Ntilde', 'Ograve',  'Oacute', 'Ocirc',  'Otilde',
			'Ouml',    'times',  'Oslash',  'Ugrave', 'Uacute', 'Ucirc',
			'Uuml',    'Yacute', 'THORN',   'szlig',  'agrave', 'aacute',
			'acirc',   'atilde', 'auml',    'aring',  'aelig',  'ccedil',
			'egrave',  'eacute', 'ecirc',   'euml',   'igrave', 'iacute',
			'icirc',   'iuml',   'eth',     'ntilde', 'ograve', 'oacute',
			'ocirc',   'otilde', 'ouml',    'divide', 'oslash', 'ugrave',
			'uacute',  'ucirc',  'uuml',    'yacute', 'thorn',  'yuml',
			'quot',    'amp',    'lt',      'gt',     'apos',   'OElig',
			'oelig',   'Scaron', 'scaron',  'Yuml',   'circ',   'tilde',
			'ensp',    'emsp',   'thinsp',  'zwnj',   'zwj',    'lrm',
			'rlm',     'ndash',  'mdash',   'lsquo',  'rsquo',  'sbquo',
			'ldquo',   'rdquo',  'bdquo',   'dagger', 'Dagger', 'permil',
			'lsaquo',  'rsaquo', 'euro',    'fnof',   'Alpha',  'Beta',
			'Gamma',   'Delta',  'Epsilon', 'Zeta',   'Eta',    'Theta',
			'Iota',    'Kappa',  'Lambda',  'Mu',     'Nu',     'Xi',
			'Omicron', 'Pi',     'Rho',     'Sigma',  'Tau',    'Upsilon',
			'Phi',     'Chi',    'Psi',     'Omega',  'alpha',  'beta',
			'gamma',   'delta',  'epsilon', 'zeta',   'eta',    'theta',
			'iota',    'kappa',  'lambda',  'mu',     'nu',     'xi',
			'omicron', 'pi',     'rho',     'sigmaf', 'sigma',  'tau',
			'upsilon', 'phi',    'chi',     'psi',    'omega',  'thetasym',
			'upsih',   'piv',    'bull',    'hellip', 'prime',  'Prime',
			'oline',   'frasl',  'weierp',  'image',  'real',   'trade',
			'alefsym', 'larr',   'uarr',    'rarr',   'darr',   'harr',
			'crarr',   'lArr',   'uArr',    'rArr',   'dArr',   'hArr',
			'forall',  'part',   'exist',   'empty',  'nabla',  'isin',
			'notin',   'ni',     'prod',    'sum',    'minus',  'lowast',
			'radic',   'prop',   'infin',   'ang',    'and',    'or',
			'cap',     'cup',    'int',     'sim',    'cong',   'asymp',
			'ne',      'equiv',  'le',      'ge',     'sub',    'sup',
			'nsub',    'sube',   'supe',    'oplus',  'otimes', 'perp',
			'sdot',    'lceil',  'rceil',   'lfloor', 'rfloor', 'lang',
			'rang',    'loz',    'spades',  'clubs',  'hearts', 'diams',
			'sup1',    'sup2',   'sup3',    'frac14', 'frac12', 'frac34',
			'there4',
		) );
	}

	/**
	 * Set the mbstring internal encoding to a binary safe encoding when func_overload
	 * is enabled.
	 *
	 * When mbstring.func_overload is in use for multi-byte encodings, the results from
	 * strlen() and similar functions respect the utf8 characters, causing binary data
	 * to return incorrect lengths.
	 *
	 * This function overrides the mbstring encoding to a binary-safe encoding, and
	 * resets it to the users expected encoding afterwards through the
	 * `reset_mbstring_encoding` function.
	 *
	 * It is safe to recursively call this function, however each
	 * `mbstring_binary_safe_encoding()` call must be followed up with an equal number
	 * of `reset_mbstring_encoding()` calls.
	 *
	 * @see reset_mbstring_encoding()
	 *
	 * @staticvar array $encodings
	 * @staticvar bool  $overloaded
	 *
	 * @param	bool $reset Optional. Whether to reset the encoding back to a previously-set encoding.
	 *                      Default FALSE.
	 * @return	void
	 */
	public static function mbstringBinarySafeEncoding( $reset = FALSE ) {
		static $encodings = array();
		static $overloaded = null;

		if ( is_null( $overloaded ) )
		{
			$overloaded = function_exists( 'mb_internal_encoding' ) && ( ini_get( 'mbstring.func_overload' ) & 2 );
		}

		if ( FALSE === $overloaded )
		{
			return;
		}

		if ( ! $reset )
		{
			$encoding = mb_internal_encoding();
			array_push( $encodings, $encoding );
			mb_internal_encoding( 'ISO-8859-1' );
		}

		if ( $reset && $encodings )
		{
			$encoding = array_pop( $encodings );
			mb_internal_encoding( $encoding );
		}
	}

	/**
	 * Reset the mbstring internal encoding to a users previously set encoding.
	 *
	 * @see		mbstringBinarySafeEncoding()
	 * @return	void
	 */
	public static function resetMbstringEncoding() {
		self::mbstringBinarySafeEncoding( TRUE );
	}

	/**
	 * Return a comma-separated string of functions that have been called to get
	 * to the current point in code.
	 *
	 * @param string $ignore_class Optional. A class to ignore all function calls within - useful
	 *                             when you want to just give info about the callee. Default null.
	 * @param int    $skip_frames  Optional. A number of stack frames to skip - useful for unwinding
	 *                             back to the source of the issue. Default 0.
	 * @param bool   $pretty       Optional. Whether or not you want a comma separated string or raw
	 *                             array returned. Default TRUE.
	 * @return string|array Either a string containing a reversed comma separated trace or an array
	 *                      of individual calls.
	 */
	public static function debugBacktraceSummary( $ignore_class = NULL, $skip_frames = 0, $pretty = TRUE )
	{
		if ( version_compare( PHP_VERSION, '5.2.5', '>=' ) )
		{
			$trace = debug_backtrace( FALSE );
		}
		else
		{
			$trace = debug_backtrace();
		}

		$caller = array();
		$check_class = ! is_null( $ignore_class );
		$skip_frames++; // skip this function

		foreach ( $trace as $call )
		{
			if ( $skip_frames > 0 )
			{
				$skip_frames--;
			}
			else if ( isset( $call[ 'class' ] ) )
			{
				if ( $check_class && $ignore_class == $call[ 'class' ] )
				{
					continue; // Filter out calls
				}

				$caller[] = "{$call[ 'class' ]}{$call[ 'type' ]}{$call[ 'function' ]}";
			}
			else
			{
				if ( in_array( $call[ 'function' ], array( 'do_action', 'apply_filters' ) ) )
				{
					$caller[] = "{$call[ 'function' ]}('{$call[ 'args' ][0]}')";
				}
				else if ( in_array( $call[ 'function' ], array( 'include', 'include_once', 'require', 'require_once' ) ) )
				{
					$caller[] = $call[ 'function' ] . "('" . $call[ 'args' ][0] . "')";
				}
				else
				{
					$caller[] = $call[ 'function' ];
				}
			}
		}
		if ( $pretty )
		{
			return join( ', ', array_reverse( $caller ) );
		}
		else
		{
			return $caller;
		}
	}

	/**
	 * Send mail, similar to PHP's mail
	 *
	 * A true return value does not automatically mean that the user received the
	 * email successfully. It just only means that the method used was able to
	 * process the request without any errors.
	 *
	 * Using the two '_mail_from' and '_mail_from_name' hooks allow from
	 * creating a from address like 'Name <email@address.com>' when both are set. If
	 * just '_mail_from' is set, then just the email address will be used with no
	 * name.
	 *
	 * The default content type is 'text/plain' which does not allow using HTML.
	 * However, you can set the content type of the email by using the
	 * '_mail_content_type' filter.
	 *
	 * The default charset is based on the charset used on the blog. The charset can
	 * be set using the '_mail_charset' filter.
	 *
	 * @param	string|array $mTo          Array or comma-separated list of email addresses to send message.
	 * @param	string       $sSubject     Email subject
	 * @param	string       $sMessage     Message contents
	 * @param	string|array $sHeaders     Optional. Additional headers.
	 * @param	string|array $aAttachments Optional. Files to attach.
	 * @return	bool Whether the email contents were sent successfully.
	 */
	public static function mail( $mTo, $sSubject, $sMessage, $sHeaders = '', $aAttachments = array() )
	{
		// Compact the input, apply the filters, and extract them back out

		/**
		 * Filter the _mail() arguments.
		 *
		 * @param array $args A compacted array of _mail() arguments, including the "to" email,
		 *                    subject, message, headers, and attachments values.
		 */
		$aAtts = Service\HookService::applyHooks( 'mail', compact( 'to', 'subject', 'message', 'headers', 'attachments' ) );

		if ( isset( $aAtts[ 'to' ] ) )
		{
			$mTo = $aAtts[ 'to' ];
		}

		if ( isset( $aAtts[ 'subject' ] ) )
		{
			$sSubject = $aAtts[ 'subject' ];
		}

		if ( isset( $aAtts[ 'message' ] ) )
		{
			$sMessage = $aAtts[ 'message' ];
		}

		if ( isset( $aAtts[ 'headers' ] ) )
		{
			$sHeaders = $aAtts[ 'headers' ];
		}

		if ( isset( $aAtts[ 'attachments' ] ) )
		{
			$aAttachments = $aAtts[ 'attachments' ];
		}

		if ( ! is_array( $aAttachments ) )
		{
			$aAttachments = explode( "\n", str_replace( "\r\n", "\n", $aAttachments ) );
		}

		// get the phpmailer
		$oPhpmailer = new Library\PHPMailer( true );

		// Headers
		if ( empty( $sHeaders ) )
		{
			$sHeaders = array();
		}
		else
		{
			if ( ! is_array( $sHeaders ) )
			{
				// Explode the headers out, so this function can take both
				// string headers and an array of headers.
				$aTempheaders = explode( "\n", str_replace( "\r\n", "\n", $sHeaders ) );
			}
			else
			{
				$aTempheaders = $sHeaders;
			}
			$sHeaders = array();
			$aCc = array();
			$aBcc = array();

			// If it's actually got contents
			if ( ! empty( $aTempheaders ) )
			{
				// Iterate through the raw headers
				foreach ( (array) $aTempheaders as $sHeader )
				{
					if ( strpos($sHeader, ':') === false )
					{
						if ( false !== stripos( $sHeader, 'boundary=' ) )
						{
							$aParts = preg_split( '/boundary=/i', trim( $sHeader ) );
							$sBoundary = trim( str_replace( array( "'", '"' ), '', $aParts[ 1 ] ) );
						}
						continue;
					}
					// Explode them out
					list( $sName, $sContent ) = explode( ':', trim( $sHeader ), 2 );

					// Cleanup crew
					$sName    = trim( $sName    );
					$sContent = trim( $sContent );

					switch ( strtolower( $sName ) )
					{
						// Mainly for legacy -- process a From: header if it's there
						case 'from':
							$iBracketPos = strpos( $sContent, '<' );
							if ( $iBracketPos !== false )
							{
								// Text before the bracketed email is the "From" name.
								if ( $iBracketPos > 0 )
								{
									$sFromName = substr( $sContent, 0, $iBracketPos - 1 );
									$sFromName = str_replace( '"', '', $sFromName );
									$sFromName = trim( $sFromName );
								}

								$sFromEmail = substr( $sContent, $iBracketPos + 1 );
								$sFromEmail = str_replace( '>', '', $sFromEmail );
								$sFromEmail = trim( $sFromEmail );
								// Avoid setting an empty $sFromEmail.
							}
							else if ( '' !== trim( $sContent ) )
							{
								$sFromEmail = trim( $sContent );
							}
							break;
						case 'content-type':
							if ( strpos( $sContent, ';' ) !== false )
							{
								list( $sType, $sCharsetContent ) = explode( ';', $sContent );
								$sContentType = trim( $sType );
								if ( false !== stripos( $sCharsetContent, 'charset=' ) )
								{
									$sCharset = trim( str_replace( array( 'charset=', '"' ), '', $sCharsetContent ) );
								}
								else if ( false !== stripos( $sCharsetContent, 'boundary=' ) )
								{
									$sBoundary = trim( str_replace( array( 'BOUNDARY=', 'boundary=', '"' ), '', $sCharsetContent ) );
									$sCharset = '';
								}
								// Avoid setting an empty $sContentType.
							}
							else if ( '' !== trim( $sContent ) )
							{
								$sContentType = trim( $sContent );
							}
							break;
						case 'cc':
							$aCc = array_merge( (array) $aCc, explode( ',', $sContent ) );
							break;
						case 'bcc':
							$aBcc = array_merge( (array) $aBcc, explode( ',', $sContent ) );
							break;
						default:
							// Add it to our grand headers array
							$sHeaders[trim( $sName )] = trim( $sContent );
							break;
					}
				}
			}
		}

		// Empty out the values that may be set
		$oPhpmailer->ClearAllRecipients();
		$oPhpmailer->ClearAttachments();
		$oPhpmailer->ClearCustomHeaders();
		$oPhpmailer->ClearReplyTos();

		// From email and name
		// If we don't have a name from the input headers
		if ( ! isset( $sFromName ) )
		{
			$sFromName = SITE_TITLE;
		}

		/* If we don't have an email from the input headers default to x@$sSitename
		 * Some hosts will block outgoing mail from this address if it doesn't exist but
		 * there's no easy alternative. Defaulting to admin_email might appear to be another
		 * option but some hosts may refuse to relay mail from an unknown domain.
		 */
		if ( ! isset( $sFromEmail ) )
		{
			// Get the site domain and get rid of www.
			$sSitename = strtolower( $_SERVER[ 'SERVER_NAME' ] );
			if ( substr( $sSitename, 0, 4 ) == 'www.' )
			{
				$sSitename = substr( $sSitename, 4 );
			}

			$sFromEmail = 'x@' . $sSitename;
		}

		/**
		 * Filter the email address to send from.
		 *
		 * @param string $sFromEmail Email address to send from.
		 */
		$oPhpmailer->From = Service\HookService::applyHooks( '_mail_from', $sFromEmail );

		/**
		 * Filter the name to associate with the "from" email address.
		 *
		 * @param string $sFromName Name associated with the "from" email address.
		 */
		$oPhpmailer->FromName = Service\HookService::applyHooks( '_mail_from_name', $sFromName );

		// Set destination addresses
		if ( ! is_array( $mTo ) )
		{
			$mTo = explode( ',', $mTo );
		}

		foreach ( (array) $mTo as $sRecipient )
		{
			try
			{
				// Break $sRecipient into name and address parts if in the format "Foo <bar@baz.com>"
				$sRecipientName = '';
				if ( preg_match( '/(.*)<(.+)>/', $sRecipient, $aMatches ) ) {
					if ( count( $aMatches ) == 3 ) {
						$sRecipientName = $aMatches[ 1 ];
						$sRecipient = $aMatches[ 2 ];
					}
				}
				$oPhpmailer->AddAddress( $sRecipient, $sRecipientName);
			}
			catch ( phpmailerException $e )
			{
				continue;
			}
		}

		// Set mail's subject and body
		$oPhpmailer->Subject = $sSubject;
		$oPhpmailer->Body    = $sMessage;

		// Add any CC and BCC recipients
		if ( ! empty( $aCc ) )
		{
			foreach ( (array) $aCc as $sRecipient )
			{
				try
				{
					// Break $sRecipient into name and address parts if in the format "Foo <bar@baz.com>"
					$sRecipientName = '';
					if ( preg_match( '/(.*)<(.+)>/', $sRecipient, $aMatches ) )
					{
						if ( count( $aMatches ) == 3 )
						{
							$sRecipientName = $aMatches[ 1 ];
							$sRecipient = $aMatches[ 2 ];
						}
					}
					$oPhpmailer->AddCc( $sRecipient, $sRecipientName );
				}
				catch ( phpmailerException $e )
				{
					continue;
				}
			}
		}

		if ( ! empty( $aBcc ) )
		{
			foreach ( (array) $aBcc as $sRecipient )
			{
				try
				{
					// Break $sRecipient into name and address parts if in the format "Foo <bar@baz.com>"
					$sRecipientName = '';
					if ( preg_match( '/(.*)<(.+)>/', $sRecipient, $aMatches ) )
					{
						if ( count( $aMatches ) == 3 )
						{
							$sRecipientName = $aMatches[ 1 ];
							$sRecipient = $aMatches[ 2 ];
						}
					}
					$oPhpmailer->AddBcc( $sRecipient, $sRecipientName );
				}
				catch ( phpmailerException $e )
				{
					continue;
				}
			}
		}

		// Set to use PHP's mail()
		$oPhpmailer->IsMail();

		// Set Content-Type and charset
		// If we don't have a content-type from the input headers
		if ( ! isset( $sContentType ) )
		{
			$sContentType = 'text/plain';
		}

		/**
		 * Filter the _mail() content type.
		 *
		 * @param string $sContentType Default _mail() content type.
		 */
		$sContentType = Service\HookService::applyHooks( '_mail_content_type', $sContentType );

		$oPhpmailer->ContentType = $sContentType;

		// Set whether it's plaintext, depending on $sContentType
		if ( 'text/html' == $sContentType )
		{
			$oPhpmailer->IsHTML( true );
		}

		// If we don't have a charset from the input headers
		if ( ! isset( $sCharset ) )
		{
			$sCharset = SITE_CHARSET;
		}

		// Set the content-type and charset

		/**
		 * Filter the default _mail() charset.
		 *
		 * @param string $sCharset Default email charset.
		 */
		$oPhpmailer->CharSet = Service\HookService::applyHooks( '_mail_charset', $sCharset );

		// Set custom headers
		if ( !empty( $sHeaders ) )
		{
			foreach( (array) $sHeaders as $sName => $sContent )
			{
				$oPhpmailer->AddCustomHeader( sprintf( '%1$s: %2$s', $sName, $sContent ) );
			}

			if ( false !== stripos( $sContentType, 'multipart' ) && ! empty( $sBoundary ) )
			{
				$oPhpmailer->AddCustomHeader( sprintf( "Content-Type: %s;\n\t boundary=\"%s\"", $sContentType, $sBoundary ) );
			}
		}

		if ( ! empty( $aAttachments ) )
		{
			foreach ( $aAttachments as $attachment )
			{
				try
				{
					$oPhpmailer->AddAttachment($attachment);
				}
				catch ( phpmailerException $e )
				{
					continue;
				}
			}
		}

		/**
		 * Fires after PHPMailer is initialized.
		 *
		 * @param PHPMailer $oPhpmailer The PHPMailer instance
		 */
		$oPhpmailer = Service\HookService::applyHooks( 'phpmailerInit', $oPhpmailer );

		// Send!
		try
		{
			return $oPhpmailer->Send();
		}
		catch ( phpmailerException $e )
		{
			return false;
		}
	}
}