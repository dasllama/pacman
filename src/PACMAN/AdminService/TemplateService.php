<?php
namespace PACMAN\AdminService;
use PACMAN\AdminService as AdminService;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;

class TemplateService
{
	/**
	 * Loads all the needed header, additional meta data
	 * and scripts, styles and so on
	 * 
	 * @return	void
	 */
	public static function loadHead()
	{
		// load the scripts
		$oScriptService = AdminService\ScriptService::getInstance();
		$aScripts = $oScriptService->getScripts( 'header' );
		if ( ! empty( $aScripts ) )
		{
			$aScriptsToLoad = array();
			foreach ( $aScripts as $sHandle => $aScriptData )
			{
				// add the script
				$aScriptsToLoad[ $sHandle ] = $aScriptData;

				// check if we have deps
				if ( ! empty( $aScriptData[ 'deps' ] ) )
				{
					AdminService\ScriptService::loadScriptDepencies( $aScriptData[ 'deps' ], $aScriptsToLoad, 'header' );
				}
			}

			$aScriptsToLoad = Service\HookService::applyHooks( 'adminScriptsToLoad', $aScriptsToLoad );
			foreach ( $aScriptsToLoad as $aScript )
			{
				echo '<script type="text/javascript" src="' . $aScript[ 'src' ] . '"></script>';
			}
		}

		// load the styles
		$oStyleService = AdminService\StyleService::getInstance();
		$aStyles = $oStyleService->getStyles( 'header' );
		if ( ! empty( $aStyles ) )
		{
			$aStylesToLoad = array();
			foreach ( $aStyles as $sHandle => $aStyleData )
			{
				// add the style
				$aStylesToLoad[ $sHandle ] = $aStyleData;

				// check if we have deps
				if ( ! empty( $aStyleData[ 'deps' ] ) )
				{
					AdminService\StyleService::loadStyleDepencies( $aStyleData[ 'deps' ], $aStylesToLoad, 'header' );
				}
			}

			$aStylesToLoad = Service\HookService::applyHooks( 'adminStylesToLoad', $aStylesToLoad );
			foreach ( $aStylesToLoad as $aStyle )
			{
				echo '<link rel="stylesheet" type="text/css" href="' . $aStyle[ 'src' ] . '">';
			}
		}

		// let other modules hook to this
		Service\HookService::execHook( 'loadAdminHead' );
	}

	/**
	 * Loads all the needed footer, additional meta data
	 * and scripts, styles and so on
	 * 
	 * @return	void
	 */
	public static function loadFoot()
	{
		// load the scripts
		$oScriptService = AdminService\ScriptService::getInstance();
		$aScripts = $oScriptService->getScripts( 'footer' );
		if ( ! empty( $aScripts ) )
		{
			$aScriptsToLoad = array();
			foreach ( $aScripts as $sHandle => $aScriptData )
			{
				// add the script
				$aScriptsToLoad[ $sHandle ] = $aScriptData;

				// check if we have deps
				if ( ! empty( $aScriptData[ 'deps' ] ) )
				{
					AdminService\ScriptService::loadScriptDepencies( $aScriptData[ 'deps' ], $aScriptsToLoad, 'footer' );
				}
			}

			$aScriptsToLoad = Service\HookService::applyHooks( 'adminFooterScriptsToLoad', $aScriptsToLoad );
			foreach ( $aScriptsToLoad as $aScript )
			{
				echo '<script type="text/javascript" src="' . $aScript[ 'src' ] . '"></script>';
			}
		}

		// let other modules hook to this
		Service\HookService::execHook( 'loadAdminFoot' );
	}
}