<?php
namespace PACMAN\AdminService;
use PACMAN\AdminService as AdminService;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;

class InitService
{
	/**
	 * The instance of this class
	 * 
	 * @var	__CLASS__
	 */
	public static $iInstance;

	/**
	 * Checks if there is an instance of this class
	 * available. If not we init one.
	 * 
	 * @return	__CLASS__
	 */
	public static function getInstance()
	{
		if ( NULL === self::$iInstance )
		{
			self::$iInstance = new self;
		}
		return self::$iInstance;
	}

	/**
	 * Inits the user system, registers the basic hook
	 * to startup with everything
	 *
	 * @pm-hook	index
	 * @return	void
	 */
	public function initAdminSystem()
	{
		// register the basic stylesheet of the template
		Service\HookService::addHook( 'admin', array( $this, 'loadStylesheet' ) );

		// register the basic scripts of the template
		Service\HookService::addHook( 'admin', array( $this, 'loadScripts' ) );
		
		// load the admin template system
		Service\HookService::addHook( 'admin', array( $this, 'loadAdminTemplateSystem' ) );

	}

	/**
	 * Loads the admin templates based on the logged in status
	 * and the given path
	 *
	 * @pm-hook	admin
	 * @return	void
	 */
	public function loadAdminTemplateSystem()
	{
		// get the current path and check what we
		// should do, maybe it is empty or not
		$sCurrentPath = Helper\Template::getCurrentPath();

		// gets the template
		$sTemplateToLoad = $this->getAdminTemplateToLoad( $sCurrentPath );

		// set up the path to the template and load it
		$sPathToTemplateFolder = Helper\General::getAdminAssetDirectory( 'templates' );
		$sTemplateFile = $sPathToTemplateFolder . $sTemplateToLoad;

		require_once $sTemplateFile;
	}

	/**
	 * Gets the needed template based on the current path
	 * 
	 * @param	string $sCurrentPath
	 * @return	string the template to load
	 */
	public function getAdminTemplateToLoad( $sCurrentPath = '/' )
	{
		/**
		 * Filter the template to load before the template is required
		 *
		 * Passing a non-null value to the filter will short-circuit
		 * checking the DB for the charset, returning that value instead.
		 *
		 * @param	boolean the check if this filter is used
		 * @param	string $sCurrentPath The character set to use. Default null.
		 */
		$mPreTemplateToLoad = Service\HookService::applyHooks( 'pre_getAdminTemplateToLoad', FALSE, $sCurrentPath );
		if ( FALSE !== $mPreTemplateToLoad )
		{
			return $mPreTemplateToLoad;
		}

		// check if the current user is logged in
		// if not, load the login template
		if ( ! Helper\User::isUserLoggedIn() && $sCurrentPath != 'login' )
		{
			// redirect the user to the login page
			Helper\Template::redirect( PM_ADMIN_URL . '?path=login' );
		}

		// load the template based on the current path
		if ( empty( $sCurrentPath ) )
		{
			// if the current path is empty we need to load the
			// index.php file which is obviously the homepage
			$sTemplateToLoad = 'dashboard.php';
		}
		else
		{
			$sTemplateToLoad =  $sCurrentPath . '.php';
		}

		return Service\HookService::applyHooks( 'adminTemplateToLoad', $sTemplateToLoad );
	}

	/**
	 * Loads the basic stylesheet file
	 *
	 * @pm-hook	admin
	 * @return	void
	 */
	public function loadStylesheet()
	{
		$oStyleService = AdminService\StyleService::getInstance();
		$oStyleService->registerStyle( 'admin', Helper\General::getAdminAssetDirectoryUrl( 'css' ) . 'admin.css' );
	}

	/**
	 * Loads the basic scripts file
	 *
	 * @pm-hook	admin
	 * @return	void
	 */
	public function loadScripts()
	{
		$oScriptService = AdminService\ScriptService::getInstance();
		$oScriptService->registerScript( 'admin', Helper\General::getAdminAssetDirectoryUrl( 'js' ) . 'admin.js' );
	}
}