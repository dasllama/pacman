<?php
namespace PACMAN\AdminService;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;

class ScriptService
{
	/**
	 * Stack holder for the currently registered scripts at
	 * the system
	 * 
	 * @var	array
	 */
	public $sScripts;

	/**
	 * Stack holder for the currently registered scripts at
	 * the system for the header
	 * 
	 * @var	array
	 */
	public $aHeaderScripts;

	/**
	 * Stack holder for the currently registered scripts at
	 * the system for the footer
	 * 
	 * @var	array
	 */
	public $aFooterScripts;

	/**
	 * The instance of this class
	 * 
	 * @var	__CLASS__
	 */
	public static $iInstance;

	/**
	 * Checks if there is an instance of this class
	 * available. If not we init one.
	 * 
	 * @return	__CLASS__
	 */
	public static function getInstance()
	{
		if ( NULL === self::$iInstance )
		{
			self::$iInstance = new self;
		}
		return self::$iInstance;
	}

	/**
	 * Registers a script to the template system
	 * to be loaded when it needs to be loaded
	 *
	 * @param	string $sHandle the identification of the script
	 * @param	string $sSrc the URL to the script
	 * @param	array $aDeps the depencies to other scripts
	 * @param	bool $bInFooter setting if the script is loaded in the footer
	 * @return	void
	 */
	public function registerScript( $sHandle, $sSrc, $aDeps = array(), $bInFooter = TRUE )
	{

		// add the script to the stack but chech if we need it in the header
		// or in the footer first
		if ( $bInFooter == TRUE )
		{
			$this->aFooterScripts[ $sHandle ] = array(
				'handle' => $sHandle,
				'src'    => $sSrc,
				'deps'   => $aDeps
			);
		}
		else
		{
			$this->aHeaderScripts[ $sHandle ] = array(
				'handle' => $sHandle,
				'src'    => $sSrc,
				'deps'   => $aDeps
			);
		}

		// add the script to the main stack to get it
		// really fast if we need it
		$this->aScripts[ $sHandle ] = array(
			'handle' => $sHandle,
			'src'    => $sSrc,
			'deps'   => $aDeps
		);
	}

	/**
	 * Get the current registered scripts at the system
	 * 
	 * @param	string $sSection the current section maybe header or footer
	 * @return	array the current scripts
	 */
	public function getScripts( $sSection = 'header' )
	{
		if ( $sSection == 'header' )
		{
			return $this->aHeaderScripts;
		}
		else
		{
			return $this->aFooterScripts;
		}
	}

	/**
	 * Gets the script by handle
	 * 
	 * @param	string $sHandle the identification of the script
	 * @return	array the script | FALSE if the script is not registered
	 */
	public function getScriptByHandle( $sHandle )
	{
		if ( isset( $this->aScripts[ $sHandle ] ) )
		{
			return $this->aScripts[ $sHandle ];
		}
		else
		{
			return FALSE;
		}
	}

	/**
	 * Loads all the depencies for a script
	 * 
	 * @param	array $aDeps the current depencies
	 * @param	array &$aScriptsToLoad the current stack for the scripts to load
	 * @param	string $sSection the current section maybe header or footer
	 * @return	void
	 */
	public static function loadScriptDepencies( $aDeps, &$aScriptsToLoad, $sSection = 'header' )
	{
		// get the scriptservice
		$oScriptService = Service\ScriptService::getInstance();

		// load the scripts
		$scripts = $oScriptService->getScripts( $sSection );

		// check deps
		foreach ( $aDeps as $sDep )
		{
			// if the dep exists we don't need to load it
			if ( isset( $scripts[ $sDep ] ) && ! empty( $scripts[ $sDep ] ) )
			{
				continue;
			}

			// the dep does not exists in the current stack
			// so we try to find it
			$aDepScript = $oScriptService->getScriptByHandle( $sDep );
			$aScriptsToLoad[ $aDepScript[ 'handle' ] ] = $aDepScript;

			// check again if we have deps
			if ( ! empty( $aDepScript[ 'deps' ] ) )
			{
				self::loadScriptDepencies( $aDepScript[ 'deps' ], $aScriptsToLoad, $sSection );
			}
		}
	}
}