<?php
namespace PACMAN\Entity;

/**
 * Represents a single error.
 * 
 * @author llama
 */
class Error
{
    /**
     * Main code of the error
     * 
     * @var string
     */
    public $sCode;

    /**
     * Message of the registered error
     * 
     * @var string
     */
    public $sMessage;

    /**
     * Check if the error is critical to terminate
     * the whole process and end PACMAN
     *
     * @var bool
     */
    public $bForceTermination;
}
