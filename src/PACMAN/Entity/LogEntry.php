<?php
namespace PACMAN\Entity;

/**
 * Represents a single log entry.
 * 
 * @author llama
 */
class LogEntry
{
    /**
     * Main code of the log entry
     * 
     * @var string
     */
    public $sCode;

    /**
     * Message of the registered log entry
     * 
     * @var string
     */
    public $sMessage;

    /**
     * the time of the log message
     *
     * @var \Date
     */
    public $oDate;
}
