<?php
namespace PACMAN\Entity;

/**
 * Represents a single plugin
 * 
 * @author llama
 */
class Plugin
{
	/**
	 * Name of the plugin
	 * 
	 * @var string
	 */
	public $sName;

	/**
	 * Description of the plugin
	 * 
	 * @var string
	 */
	public $sDescription;

	/**
	 * Current version of the plugin
	 *
	 * @var string
	 */
	public $sVersion;

	/**
     * Licence information
     *
     * @var string
     */
    public $sLicence;

    /**
     * Name of the author
     *
     * @var string
     */
    public $sAuthor;

    /**
     * URL of the author
     *
     * @var string
     */
    public $sAuthorURL;
}