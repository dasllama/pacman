<?php
namespace PACMAN\Plugins\Test;
use PACMAN\Entity as Entity;
use PACMAN\Service as Service;

/**
 * Testplugin
 *
 * @author	llama
 */
class Test extends Entity\Plugin implements Service\PluginInterface
{
	/**
	 * Name of the plugin
	 * 
	 * @var string
	 */
	public $sName = 'Testplugin';

	/**
	 * Description of the plugin
	 * 
	 * @var string
	 */
	public $sDescription = 'Testdescription';

	/**
	 * Current version of the plugin
	 *
	 * @var string
	 */
	public $sVersion = '1.0';

	/**
     * Licence information
     *
     * @var string
     */
    public $sLicence = 'GPLv3';

    /**
     * Name of the author
     *
     * @var string
     */
    public $sAuthor = 'Llama';

    /**
     * URL of the author
     *
     * @var string
     */
    public $sAuthorURL = 'http://dasllama.github.io';

    /**
	 * @inherited
	 */
    public function registerPlugin( Service\PluginService $oPluginService )
    {
    	$oPluginService->registerPlugin( $this );
    }

	/**
	 * @inherited
	 */
	public function initPlugin()
	{
		
	}
}

// kick of the plugin
return 'PACMAN\Plugins\Test\Test';