<?php
namespace PACMAN\Service;

class HookService
{
	/**
	 * Stack holder for the currently registered hooks at
	 * the system. The stack is build like this:
	 * 		$sHookName => array(
	 * 			$iPriority => array(
	 * 				$function_name_a,
	 * 				$function_name_b,
	 * 				$function_name_c,
	 * 				...
	 * 			)
	 * 		)
	 * More information are in the addHook and addAction
	 * methods below
	 * 
	 * @var	array
	 */
	public $aHooks = [];

	/**
	 * The instance of this class
	 * 
	 * @var	__CLASS__
	 */
	public static $iInstance;

	/**
	 * Checks if there is an instance of this class
	 * available. If not we init one.
	 * 
	 * @return	__CLASS__
	 */
	public static function getInstance()
	{
		if ( NULL === self::$iInstance )
		{
			self::$iInstance = new self;
		}
		return self::$iInstance;
	}

	/**
	 * Hooks a function on to a specific action.
	 *
	 * Actions are the hooks that the Llama core launches at specific points
	 * during execution, or when specific events occur. Modules can specify that
	 * one or more of its PHP functions are executed at these points, using the
	 * Action API.
	 *
	 * @param	string   $sTag            The name of the action to which the $sFunctionToAdd is hooked.
	 * @param	callback $sFunctionToAdd  The name of the function you wish to be called.
	 * @param	int      $iPriority       Optional. Used to specify the order in which the functions
	 *                                    associated with a particular action are executed. Default 10.
	 *                                    Lower numbers correspond with earlier execution,
	 *                                    and functions with the same priority are executed
	 *                                    in the order in which they were added to the action.
	 * @param	int      $iAcceptedArgs   Optional. The number of arguments the function accept. Default 1.
	 * @return	bool Will always return true.
	 */
	public function addAction( $sTag, $sFunctionToAdd, $iPriority = 10, $iAcceptedArgs = 1 )
	{
		// get the instance of the hook class
		$oHookServiceInstance = self::getInstance();

		return $oHookServiceInstance->addHook( $sTag, $sFunctionToAdd, $iPriority, $iAcceptedArgs );
	}

	/**
	 * Hook a function or method to a specific hook action.
	 *
	 * Llama offers hook hooks to allow plugins to modify
	 * various types of internal data at runtime.
	 *
	 * A plugin can modify data by binding a callback to a hook hook. When the hook
	 * is later applied, each bound callback is run in order of priority, and given
	 * the opportunity to modify a value by returning a new value.
	 *
	 * The following example shows how a callback function is bound to a hook hook.
	 *
	 * Note that `$example` is passed to the callback, (maybe) modified, then returned:
	 *
	 *     function example_callback( $example ) {
	 *         // Maybe modify $example in some way.
	 *     	   return $example;
	 *     }
	 *     addHook( 'example_hook', 'example_callback' );
	 *
	 * *Note:* the function will return true whether or not the callback is valid.
	 * It is up to you to take care. This is done for optimization purposes,
	 * so everything is as quick as possible.
	 *
	 * @param	string   $sTag            The name of the hook to hook the $sFunctionToAdd callback to.
	 * @param	callback $sFunctionToAdd  The callback to be run when the hook is applied.
	 * @param	int      $iPriority       Optional. Used to specify the order in which the functions
	 *                                    associated with a particular action are executed. Default 10.
	 *                                    Lower numbers correspond with earlier execution,
	 *                                    and functions with the same priority are executed
	 *                                    in the order in which they were added to the action.
	 * @param	int      $iAcceptedArgs   Optional. The number of arguments the function accepts. Default 1.
	 * @return	boolean true
	 */
	public static function addHook( $sTag, $sFunctionToAdd, $iPriority = 10, $iAcceptedArgs = 1 )
	{
		// get the instance of the hook class
		$oHookServiceInstance = self::getInstance();

		// set an uniqe index
		$sIndex = uniqid( md5( $sTag . $iPriority ) );

		// add our action / hook to the stack
		$oHookServiceInstance->aHooks[ $sTag ][ $iPriority ][ $sIndex ] = array(
			'function'      => $sFunctionToAdd,
			'accepted_args' => $iAcceptedArgs
		);

		return TRUE;
	}

	/**
	 * Removes an action from the hook stack
	 * 
	 * @param	string   $sTag            The name of the action to which the $sFunctionToAdd is hooked.
	 * @param	callback $sFunctionToAdd  The name of the function you wish to be called.
	 * @param	int      $iPriority       Optional. Used to specify the order in which the functions
	 *                                    associated with a particular action are executed. Default 10.
	 *                                    Lower numbers correspond with earlier execution,
	 *                                    and functions with the same priority are executed
	 *                                    in the order in which they were added to the action.
	 * @return	bool TRUE
	 */
	public static function removeAction( $sTag, $sFunctionToAdd, $iPriority = 10 )
	{
		// get the instance of the hook class
		$oHookServiceInstance = self::getInstance();

		return $oHookServiceInstance->removeHook( $sTag, $sFunctionToAdd, $iPriority );
	}

	/**
	 * Removes a hook or action from the hook stack
	 * 
	 * @param	string   $sTag            The name of the action to which the $sFunctionToAdd is hooked.
	 * @param	callback $sFunctionToAdd  The name of the function you wish to be called.
	 * @param	int      $iPriority       Optional. Used to specify the order in which the functions
	 *                                    associated with a particular action are executed. Default 10.
	 *                                    Lower numbers correspond with earlier execution,
	 *                                    and functions with the same priority are executed
	 *                                    in the order in which they were added to the action.
	 * @return	bool TRUE
	 */
	public static function removeHook( $sTag, $sFunctionToAdd, $iPriority = 10 )
	{
		// get the instance of the hook class
		$oHookServiceInstance = self::getInstance();

		// search for the hook with the given priority
		if ( count( $oHookServiceInstance->aHooks[ $sTag ][ $iPriority ] ) == 0 )
		{
			return TRUE;
		}

		// walk the hooks to determinate which one we need
		foreach ( $oHookServiceInstance->aHooks[ $sTag ][ $iPriority ] as $sUniqeId => $aHook )
		{
			// if the function is the same as the given one we
			// need to remove this hook from our stack
			if ( $aHook[ 'function' ] === $sFunctionToAdd )
			{
				unset( $oHookServiceInstance->aHooks[ $sTag ][ $iPriority ][ $sUniqeId ] );
			}
		}

		return TRUE;
	}

	/**
	 * Call the functions added to a hook hook.
	 *
	 * The callback functions attached to hook hook $sTag are invoked by calling
	 * this function. This function can be used to create a new hook hook by
	 * simply calling this function with the name of the new hook specified using
	 * the $sTag parameter.
	 *
	 * The function allows for additional arguments to be added and passed to hooks.
	 *
	 *     // Our hook callback function
	 *     function example_callback( $string, $arg1, $arg2 ) {
	 *         // (maybe) modify $string
	 *         return $string;
	 *     }
	 *     addHook( 'example_hook', 'example_callback', 10, 3 );
	 *
	 *     /*
	 *      * Apply the hooks by calling the 'example_callback' function we
	 *      * "hooked" to 'example_hook' using the addHook() function above.
	 *      * - 'example_hook' is the hook hook $sTag
	 *      * - 'hook me' is the value being hooked
	 *      * - $arg1 and $arg2 are the additional arguments passed to the callback.
	 *     $value = applyHooks( 'example_hook', 'hook me', $arg1, $arg2 );
	 *
	 *
	 * @param	string $sTag  The name of the hook hook.
	 * @param	mixed  $value The value on which the hooks hooked to `$sTag` are applied on.
	 * @param	mixed  $var   Additional variables passed to the functions hooked to `$sTag`.
	 * @return	mixed         The hooked value after all hooked functions are applied to it.
	 */
	public static function applyHooks( $sTag, $mValue )
	{
		// get the instance of the hook class
		$oHookServiceInstance = self::getInstance();

		// determinate if our tag is in the hooks
		if ( ! isset( $oHookServiceInstance->aHooks[ $sTag ] ) )
		{
			return $mValue;
		}

		// get the hooks and sort them the correct way
		ksort( $oHookServiceInstance->aHooks[ $sTag ] );

		// set the arguments
		$aArgs = array();
		if ( empty( $aArgs ) )
		{
			$aArgs = func_get_args();
		}

		// set the hooks to deplay
		foreach ( $oHookServiceInstance->aHooks[ $sTag ] as $aCurrentHooks )
		{
			foreach ( $aCurrentHooks as $aCurrentHook )
			{
				if ( ! is_null( $aCurrentHook[ 'function' ] ) )
				{
					$aArgs[ 1 ] = $mValue;
					$mValue = call_user_func_array( $aCurrentHook[ 'function' ], array_slice( $aArgs, 1, (int) $aCurrentHook[ 'accepted_args' ] ) );
				}
			}
		}

		return $mValue;
	}

	/**
	 * Execute functions hooked on a specific ´hook.
	 *
	 * This function invokes all functions attached to hook `$sTag`. It is
	 * possible to create new hooks by simply calling this function,
	 * specifying the name of the new hook using the `$sTag` parameter.
	 *
	 * You can pass extra arguments to the hooks, much like you can with
	 * {@see applyHooks()}.
	 *
	 * @param	string $sTag The name of the action to be executed.
	 * @param	mixed  $aArgs Optional. Additional arguments which are passed on to the
	 *                       functions hooked to the action. Default empty.
	 * @return	null         Will return null if $sTag does not exist in $wp_hook array.
	 */
	public static function execHook( $sTag, $aArgs = '' )
	{
		// get the instance of the hook class
		$oHookServiceInstance = self::getInstance();

		// determinate if our tag is in the hooks
		if ( ! isset( $oHookServiceInstance->aHooks[ $sTag ] ) )
		{
			return;
		}

		// get the hooks and sort them the correct way
		ksort( $oHookServiceInstance->aHooks[ $sTag ] );

		// get the arguments of this tag
		$aArguments = array();
		if ( is_array( $aArgs ) && 1 == count( $aArgs ) && isset( $aArgs[ 0 ] ) && is_object( $aArgs[ 0 ] ) )
		{
			$aArguments[] =& $aArgs[0];
		}
		else{
			$aArguments[] = $aArgs;
		}

		// build the arguments array
		for ( $iA = 2, $num = func_num_args(); $iA < $num; $iA++ )
		{
			$aArguments[] = func_get_arg( $iA );
		}

		// set the hooks to deplay
		foreach ( $oHookServiceInstance->aHooks[ $sTag ] as $aCurrentHooks )
		{
			foreach ( $aCurrentHooks as $aCurrentHook )
			{
				if ( ! is_null( $aCurrentHook[ 'function' ] ) )
				{
					call_user_func_array( $aCurrentHook[ 'function' ], array_slice( $aArguments, 0, (int) $aCurrentHook[ 'accepted_args' ] ) );
				}
			}
		}
	}
}