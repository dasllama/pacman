<?php
namespace PACMAN\Service;
use PACMAN\Service as Service;
use PACMAN\Entity as Entity;

class ErrorService
{
	/**
	 * Stack holder for the currently registered errors at
	 * the system.
	 * 
	 * @var	array
	 */
	public $aErrors = [];

	/**
	 * The instance of this class
	 * 
	 * @var	__CLASS__
	 */
	public static $iInstance;

	/**
	 * Checks if there is an instance of this class
	 * available. If not we init one.
	 * 
	 * @return	__CLASS__
	 */
	public static function getInstance()
	{
		if ( NULL === self::$iInstance )
		{
			self::$iInstance = new self;
		}
		return self::$iInstance;
	}

	/**
	 * Adds an error to the error system
	 * 
	 * @param string  $sCode             Main code of the error
	 * @param string  $sMessage          Message of the registered error
	 * @param boolean $bSuppressLog      Checks if this error should be registered in the log
	 * @param boolean $bForceTermination Check if the error is critical to terminate the whole process and end PACMAN
	 *
	 * @return Error[]
	 */
	public static function addError( $sCode, $sMessage, $bSuppressLog = FALSE, $bForceTermination = FALSE )
	{
		// get the instance of the hook class
		$oErrorServiceInstance = self::getInstance();

		// adbuild the entity
		$oError = new Entity\Error();
		$oError->sCode = $sCode;
		$oError->sMessage = $sMessage;
		$oError->bForceTermination = $bForceTermination;

		// add the error to the stack
		$oErrorServiceInstance->aErrors[] = $oError;

		// check if we need this message in the log
		if ( $bSuppressLog !== FALSE )
		{
			Service\LogService::addLogEntry( 'error', 'Code: '. $sCode .'\tMessage: '. $sMessage );
		}

		// if the termination is TRUE, we need to kill this whole thing
		// and echo the error
		if ( $bForceTermination === TRUE )
		{
			// remove all the previous content
			ob_clean();

			// display our error message and die
			echo '<p><strong>An error occured!</strong></p>';
			echo '<p><em>Code:</em> '. $sCode .'</p>';
			echo '<p><em>Error:</em> '. $sMessage .'</p>';
			exit;
		}

		// return the error, maybe someone want to use it
		return $oError;
	}
}