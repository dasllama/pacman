<?php
namespace PACMAN\Service;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;

class ContentService
{
	/**
	 * The instance of this class
	 * 
	 * @var	__CLASS__
	 */
	public static $iInstance;

	/**
	 * Checks if there is an instance of this class
	 * available. If not we init one.
	 * 
	 * @return	__CLASS__
	 */
	public static function getInstance()
	{
		if ( NULL === self::$iInstance )
		{
			self::$iInstance = new self;
		}
		return self::$iInstance;
	}

	/**
	 * Loads the whole content data
	 * 
	 * @param	mixed $mIdOrPath
	 * @return	object
	 */
	public static function getContent( $mIdOrPath )
	{
		// check static cache
		static $aContentStack;
		if ( isset( $aContentStack[ $mIdOrPath ] ) )
		{
			return $aContentStack[ $mIdOrPath ];
		}

		// validate input
		if ( empty( $mIdOrPath ) )
		{
			// TODO throw error
			return FALSE;
		}

		// everything seems to be fine, so we can build a query
		$oDb = Helper\General::getDatabase();

		// check if this is an id or string
		if ( is_int( $mIdOrPath ) )
		{
			$oContent = $oDb->getRow( $oDb->prepare( 'SELECT * FROM content WHERE `id` = %d', $mIdOrPath ) );
		}
		else
		{
			$oContent = $oDb->getRow( $oDb->prepare( 'SELECT * FROM content WHERE `path` = %s', $mIdOrPath ) );
		}
		
		// we don't have anything
		if ( empty( $oContent ) )
		{
			return FALSE;
		}

		// get the field data and append it to the content
		$oFieldData = $oDb->getResults( $oDb->prepare( 'SELECT * FROM content_data WHERE `content_id` = %d', $oContent->id ) );
		foreach ( $oFieldData as $oField )
		{
			$sFieldName = $oField->key;
			$oContent->{$sFieldName} = Helper\Formatting::maybeUnserialize( $oField->value );
		}

		// set the stack
		$aContentStack[ $mIdOrPath ] = $oContent;
		return $oContent;
	}

	/**
	 * Builds an standard object with the current content
	 * which contains a title, meta informations, the path
	 * and other stuff we need. If no path is set then we
	 * asume that we have a 
	 * 
	 * @return	object
	 */
	public static function getCurrentContent()
	{
		// get the current path to determinate what the current
		// selected content is
		$sCurrentPath = Helper\Template::getCurrentPath();
		if ( empty( $sCurrentPath ) )
		{
			// we got an empty path so we are at the index page
			$oContent = new \stdClass();
			$oContent->title = 'Home';
			$oContent->type = 'archive';
			$oContent->template = 'index.php';
			return $oContent;
		}

		// get the content object by the path
		$oContent = self::getContent( $sCurrentPath );
		if ( empty( $oContent ) || $oContent == FALSE )
		{
			// check if we got a 404
			$oContent = new \stdClass();
			$oContent->title = 'Content not found';
			$oContent->type = 'archive';
			$oContent->template = '404.php';
			return $oContent;
		}

		// everything is fine so we just return the content object
		return $oContent;
	}
}