<?php
namespace PACMAN\Service;
use PACMAN\Helper as Helper;

abstract class DatabaseServiceAbstract
{
	/**
	 * Whether to show SQL/DB errors.
	 *
	 * Default behavior is to show errors if both
	 * BASE_DEBUG and BASE_DEBUG_DISPLAY
	 * evaluated to TRUE.
	 *
	 * @var	bool
	 */
	var $bShowErrors = FALSE;

	/**
	 * Whether to suppress errors during the DB bootstrapping.
	 *
	 * @var	bool
	 */
	var $bSuppressErrors = FALSE;

	/**
	 * The last error during query.
	 *
	 * @var	string
	 */
	public $sLastError = '';

	/**
	 * Last query made
	 *
	 * @var	array
	 */
	var $aLastQuery;

	/**
	 * Results of the last query made
	 *
	 * @var	array|null
	 */
	var $aLastResult;

	/**
	 * Amount of queries made
	 *
	 * @var	int
	 */
	public $iNumQueries = 0;

	/**
	 * Count of rows returned by previous query
	 *
	 * @var	int
	 */
	public $iNumRows = 0;

	/**
	 * Count of affected rows by previous query
	 *
	 * @var	int
	 */
	var $iRowsAffected = 0;

	/**
	 * The ID generated for an AUTO_INCREMENT column by
	 * the previous query (usually INSERT).
	 *
	 * @var	int
	 */
	public $iInsertId = 0;

	/**
	 * MySQL result, which is either a resource or boolean.
	 *
	 * @var	mixed
	 */
	protected $mResult;

	/**
	 * G!Galarm table prefix
	 *
	 * You can set this to have multiple G!Galarm installations
	 * in a single database. The second reason is for possible
	 * security precautions.
	 *
	 * @var	string
	 */
	public $sPrefix = '';

	/**
	 * Database table columns charset
	 *
	 * @var	string
	 */
	public $sCharset;

	/**
	 * Calculated character sets on tables
	 *
	 * @var	array
	 */
	protected $aTableCharset = array();

	/**
	 * Cached column info, for sanity checking data before inserting
	 *
	 * @var	array
	 */
	protected $aColMeta = array();

	/**
	 * Format specifiers for DB columns. Columns not listed here default to %s. Initialized during G!Galarm load.
	 * Keys are column names, values are format types: 'ID' => '%d'
	 *
	 * @var	array
	 */
	public $aFieldTypes = array();

	/**
	 * Database table columns collate
	 *
	 * @var	string
	 */
	public $sCollate;

	/**
	 * Database Username
	 *
	 * @var	string
	 */
	protected $sDBUser;

	/**
	 * Database Password
	 *
	 * @var	string
	 */
	protected $sDBPassword;

	/**
	 * Database Name
	 *
	 * @var	string
	 */
	protected $sDBName;

	/**
	 * Database Host
	 *
	 * @var	string
	 */
	protected $sDBHost;

	/**
	 * Database Handle
	 *
	 * @var	string
	 */
	protected $rDBHandle;

	/**
	 * Whether we've managed to successfully connect at some point
	 *
	 * @var	bool
	 */
	private $bHasConnected = FALSE;

	/**
	 * Whether text fields in the current query need to be sanity checked.
	 *
	 * @var	bool
	 */
	protected $bCheckCurrentQuery = TRUE;

	/**
	 * Flag to ensure we don't run into recursion problems when checking the collation.
	 *
	 * @var	bool
	 */
	private $bCheckingCollation = FALSE;

	/**
	 * Flag to ensure that the database is ready
	 *
	 * @var	bool
	 */
	private $bReady;

	/**
	 * The number of times to retry reconnecting before dying.
	 *
	 * @var	int
	 */
	protected $iReconnectRetries = 5;

	/**
	 * Insert a row into a table.
	 *
	 *     DB::insert( 'table', array( 'column' => 'foo', 'field' => 'bar' ) )
	 *     DB::insert( 'table', array( 'column' => 'foo', 'field' => 1337 ), array( '%s', '%d' ) )
	 *
	 * @param	string       $sTable  Table name
	 * @param	array        $aData   Data to insert (in column => value pairs).
	 *                               Both $aData columns and $aData values should be "raw" (neither should be SQL escaped).
	 * @param	array|string $mFormat Optional. An array of formats to be mapped to each of the value in $aData.
	 *                               If string, that format will be used for all of the values in $aData.
	 *                               A format is one of '%d', '%f', '%s' (integer, float, string).
	 *                               If omitted, all values in $aData will be treated as strings unless otherwise specified in DB::$aFieldTypes.
	 * @return	int|FALSE The number of rows inserted, or FALSE on error.
	 */
	public function insert( $sTable, $aData, $mFormat = NULL )
	{
		return $this->insertReplaceHelper( $sTable, $aData, $mFormat, 'INSERT' );
	}

	/**
	 * Replace a row into a table.
	 *
	 *     DB::replace( 'table', array( 'column' => 'foo', 'field' => 'bar' ) )
	 *     DB::replace( 'table', array( 'column' => 'foo', 'field' => 1337 ), array( '%s', '%d' ) )
	 *
	 * @param	string       $sTable  Table name
	 * @param	array        $aData   Data to insert (in column => value pairs).
	 *                               Both $aData columns and $aData values should be "raw" (neither should be SQL escaped).
	 * @param	array|string $mFormat Optional. An array of formats to be mapped to each of the value in $aData.
	 *                               If string, that format will be used for all of the values in $aData.
	 *                               A format is one of '%d', '%f', '%s' (integer, float, string).
	 *                               If omitted, all values in $aData will be treated as strings unless otherwise specified in DB::$aFieldTypes.
	 * @return	int|FALSE The number of rows affected, or FALSE on error.
	 */
	public function replace( $sTable, $aData, $mFormat = NULL )
	{
		return $this->insertReplaceHelper( $sTable, $aData, $mFormat, 'REPLACE' );
	}

	/**
	 * Processes arrays of field/value pairs and field formats.
	 *
	 * This is a helper method for DB's CRUD methods, which take field/value
	 * pairs for inserts, updates, and where clauses. This method first pairs
	 * each value with a format. Then it determines the charset of that field,
	 * using that to determine if any invalid text would be stripped. If text is
	 * stripped, then field processing is rejected and the query fails.
	 *
	 * @param	string $sTable  Table name.
	 * @param	array  $aData   Field/value pair.
	 * @param	mixed  $mFormat Format for each field.
	 * @return	array|FALSE Returns an array of fields that contain paired values
	 *                      and formats. Returns FALSE for invalid values.
	 */
	protected function processFields( $sTable, $aData, $mFormat )
	{
		$aData = $this->processFieldFormats( $aData, $mFormat );
		if ( FALSE === $aData )
		{
			return FALSE;
		}

		$aData = $this->processFieldCharsets( $aData, $sTable );
		if ( FALSE === $aData )
		{
			return FALSE;
		}

		$aData = $this->processFieldLengths( $aData, $sTable );
		if ( FALSE === $aData )
		{
			return FALSE;
		}

		$aConvertedData = $this->stripInvalidText( $aData );
		if ( $aData !== $aConvertedData )
		{
			return FALSE;
		}

		return $aData;
	}

	/**
	 * Prepares arrays of value/format pairs as passed to DB CRUD methods.
	 *
	 * @param	array $aData   Array of fields to values.
	 * @param	mixed $mFormat Formats to be mapped to the values in $aData.
	 * @return	array Array keyed by field names with values being an array
	 *                      of 'value' and 'format' keys.
	 */
	protected function processFieldFormats( $aData, $mFormat )
	{
		$aFormats = $aOriginalFormats = (array) $mFormat;

		foreach ( $aData as $sField => $aValue )
		{
			$aValue = array(
				'value'  => $aValue,
				'format' => '%s',
			);

			if ( ! empty( $mFormat ) )
			{
				$aValue[ 'format' ] = array_shift( $aFormats );
				if ( ! $aValue[ 'format' ] )
				{
					$aValue[ 'format' ] = reset( $aOriginalFormats );
				}
			}
			else if ( isset( $this->aFieldTypes[ $sField ] ) )
			{
				$aValue[ 'format' ] = $this->aFieldTypes[ $sField ];
			}

			$aData[ $sField ] = $aValue;
		}

		return $aData;
	}

	/**
	 * Adds field charsets to field/value/format arrays generated by
	 * the DB::process_field_formats() method.
	 *
	 * @param	array  $aData  As it comes from the DB::process_field_formats() method.
	 * @param	string $sTable Table name.
	 * @return	array|FALSE The same array as $aData with additional 'charset' keys.
	 */
	protected function processFieldCharsets( $aData, $sTable )
	{
		foreach ( $aData as $sField => $aValue )
		{
			// We can skip this field if we know it isn't a string.
			// This checks %d/%f versus ! %s because it's sprintf() could take more.
			if ( '%d' === $aValue[ 'format' ] || '%f' === $aValue[ 'format' ] )
			{
				$aValue[ 'charset' ] = FALSE;
			}
			else
			{
				$aValue[ 'charset' ] = $this->getColCharset( $sTable, $sField );
			}

			$aData[ $sField ] = $aValue;
		}

		return $aData;
	}

	/**
	 * For string fields, record the maximum string length that field can safely save.
	 *
	 * @param	array  $aData  As it comes from the DB::process_field_charsets() method.
	 * @param	string $sTable Table name.
	 * @return	array|FALSE The same array as $aData with additional 'length' keys, or FALSE if
	 *                      any of the values were too long for their corresponding field.
	 */
	protected function processFieldLengths( $aData, $sTable )
	{
		foreach ( $aData as $sField => $aValue )
		{
			if ( '%d' === $aValue[ 'format' ] || '%f' === $aValue[ 'format' ] )
			{
				// We can skip this field if we know it isn't a string.
				// This checks %d/%f versus ! %s because it's sprintf() could take more.
				$aValue[ 'length' ] = FALSE;
			}
			else
			{
				$aValue[ 'length' ] = $this->getColLength( $sTable, $sField );
				if ( Helper\General::isError( $aValue[ 'length' ] ) )
				{
					return FALSE;
				}
			}

			$aData[ $sField ] = $aValue;
		}

		return $aData;
	}

	/**
	 * Check if a string is ASCII.
	 *
	 * The negative regex is faster for non-ASCII strings, as it allows
	 * the search to finish as soon as it encounters a non-ASCII character.
	 *
	 * @param	string $sString String to check.
	 * @return	bool True if ASCII, FALSE if not.
	 */
	protected function checkAscii( $sString )
	{
		if ( function_exists( 'mb_check_encoding' ) )
		{
			if ( mb_check_encoding( $sString, 'ASCII' ) )
			{
				return TRUE;
			}
		}
		else if ( ! preg_match( '/[^\x00-\x7F]/', $sString ) )
		{
			return TRUE;
		}

		return FALSE;
	}

	/**
	 * Strips any invalid characters from the query.
	 *
	 * @param	string $sQuery Query to convert.
	 * @return	string|error
	 */
	protected function stripInvalidTextFromQuery( $sQuery )
	{

		// We don't need to check the collation for queries that don't read data.
		$sTrimmedQuery = ltrim( $sQuery, "\r\n\t (" );
		if ( preg_match( '/^(?:SHOW|DESCRIBE|DESC|EXPLAIN|CREATE)\s/i', $sTrimmedQuery ) )
		{
			return $sQuery;
		}

		$sTable = $this->getTableFromQuery( $sQuery );
		if ( $sTable )
		{
			$sCharset = $this->getTableCharset( $sTable );

			// We can't reliably strip text from tables containing binary/blob columns
			if ( 'binary' === $sCharset )
			{
				return $sQuery;
			}
		}
		else
		{
			$sCharset = $this->sCharset;
		}

		$aData = array(
			'value'   => $sQuery,
			'charset' => $sCharset,
			'ascii'   => FALSE,
			'length'  => FALSE,
		);

		$aData = $this->stripInvalidText( array( $aData ) );
		if ( Helper\General::isError( $aData ) )
		{
			return $aData;
		}

		return $aData[ 0 ][ 'value' ];
	}

	/**
	 * Strips any invalid characters based on value/charset pairs.
	 *
	 * @param	array $aData Array of value arrays. Each value array has the keys
	 *                    'value' and 'charset'. An optional 'ascii' key can be
	 *                    set to FALSE to avoid redundant ASCII checks.
	 * @return	array|die
	 */
	protected function stripInvalidText( $aData )
	{
		$bDBCheckString = FALSE;

		foreach ( $aData as &$aValue )
		{
			$sCharset = $aValue[ 'charset' ];

			if ( is_array( $aValue[ 'length' ] ) )
				$iLength = $aValue[ 'length' ][ 'length' ];
			else
				$iLength = FALSE;

			// There's no charset to work with.
			if ( FALSE === $sCharset )
				continue;

			// Column isn't a string.
			if ( ! is_string( $aValue[ 'value' ] ) )
				continue;

			$bTruncateByByteLength = 'byte' === $aValue[ 'length' ][ 'type' ];

			$bNeedsValidation = TRUE;
			if (
				// latin1 can store any byte sequence
				'latin1' === $sCharset
			||
				// ASCII is always OK.
				( ! isset( $aValue[ 'ascii' ] ) && $this->checkAscii( $aValue[ 'value' ] ) )
			)
			{
				$bTruncateByByteLength = TRUE;
				$bNeedsValidation = FALSE;
			}

			if ( $bTruncateByByteLength )
			{
				Helper\General::mbstringBinarySafeEncoding();
				if ( FALSE !== $iLength && strlen( $aValue[ 'value' ] ) > $iLength )
				{
					$aValue[ 'value' ] = substr( $aValue[ 'value' ], 0, $iLength );
				}
				Helper\General::resetMbstringEncoding();

				if ( ! $bNeedsValidation )
				{
					continue;
				}
			}

			// utf8 can be handled by regex, which is a bunch faster than a DB lookup.
			if ( ( 'utf8' === $sCharset || 'utf8mb3' === $sCharset || 'utf8mb4' === $sCharset ) && function_exists( 'mb_strlen' ) )
			{
				$sRegex = '/
					(
						(?: [\x00-\x7F]                  # single-byte sequences   0xxxxxxx
						|   [\xC2-\xDF][\x80-\xBF]       # double-byte sequences   110xxxxx 10xxxxxx
						|   \xE0[\xA0-\xBF][\x80-\xBF]   # triple-byte sequences   1110xxxx 10xxxxxx * 2
						|   [\xE1-\xEC][\x80-\xBF]{2}
						|   \xED[\x80-\x9F][\x80-\xBF]
						|   [\xEE-\xEF][\x80-\xBF]{2}';

				if ( 'utf8mb4' === $sCharset )
				{
					$sRegex .= '
						|    \xF0[\x90-\xBF][\x80-\xBF]{2} # four-byte sequences   11110xxx 10xxxxxx * 3
						|    [\xF1-\xF3][\x80-\xBF]{3}
						|    \xF4[\x80-\x8F][\x80-\xBF]{2}
					';
				}

				$sRegex .= '){1,40}                          # ...one or more times
					)
					| .                                  # anything else
					/x';
				$aValue[ 'value' ] = preg_replace( $sRegex, '$1', $aValue[ 'value' ] );


				if ( FALSE !== $iLength && mb_strlen( $aValue[ 'value' ], 'UTF-8' ) > $iLength )
				{
					$aValue[ 'value' ] = mb_substr( $aValue[ 'value' ], 0, $iLength, 'UTF-8' );
				}
				continue;
			}

			// We couldn't use any local conversions, send it to the DB.
			$aValue[ 'db' ] = $bDBCheckString = TRUE;
		}
		unset( $aValue ); // Remove by reference.

		if ( $bDBCheckString )
		{
			$queries = array();
			foreach ( $aData as $col => $aValue )
			{
				if ( ! empty( $aValue[ 'db' ] ) )
				{
					if ( ! isset( $queries[ $aValue[ 'charset' ] ] ) )
					{
						$queries[ $aValue[ 'charset' ] ] = array();
					}

					// We're going to need to truncate by characters or bytes, depending on the length value we have.
					if ( 'byte' === $aValue[ 'length' ][ 'type' ] )
					{
						// Split the CONVERT() calls by charset, so we can make sure the connection is right
						$queries[ $aValue[ 'charset' ] ][ $col ] = $this->prepare( "CONVERT( LEFT( CONVERT( %s USING binary ), %d ) USING {$aValue[ 'charset' ]} )", $aValue[ 'value' ], $aValue[ 'length' ][ 'length' ] );
					}
					else
					{
						$queries[ $aValue[ 'charset' ] ][ $col ] = $this->prepare( "LEFT( CONVERT( %s USING {$aValue[ 'charset' ]} ), %d )", $aValue[ 'value' ], $aValue[ 'length' ][ 'length' ] );
					}

					unset( $aData[ $col ][ 'db' ] );
				}
			}

			$sConnectionCharset = $this->sCharset;
			foreach ( $queries as $sCharset => $sQuery )
			{
				if ( ! $sQuery )
					continue;

				// Change the charset to match the string(s) we're converting
				if ( $sCharset !== $sConnectionCharset )
				{
					$sConnectionCharset = $sCharset;
					$this->setCharset( $this->rDBHandle, $sCharset );
				}

				$this->bCheckCurrentQuery = FALSE;

				$aSQL = array();
				foreach ( $sQuery as $sColumn => $sColumn_query )
				{
					$aSQL[] = $sColumn_query . " AS x_$sColumn";
				}

				$aRow = $this->getRow( "SELECT " . implode( ', ', $aSQL ), ARRAY_A );
				if ( ! $aRow )
				{
					$this->setCharset( $this->rDBHandle, $sConnectionCharset );
					Service\ErrorService::addError( 'invalid_strip_text', 'Strip text failed', TRUE, TRUE );
				}

				foreach ( array_keys( $sQuery ) as $sColumn )
				{
					$aData[ $sColumn ][ 'value' ] = $aRow[ "x_$sColumn" ];
				}
			}

			// Don't forget to change the charset back!
			if ( $sConnectionCharset !== $this->sCharset )
			{
				$this->setCharset( $this->rDBHandle );
			}
		}

		return $aData;
	}

	/**
	 * Check if the query is accessing a collation considered safe on the current version of MySQL.
	 *
	 * @param	string $sQuery The query to check.
	 * @return	bool True if the collation is safe, FALSE if it isn't.
	 */
	protected function checkSafeCollation( $sQuery )
	{
		if ( $this->bCheckingCollation )
		{
			return TRUE;
		}

		// We don't need to check the collation for queries that don't read data.
		$sQuery = ltrim( $sQuery, "\r\n\t (" );
		if ( preg_match( '/^(?:SHOW|DESCRIBE|DESC|EXPLAIN|CREATE)\s/i', $sQuery ) )
		{
			return TRUE;
		}

		// All-ASCII queries don't need extra checking.
		if ( $this->checkAscii( $sQuery ) )
		{
			return TRUE;
		}

		$sTable = $this->getTableFromQuery( $sQuery );
		if ( ! $sTable )
		{
			return FALSE;
		}

		$this->bCheckingCollation = TRUE;
		$sCollation = $this->getTableCharset( $sTable );
		$this->bCheckingCollation = FALSE;

		// Tables with no collation, or latin1 only, don't need extra checking.
		if ( FALSE === $sCollation || 'latin1' === $sCollation )
		{
			return TRUE;
		}

		$sTable = strtolower( $sTable );
		if ( empty( $this->aColMeta[ $sTable ] ) )
		{
			return FALSE;
		}

		// If any of the columns don't have one of these collations, it needs more sanity checking.
		foreach( $this->aColMeta[ $sTable ] as $oCol )
		{
			if ( empty( $oCol->Collation ) )
			{
				continue;
			}

			if ( ! in_array( $col->Collation, array( 'utf8_general_ci', 'utf8_bin', 'utf8mb4_general_ci', 'utf8mb4_bin' ), TRUE ) )
			{
				return FALSE;
			}
		}
		return TRUE;
	}

	/**
	 * Find the first table name referenced in a query.
	 *
	 * @param	string $sQuery The query to search.
	 * @return	string|FALSE $sTable The table name found, or FALSE if a table couldn't be found.
	 */
	protected function getTableFromQuery( $sQuery )
	{

		// Remove characters that can legally trail the table name.
		$sQuery = rtrim( $sQuery, ';/-#' );

		// Allow (select...) union [...] style queries. Use the first query's table name.
		$sQuery = ltrim( $sQuery, "\r\n\t (" );

		/*
		 * Strip everything between parentheses except nested selects and use only 1,000
		 * chars of the query.
		 */
		$sQuery = preg_replace( '/\((?!\s*select)[^(]*?\)/is', '()', substr( $sQuery, 0, 1000 ) );

		// Quickly match most common queries.
		if ( preg_match( '/^\s*(?:'
				. 'SELECT.*?\s+FROM'
				. '|INSERT(?:\s+LOW_PRIORITY|\s+DELAYED|\s+HIGH_PRIORITY)?(?:\s+IGNORE)?(?:\s+INTO)?'
				. '|REPLACE(?:\s+LOW_PRIORITY|\s+DELAYED)?(?:\s+INTO)?'
				. '|UPDATE(?:\s+LOW_PRIORITY)?(?:\s+IGNORE)?'
				. '|DELETE(?:\s+LOW_PRIORITY|\s+QUICK|\s+IGNORE)*(?:\s+FROM)?'
				. ')\s+((?:[0-9a-zA-Z$_.`]|[\xC2-\xDF][\x80-\xBF])+)/is', $sQuery, $aMaybe ) )
		{
			return str_replace( '`', '', $aMaybe[1] );
		}

		// SHOW TABLE STATUS and SHOW TABLES
		if ( preg_match( '/^\s*(?:'
				. 'SHOW\s+TABLE\s+STATUS.+(?:LIKE\s+|WHERE\s+Name\s*=\s*)'
				. '|SHOW\s+(?:FULL\s+)?TABLES.+(?:LIKE\s+|WHERE\s+Name\s*=\s*)'
				. ')\W((?:[0-9a-zA-Z$_.`]|[\xC2-\xDF][\x80-\xBF])+)\W/is', $sQuery, $aMaybe ) )
		{
			return str_replace( '`', '', $aMaybe[1] );
		}

		// Big pattern for the rest of the table-related queries.
		if ( preg_match( '/^\s*(?:'
				. '(?:EXPLAIN\s+(?:EXTENDED\s+)?)?SELECT.*?\s+FROM'
				. '|DESCRIBE|DESC|EXPLAIN|HANDLER'
				. '|(?:LOCK|UNLOCK)\s+TABLE(?:S)?'
				. '|(?:RENAME|OPTIMIZE|BACKUP|RESTORE|CHECK|CHECKSUM|ANALYZE|REPAIR).*\s+TABLE'
				. '|TRUNCATE(?:\s+TABLE)?'
				. '|CREATE(?:\s+TEMPORARY)?\s+TABLE(?:\s+IF\s+NOT\s+EXISTS)?'
				. '|ALTER(?:\s+IGNORE)?\s+TABLE'
				. '|DROP\s+TABLE(?:\s+IF\s+EXISTS)?'
				. '|CREATE(?:\s+\w+)?\s+INDEX.*\s+ON'
				. '|DROP\s+INDEX.*\s+ON'
				. '|LOAD\s+DATA.*INFILE.*INTO\s+TABLE'
				. '|(?:GRANT|REVOKE).*ON\s+TABLE'
				. '|SHOW\s+(?:.*FROM|.*TABLE)'
				. ')\s+\(*\s*((?:[0-9a-zA-Z$_.`]|[\xC2-\xDF][\x80-\xBF])+)\s*\)*/is', $sQuery, $aMaybe ) )
		{
			return str_replace( '`', '', $aMaybe[1] );
		}

		return FALSE;
	}

	/**
	 * Retrieves the character set for the given table.
	 *
	 * @param	string $sTable Table name.
	 * @return	string|die
	 */
	protected function getTableCharset( $sTable )
	{
		$sTablekey = strtolower( $sTable );

		// check if we have the charset in the stack
		if ( isset( $this->aTableCharset[ $sTablekey ] ) )
		{
			return $this->aTableCharset[ $sTablekey ];
		}

		$sCharsets = $aColumns = array();

		$sTable_parts = explode( '.', $sTable );
		$sTable = '`' . implode( '`.`', $sTable_parts ) . '`';
		$oResults = $this->getResults( "SHOW FULL COLUMNS FROM $sTable" );
		if ( ! $oResults )
		{
			Service\ErrorService::addError( 'db_charset_failure', 'Cannot load charset.', TRUE, TRUE );
		}

		foreach ( $oResults as $oColumn )
		{
			$aColumns[ strtolower( $oColumn->Field ) ] = $oColumn;
		}

		$this->aColMeta[ $sTablekey ] = $aColumns;
		foreach ( $aColumns as $oColumn )
		{
			if ( ! empty( $oColumn->Collation ) )
			{
				list( $sCharset ) = explode( '_', $oColumn->Collation );

				// If the current connection can't support utf8mb4 characters, let's only send 3-byte utf8 characters.
				if ( 'utf8mb4' === $sCharset && ! $this->hasCap( 'utf8mb4' ) ) {
					$sCharset = 'utf8';
				}

				$sCharsets[ strtolower( $sCharset ) ] = TRUE;
			}

			list( $sType ) = explode( '(', $oColumn->Type );

			// A binary/blob means the whole query gets treated like this.
			if ( in_array( strtoupper( $sType ), array( 'BINARY', 'VARBINARY', 'TINYBLOB', 'MEDIUMBLOB', 'BLOB', 'LONGBLOB' ) ) )
			{
				$this->aTableCharset[ $sTablekey ] = 'binary';
				return 'binary';
			}
		}

		// utf8mb3 is an alias for utf8.
		if ( isset( $sCharsets[ 'utf8mb3' ] ) )
		{
			$sCharsets[ 'utf8' ] = TRUE;
			unset( $sCharsets[ 'utf8mb3' ] );
		}

		// Check if we have more than one charset in play.
		$iCount = count( $sCharsets );
		if ( 1 === $iCount )
		{
			$sCharset = key( $sCharsets );
		}
		else if ( 0 === $iCount )
		{
			// No charsets, assume this table can store whatever.
			$sCharset = FALSE;
		}
		else
		{
			// More than one charset. Remove latin1 if present and recalculate.
			unset( $sCharsets[ 'latin1' ] );
			$iCount = count( $sCharsets );
			if ( 1 === $iCount )
			{
				// Only one charset (besides latin1).
				$sCharset = key( $sCharsets );
			}
			else if ( 2 === $iCount && isset( $sCharsets[ 'utf8' ], $sCharsets[ 'utf8mb4' ] ) )
			{
				// Two charsets, but they're utf8 and utf8mb4, use utf8.
				$sCharset = 'utf8';
			}
			else
			{
				// Two mixed character sets. ascii.
				$sCharset = 'ascii';
			}
		}

		$this->aTableCharset[ $sTablekey ] = $sCharset;
		return $sCharset;
	}

	/**
	 * Retrieves the character set for the given column.
	 *
	 * @param	string $sTable  Table name.
	 * @param	string $sColumn Column name.
	 * @return	string|FALSE|die
	 */
	protected function getColCharset( $sTable, $sColumn )
	{

		$sTablekey = strtolower( $sTable );
		$sColumnkey = strtolower( $sColumn );

		// This primes column information for us.
		if ( empty( $this->aTableCharset[ $sTablekey ] ) )
		{
			$sTable_charset = $this->getTableCharset( $sTable );
		}

		// If still no column information, return the table charset.
		if ( empty( $this->aColMeta[ $sTablekey ] ) )
		{
			return $this->aTableCharset[ $sTablekey ];
		}

		// If this column doesn't exist, return the table charset.
		if ( empty( $this->aColMeta[ $sTablekey ][ $sColumnkey ] ) )
		{
			return $this->aTableCharset[ $sTablekey ];
		}

		// Return FALSE when it's not a string column.
		if ( empty( $this->aColMeta[ $sTablekey ][ $sColumnkey ]->Collation ) )
		{
			return FALSE;
		}

		list( $sCharset ) = explode( '_', $this->aColMeta[ $sTablekey ][ $sColumnkey ]->Collation );
		return $sCharset;
	}

	/**
	 * Retrieve the maximum string length allowed in a given column.
	 * The length may either be specified as a byte length or a character length.
	 *
	 * @param	string $sTable  Table name.
	 * @param	string $sColumn Column name.
	 * @return	array|FALSE|die
	 */
	protected function getColLength( $sTable, $sColumn )
	{
		$sTablekey = strtolower( $sTable );
		$sColumnkey = strtolower( $sColumn );

		// This primes column information for us.
		if ( empty( $this->aColMeta[ $sTablekey ] ) )
		{
			$sTable_charset = $this->getTableCharset( $sTable );
		}

		if ( empty( $this->aColMeta[ $sTablekey ][ $sColumnkey ] ) )
		{
			return FALSE;
		}

		$aTypeInfo = explode( '(', $this->aColMeta[ $sTablekey ][ $sColumnkey ]->Type );

		$sType = strtolower( $aTypeInfo[0] );
		if ( ! empty( $aTypeInfo[1] ) )
		{
			$iLength = trim( $aTypeInfo[1], ')' );
		}
		else
		{
			$iLength = FALSE;
		}

		switch( $sType )
		{
			case 'char':
			case 'varchar':
				return array(
					'type'   => 'char',
					'length' => (int) $iLength,
				);
				break;
			case 'binary':
			case 'varbinary':
				return array(
					'type'   => 'byte',
					'length' => (int) $iLength,
				);
				break;
			case 'tinyblob':
			case 'tinytext':
				return array(
					'type'   => 'byte',
					'length' => 255,        // 2^8 - 1
				);
				break;
			case 'blob':
			case 'text':
				return array(
					'type'   => 'byte',
					'length' => 65535,      // 2^16 - 1
				);
				break;
			case 'mediumblob':
			case 'mediumtext':
				return array(
					'type'   => 'byte',
					'length' => 16777215,   // 2^24 - 1
				);
				break;
			case 'longblob':
			case 'longtext':
				return array(
					'type'   => 'byte',
					'length' => 4294967295, // 2^32 - 1
				);
				break;
			default:
				return FALSE;
		}

		return FALSE;
	}

	/**
	 * Retrieve the name of the function that called DB.
	 *
	 * Searches up the list of functions until it reaches
	 * the one that would most logically had called this method.
	 *
	 * @return	string|array The name of the calling function
	 */
	protected function getCaller() {
		return Helper\General::debugBacktraceSummary( __CLASS__ );
	}
}