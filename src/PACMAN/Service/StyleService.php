<?php
namespace PACMAN\Service;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;

class StyleService
{
	/**
	 * Stack holder for the currently registered styles at
	 * the system
	 * 
	 * @var	array
	 */
	public $aStyles;

	/**
	 * The instance of this class
	 * 
	 * @var	__CLASS__
	 */
	public static $iInstance;

	/**
	 * Checks if there is an instance of this class
	 * available. If not we init one.
	 * 
	 * @return	__CLASS__
	 */
	public static function getInstance()
	{
		if ( NULL === self::$iInstance )
		{
			self::$iInstance = new self;
		}
		return self::$iInstance;
	}

	/**
	 * Registers a style to the template system
	 * to be loaded when it needs to be loaded
	 *
	 * @param	string $sHandle the identification of the style
	 * @param	string $sSrc the URL to the style
	 * @param	array $aDeps the depencies to other styles
	 * @param	bool $bInFooter setting if the style is loaded in the footer
	 * @return	void
	 */
	public function registerStyle( $sHandle, $sSrc, $aDeps = array() )
	{
		// add the style to the main stack to get it
		// really fast if we need it
		$this->aStyles[ $sHandle ] = array(
			'handle' => $sHandle,
			'src'    => $sSrc,
			'deps'   => $aDeps
		);
	}

	/**
	 * Get the current registered styles at the system
	 * 
	 * @return	array the current styles
	 */
	public function getStyles()
	{
		return $this->aStyles;
	}

	/**
	 * Gets the style by handle
	 * 
	 * @param	string $sHandle the identification of the style
	 * @return	array the style | FALSE if the style is not registered
	 */
	public function getStyleByHandle( $sHandle )
	{
		if ( isset( $this->aStyles[ $sHandle ] ) )
		{
			return $this->aStyles[ $sHandle ];
		}
		else
		{
			return FALSE;
		}
	}

	/**
	 * Loads all the depencies for a style
	 * 
	 * @param	array $aDeps the current depencies
	 * @param	array &$aStylesToLoad the current stack for the styles to load
	 * @return	void
	 */
	public static function loadStyleDepencies( $aDeps, &$aStylesToLoad )
	{
		// get the styleservice
		$oStyleService = Service\StyleService::getInstance();

		// load the styles
		$styles = $oStyleService->getStyles();

		// check deps
		foreach ( $aDeps as $sDep )
		{
			// if the dep exists we don't need to load it
			if ( isset( $styles[ $sDep ] ) && ! empty( $styles[ $sDep ] ) )
			{
				continue;
			}

			// the dep does not exists in the current stack
			// so we try to find it
			$aDepStyle = $oStyleService->getStyleByHandle( $sDep );
			$aStylesToLoad[ $aDepStyle[ 'handle' ] ] = $aDepStyle;

			// check again if we have deps
			if ( ! empty( $aDepStyle[ 'deps' ] ) )
			{
				self::loadStyleDepencies( $aDepStyle[ 'deps' ], $aStylesToLoad );
			}
		}
	}
}