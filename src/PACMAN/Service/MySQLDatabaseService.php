<?php
namespace PACMAN\Service;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;

class MySQLDatabaseService extends DatabaseServiceAbstract implements DatabaseServiceInterface
{
	/**
	 * The instance of this class
	 * 
	 * @var	__CLASS__
	 */
	public static $iInstance;

	/**
	 * Checks if there is an instance of this class
	 * available. If not we init one.
	 * 
	 * @return	__CLASS__
	 */
	public static function getInstance()
	{
		if ( NULL === self::$iInstance )
		{
			self::$iInstance = new self;
		}
		return self::$iInstance;
	}

	/**
	 * Connects to the database server and selects a database
	 *
	 * PHP5 style constructor for compatibility with PHP5. Does
	 * the actual setting up of the class properties and connection
	 * to the database.
	 *
	 * @param string $dbuser     MySQL database user
	 * @param string $dbpassword MySQL database password
	 * @param string $dbname     MySQL database name
	 * @param string $sDBHost     MySQL database host
	 */
	public function __construct( $sDBUser, $sDBPassword, $sDBName, $sDBHost )
	{
		register_shutdown_function( array( $this, '__destruct' ) );

		// set debug mode
		if ( PM_DEBUG && PM_DEBUG_DISPLAY )
		{
			$this->showErrors();
		}

		// set basic data
		$this->sDBUser     = $sDBUser;
		$this->sDBPassword = $sDBPassword;
		$this->sDBName     = $sDBName;
		$this->sDBHost     = $sDBHost;
		$this->sCollate    = DB_COLLATE;
		$this->sCharset    = DB_CHARSET;

		// connect
		$this->DBConnect();
	}

	/**
	 * PHP5 style destructor and will run when database object is destroyed.
	 *
	 * @return	TRUE
	 */
	public function __destruct()
	{
		return TRUE;
	}

	/**
	 * @inhertited
	 */
	public function DBConnect( $bAllowBail = TRUE )
	{
		/*
		 * Deprecated in 3.9+ when using MySQLi. No equivalent
		 * $new_link parameter exists for mysqli_* functions.
		 */
		$mClientFlags = defined( 'MYSQL_CLIENT_FLAGS' ) ? MYSQL_CLIENT_FLAGS : 0;

		// init dbh on mysqli
		$this->rDBHandle = mysqli_init();

		// mysqli_real_connect doesn't support the host param including a port or socket
		// like mysql_connect does. This duplicates how mysql_connect detects a port and/or socket file.
		$iPort = NULL;
		$sSocket = NULL;
		$sHost = $this->sDBHost;
		$mPortOrSocket = strstr( $sHost, ':' );
		if ( ! empty( $mPortOrSocket ) )
		{
			$sHost = substr( $sHost, 0, strpos( $sHost, ':' ) );
			$mPortOrSocket = substr( $mPortOrSocket, 1 );
			if ( 0 !== strpos( $mPortOrSocket, '/' ) )
			{
				$iPort = intval( $mPortOrSocket );
				$maybe_socket = strstr( $mPortOrSocket, ':' );
				if ( ! empty( $maybe_socket ) )
				{
					$sSocket = substr( $maybe_socket, 1 );
				}
			}
			else
			{
				$sSocket = $mPortOrSocket;
			}
		}

		if ( PM_DEBUG )
		{
			mysqli_real_connect( $this->rDBHandle, $sHost, $this->sDBUser, $this->sDBPassword, NULL, $iPort, $sSocket, $mClientFlags );
		}
		else
		{
			@mysqli_real_connect( $this->rDBHandle, $sHost, $this->sDBUser, $this->sDBPassword, NULL, $iPort, $sSocket, $mClientFlags );
		}

		if ( $this->rDBHandle->connect_errno )
		{
			Service\ErrorService::addError( 'no_db_connection', 'Cannot connect to database.', TRUE, TRUE );
		}

		// check if we got a connection
		if ( ! $this->rDBHandle && $bAllowBail )
		{
			Service\ErrorService::addError( 'no_db_connection', 'Cannot connect to database.', TRUE, TRUE );
		}
		else if ( $this->rDBHandle )
		{

			// okay, we connected
			$this->bHasConnected = TRUE;

			// set the charset
			$this->setCharset( $this->rDBHandle );

			// set SQL mode
			$this->setSqlMode();
			$this->select( $this->sDBName, $this->rDBHandle );

			return TRUE;
		}

		return FALSE;
	}

	/**
	 * @inhertited
	 */
	public function checkConnection( $bAllowBail = TRUE )
	{
		// try to ping the database
		if ( @mysqli_ping( $this->rDBHandle ) )
			return TRUE;

		$mErrorReporting = FALSE;

		// Disable warnings, as we don't want to see a multitude of "unable to connect" messages
		if ( PM_DEBUG )
		{
			$mErrorReporting = error_reporting();
			error_reporting( $mErrorReporting & ~E_WARNING );
		}

		for ( $iTries = 1; $iTries <= $this->iReconnectRetries; $iTries++ )
		{
			// On the last try, re-enable warnings. We want to see a single instance of the
			// "unable to connect" message on the bail() screen, if it appears.
			if ( $this->iReconnectRetries === $iTries && PM_DEBUG )
			{
				error_reporting( $mErrorReporting );
			}

			if ( $this->DBConnect( FALSE ) )
			{
				if ( $mErrorReporting )
				{
					error_reporting( $mErrorReporting );
				}
				return TRUE;
			}
			sleep( 1 );
		}

		if ( ! $bAllowBail )
		{
			return FALSE;
		}

		// We weren't able to reconnect, so we better bail.
		Service\ErrorService::addError( 'no_db_connection', 'Database is not available.', TRUE, TRUE );
	}

	/**
	 * @inhertited
	 */
	public function setCharset( $rDBHandle, $sCharset = NULL, $sCollate = NULL )
	{
		// set charset
		if ( ! isset( $sCharset ) )
		{
			$sCharset = $this->sCharset;
		}

		// set collate
		if ( ! isset( $sCollate ) )
		{
			$sCollate = $this->sCollate;
		}

		if ( $this->hasCap( 'collation' ) && ! empty( $sCharset ) )
		{
			if ( function_exists( 'mysqli_set_charset' ) && $this->hasCap( 'set_charset' ) )
			{
				mysqli_set_charset( $rDBHandle, $sCharset );
			}
			else
			{
				$sQuery = $this->prepare( 'SET NAMES %s', $sCharset );
				if ( ! empty( $sCollate ) )
				{
					$sQuery .= $this->prepare( ' COLLATE %s', $sCollate );
				}
				mysqli_query( $rDBHandle, $sQuery );
			}
		}
	}

	/**
	 * @inhertited
	 */
	public function setSqlMode( $aModes = array() )
	{
		if ( empty( $aModes ) )
		{
			$rRes = mysqli_query( $this->rDBHandle, 'SELECT @@SESSION.sql_mode' );
			if ( empty( $rRes ) )
			{
				return;
			}

			$aModesArray = mysqli_fetch_array( $rRes );
			if ( empty( $aModesArray[ 0 ] ) )
			{
				return;
			}
			$sModes = $aModesArray[ 0 ];
			if ( empty( $sModes ) )
			{
				return;
			}
			$aModes = explode( ',', $sModes );
		}
		$aModes = array_change_key_case( $aModes, CASE_UPPER );
		$sModes = implode( ',', $aModes );

		mysqli_query( $this->rDBHandle, "SET SESSION sql_mode='$sModes'" );
	}

	/**
	 * @inhertited
	 */
	public function hasCap( $sDBCap )
	{
		$sVersion = $this->getDBVersion();

		switch ( strtolower( $sDBCap ) )
		{
			case 'collation' :    // @since 2.5.0
			case 'group_concat' : // @since 2.7.0
			case 'subqueries' :   // @since 2.7.0
				return version_compare( $sVersion, '4.1', '>=' );
			case 'set_charset' :
				return version_compare( $sVersion, '5.0.7', '>=' );
			case 'utf8mb4' :      // @since 4.1.0
				// check version
				if ( version_compare( $sVersion, '5.5.3', '<' ) )
				{
					return FALSE;
				}

				// get client data
				$sClientVersion = mysqli_get_client_info();

				/*
				 * libmysql has supported utf8mb4 since 5.5.3, same as the MySQL server.
				 * mysqlnd has supported utf8mb4 since 5.0.9.
				 */
				if ( FALSE !== strpos( $sClientVersion, 'mysqlnd' ) )
				{
					$sClientVersion = preg_replace( '/^\D+([\d.]+).*/', '$1', $sClientVersion );
					return version_compare( $sClientVersion, '5.0.9', '>=' );
				}
				else
				{
					return version_compare( $sClientVersion, '5.5.3', '>=' );
				}
		}
		return FALSE;
	}

	/**
	 * @inhertited
	 */
	public function select( $sDBName, $rDBHandle = NULL )
	{
		if ( is_null( $rDBHandle ) )
		{
			$rDBHandle = $this->rDBHandle;
		}
		$rSuccess = @mysqli_select_db( $rDBHandle, $sDBName );

		if ( ! $rSuccess )
		{
			$this->bReady = FALSE;
			Service\ErrorService::addError( 'no_db_selection', 'Cannot select database.', TRUE, TRUE );
		}
	}

	/**
	 * @inhertited
	 */
	public function getDBVersion()
	{
		$sServerInfo = mysqli_get_server_info( $this->rDBHandle );
		return preg_replace( '/[^0-9.].*/', '', $sServerInfo );
	}

	/**
	 * @inhertited
	 */
	public function showErrors( $bShow = TRUE )
	{
		$bErrors = $this->bShowErrors;
		$this->bShowErrors = $bShow;
		return $bErrors;
	}

	/**
	 * @inhertited
	 */
	public function prepare( $sQuery, $aArgs )
	{
		if ( is_null( $sQuery ) )
		{
			return;
		}

		// This is not meant to be foolproof -- but it will catch obviously incorrect usage.
		if ( strpos( $sQuery, '%' ) === FALSE )
		{
			die( sprintf( 'The query argument of %s must have a placeholder.', 'DB::prepare()' ) );
		}

		$aArgs = func_get_args();
		array_shift( $aArgs );

		// If args were passed as an array (as in vsprintf), move them up
		if ( isset( $aArgs[ 0 ] ) && is_array( $aArgs[ 0 ] ) )
		{
			$aArgs = $aArgs[ 0 ];
		}

		// in case someone mistakenly already singlequoted it
		$sQuery = str_replace( "'%s'", '%s', $sQuery );
		// doublequote unquoting
		$sQuery = str_replace( '"%s"', '%s', $sQuery );
		// Force floats to be locale unaware
		$sQuery = preg_replace( '|(?<!%)%f|' , '%F', $sQuery );
		// quote the strings, avoiding escaped strings like %%s
		$sQuery = preg_replace( '|(?<!%)%s|', "'%s'", $sQuery );
		array_walk( $aArgs, array( $this, 'escapeByRef' ) );

		return @vsprintf( $sQuery, $aArgs );
	}

	/**
	 * @inhertited
	 */
	public function query( $sQuery )
	{
		/**
		 * Filter the database query.
		 *
		 * Some queries are made before the plugins have been loaded,
		 * and thus cannot be filtered with this method.
		 *
		 * @param	string $sQuery Database query.
		 */
		$sQuery = Service\HookService::applyHooks( 'query', $sQuery );

		// clear the SQL cache
		$this->flush();

		// If we're writing to the database, make sure the query will write safely.
		if ( $this->bCheckCurrentQuery && ! $this->checkAscii( $sQuery ) )
		{
			$sStrippedQuery = $this->stripInvalidTextFromQuery( $sQuery );
			// stripInvalidText_from_query() can perform queries, so we need
			// to flush again, just to make sure everything is clear.
			$this->flush();
			if ( $sStrippedQuery !== $sQuery )
			{
				$this->iInsertId = 0;
				return FALSE;
			}
		}

		$this->bCheckCurrentQuery = TRUE;
		// Keep track of the last query for debug..
		$this->aLastQuery = $sQuery;

		// Keep track of the last query for debug..
		$this->doQuery( $sQuery );

		// MySQL server has gone away, try to reconnect
		$iMySQLErrno = 0;
		if ( ! empty( $this->rDBHandle ) )
		{
			$iMySQLErrno = mysqli_errno( $this->rDBHandle );
		}

		if ( empty( $this->rDBHandle ) || 2006 == $iMySQLErrno )
		{
			if ( $this->checkConnection() )
			{
				$this->doQuery( $sQuery );
			}
			else
			{
				$this->iInsertId = 0;
				return FALSE;
			}
		}

		// If there is an error then take note of it..
		$this->sLastError = mysqli_error( $this->rDBHandle );

		if ( $this->sLastError )
		{
			// Clear iInsertId on a subsequent failed insert.
			if ( $this->iInsertId && preg_match( '/^\s*(insert|replace)\s/i', $sQuery ) )
			{
				$this->iInsertId = 0;
			}

			$this->printError();
			return FALSE;
		}

		if ( preg_match( '/^\s*(create|alter|truncate|drop)\s/i', $sQuery ) )
		{
			$mReturnVal = $this->mResult;
		}
		else if ( preg_match( '/^\s*(insert|delete|update|replace)\s/i', $sQuery ) )
		{
			$this->iRowsAffected = mysqli_affected_rows( $this->rDBHandle );
			// Take note of the iInsertId
			if ( preg_match( '/^\s*(insert|replace)\s/i', $sQuery ) )
			{
				$this->iInsertId = mysql_insert_id( $this->rDBHandle );
			}
			// Return number of rows affected
			$mReturnVal = $this->iRowsAffected;
		}
		else
		{
			$iNumRows = 0;
			while ( $oRow = @mysqli_fetch_object( $this->mResult ) )
			{
				$this->aLastResult[ $iNumRows ] = $oRow;
				$iNumRows++;
			}

			// Log number of rows the query returned
			// and return number of rows selected
			$this->iNumRows = $iNumRows;
			$mReturnVal     = $iNumRows;
		}

		return $mReturnVal;
	}

	/**
	 * @inhertited
	 */
	public function doQuery( $sQuery )
	{
		$this->mResult = @mysqli_query( $this->rDBHandle, $sQuery );
		$this->iNumQueries++;
	}

	/**
	 * @inhertited
	 */
	public function getResults( $sQuery = NULL )
	{
		// check the current query
		if ( $this->checkSafeCollation( $sQuery ) )
		{
			$this->bCheckCurrentQuery = FALSE;
		}

		// set the query
		if ( $sQuery )
		{
			$this->query( $sQuery );
		}
		else
		{
			return null;
		}

		// Return an integer-keyed array of row objects
		return $this->aLastResult;
	}

	/**
	 * @inhertited
	 */
	public function getVar( $sQuery = NULL, $iX = 0, $iY = 0 )
	{

		// check the current query
		if ( $this->checkSafeCollation( $sQuery ) )
		{
			$this->bCheckCurrentQuery = FALSE;
		}

		// set the query
		if ( $sQuery )
		{
			$this->query( $sQuery );
		}

		// Extract var out of cached results based x,y vals
		if ( ! empty( $this->aLastResult[ $iY ] ) )
		{
			$aValues = array_values( get_object_vars( $this->aLastResult[ $iY ] ) );
		}

		// If there is a value return it else return null
		return ( isset( $aValues[ $iX ] ) && $aValues[ $iX ] !== '' ) ? $aValues[ $iX ] : NULL;
	}

	/**
	 * @inhertited
	 */
	public function getRow( $sQuery = NULL, $iY = 0 )
	{

		// check the current query
		if ( $this->checkSafeCollation( $sQuery ) )
		{
			$this->bCheckCurrentQuery = FALSE;
		}


		// set the query
		if ( $sQuery )
		{
			$this->query( $sQuery );
		}
		else
		{
			return NULL;
		}

		// check the latest result
		if ( ! isset( $this->aLastResult[ $iY ] ) )
			return NULL;

		// return the var
		return $this->aLastResult[ $iY ] ? $this->aLastResult[ $iY ] : NULL;
	}

	/**
	 * @inhertited
	 */
	public function getCol( $sQuery = null , $iX = 0 )
	{

		// check the current query
		if ( $this->checkSafeCollation( $sQuery ) )
			$this->bCheckCurrentQuery = FALSE;

		// set the query
		if ( $sQuery )
		{
			$this->query( $sQuery );
		}

		// Extract the column values
		$aNewArray = array();
		for ( $i = 0, $j = count( $this->aLastResult ); $i < $j; $i++ )
		{
			$aNewArray[ $i ] = $this->getVar( null, $iX, $i );
		}

		return $aNewArray;
	}

	/**
	 * @inhertited
	 */
	public function insertReplaceHelper( $sTable, $aData, $aFormat = NULL, $sType = 'INSERT' )
	{
		// set the basic inserted ID
		$this->iInsertId = 0;

		// check the type
		if ( ! in_array( strtoupper( $sType ), array( 'REPLACE', 'INSERT' ) ) )
		{
			return FALSE;
		}

		// process the fields
		$aData = $this->processFields( $sTable, $aData, $aFormat );
		if ( FALSE === $aData )
		{
			return FALSE;
		}

		// set the formats
		$aFormats = $aValues = array();
		foreach ( $aData as $aValue )
		{
			$aFormats[] = $aValue[ 'format' ];
			$aValues[]  = $aValue[ 'value' ];
		}

		// build up the fields
		$sFields  = '`' . implode( '`, `', array_keys( $aData ) ) . '`';
		$sFormats = implode( ', ', $aFormats );

		// set the sqö
		$sSQL = "$sType INTO `$sTable` ($sFields) VALUES ($sFormats)";
		$this->bCheckCurrentQuery = FALSE;
		return $this->query( $this->prepare( $sSQL, $aValues ) );
	}

	/**
	 * @inhertited
	 */
	public function update( $sTable, $aData, $aWhere, $aFormat = NULL, $mWhereFormat = NULL )
	{
		// check the data
		if ( ! is_array( $aData ) || ! is_array( $aWhere ) )
		{
			return FALSE;
		}

		// process the data fields
		$aData = $this->processFields( $sTable, $aData, $aFormat );
		if ( FALSE === $aData )
		{
			return FALSE;
		}

		// process the conditions
		$aWhere = $this->processFields( $sTable, $aWhere, $mWhereFormat );
		if ( FALSE === $aWhere )
		{
			return FALSE;
		}

		// build up the fields and conditions
		$aFields = $aConditions = $aValues = array();
		foreach ( $aData as $sField => $aValue )
		{
			$aFields[] = "`$sField` = " . $aValue[ 'format' ];
			$aValues[] = $aValue[ 'value' ];
		}
		foreach ( $aWhere as $sField => $aValue )
		{
			$aConditions[] = "`$sField` = " . $aValue[ 'format' ];
			$aValues[] = $aValue[ 'value' ];
		}

		// build the sql
		$sFields = implode( ', ', $aFields );
		$sConditions = implode( ' AND ', $aConditions );
		$sSQL = "UPDATE `$sTable` SET $sFields WHERE $sConditions";

		$this->bCheckCurrentQuery = FALSE;
		return $this->query( $this->prepare( $sSQL, $aValues ) );
	}

	/**
	 * @inhertited
	 */
	public function delete( $sTable, $aWhere, $mWhereFormat = NULL )
	{
		// check the conditions
		if ( ! is_array( $aWhere ) )
		{
			return FALSE;
		}

		// process the condition fields
		$aWhere = $this->processFields( $sTable, $aWhere, $mWhereFormat );
		if ( FALSE === $aWhere )
		{
			return FALSE;
		}

		$aConditions = $aValues = array();
		foreach ( $aWhere as $sField => $aValue )
		{
			$aConditions[] = "`$fielsFieldd` = " . $aValue[ 'format' ];
			$aValues[] = $aValue[ 'value' ];
		}

		// build the sql
		$sSonditions = implode( ' AND ', $aConditions );
		$sql = "DELETE FROM `$sTable` WHERE $sSonditions";

		$this->bCheckCurrentQuery = FALSE;
		return $this->query( $this->prepare( $sql, $aValues ) );
	}

	/**
	 * @inhertited
	 */
	public function printError( $sString = '' )
	{
		global $EZSQL_ERROR;

		// get the error string
		if ( ! $sString )
		{
			$sString = mysqli_error( $this->rDBHandle );
		}
		$EZSQL_ERROR[] = array( 'query' => $this->aLastQuery, 'error_str' => $sString );

		if ( $this->bSuppressErrors )
			return FALSE;

		if ( $caller = $this->getCaller() )
			$sErrorStr = sprintf( 'Database error %1$s for query %2$s made by %3$s', $sString, $this->aLastQuery, $caller );
		else
			$sErrorStr = sprintf(  'Database error %1$s for query %2$s', $sString, $this->aLastQuery );

		// log the error
		error_log( $sErrorStr );
		Service\ErrorService::addError( 'db_error', $sErrorStr );

		// Are we showing errors?
		if ( ! $this->bShowErrors )
		{
			return FALSE;
		}

		// If there is an error then take note of it
		$sString   = htmlspecialchars( $sString, ENT_QUOTES );
		$sQuery = htmlspecialchars( $this->aLastQuery, ENT_QUOTES );
		printf(
			'<div id="error"><p class="dberror"><strong>%s</strong> [%s]<br /><code>%s</code></p></div>',
			'Database error:',
			$sString,
			$sQuery
		);
	}

	/**
	 * @inhertited
	 */
	public function escapeByRef( &$sString )
	{
		if ( ! is_float( $sString ) )
		{
			$string = $this->realEscape( $sString );
		}
	}

	/**
	 * @inhertited
	 */
	public function realEscape( $sString )
	{
		if ( $this->rDBHandle )
		{
			return mysqli_real_escape_string( $this->rDBHandle, $sString );
		}
		return addslashes( $sString );
	}

	/**
	 * @inhertited
	 */
	public function flush()
	{
		$this->iRowsAffected = $this->iNumRows = 0;
		$this->sLastError  = '';
		$this->aLastResult = array();
		$this->aLastQuery  = null;

		if ( $this->mResult && ! is_bool( $this->mResult ) )
			mysqli_free_result( $this->mResult );
		$this->mResult = null;

		// Sanity check before using the handle
		if ( empty( $this->rDBHandle ) || ! ( $this->rDBHandle instanceof mysqli ) )
			return;

		// Clear out any results from a multi-query
		while ( mysqli_more_results( $this->rDBHandle ) )
			mysqli_next_result( $this->rDBHandle );
	}
}