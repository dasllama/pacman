<?php
namespace PACMAN\Service;
use PACMAN\Entity as Entity;

class LogService
{
	/**
	 * Stack holder for the currently registered log entries at
	 * the system.
	 * 
	 * @var	array
	 */
	public $aLogEntries = [];

	/**
	 * The instance of this class
	 * 
	 * @var	__CLASS__
	 */
	public static $iInstance;

	/**
	 * Checks if there is an instance of this class
	 * available. If not we init one.
	 * 
	 * @return	__CLASS__
	 */
	public static function getInstance()
	{
		if ( NULL === self::$iInstance )
		{
			self::$iInstance = new self;
		}
		return self::$iInstance;
	}

	/**
	 * Outputs the log
	 *
	 * @param boolean $bToFile checks if the output goes to a file
	 * @return void
	 */
	public function outputLog( $bToFile = FALSE )
	{

	}

	/**
	 * Adds a log entry to the log system
	 * 
	 * @param string  $sCode    Main code of the log entry
	 * @param string  $sMessage Message of the registered log entry
	 *
	 * @return LogEntry[]
	 */
	public static function addLogEntry( $sCode, $sMessage )
	{
		// get the instance of the hook class
		$oLogServiceInstance = self::getInstance();

		// adbuild the entity
		$oLogEntry = new Entity\LogEntry();
		$oLogEntry->sCode = $sCode;
		$oLogEntry->sMessage = $sMessage;
		$iCurrentTime = time();
		$oLogEntry->oDate = new \DateTime( "@$iCurrentTime" );

		// add the log entry to the stack
		$oLogServiceInstance->aLogEntries[] = $oLogEntry;

		// return the log entry, maybe someone want to use it
		return $oLogEntry;
	}
}