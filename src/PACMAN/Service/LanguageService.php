<?php
namespace PACMAN\Service;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;

class LanguageService
{
	/**
	 * The instance of this class
	 * 
	 * @var	__CLASS__
	 */
	public static $iInstance;

	/**
	 * Checks if there is an instance of this class
	 * available. If not we init one.
	 * 
	 * @return	__CLASS__
	 */
	public static function getInstance()
	{
		if ( NULL === self::$iInstance )
		{
			self::$iInstance = new self;
		}
		return self::$iInstance;
	}

	/**
	 * Inits the language system, registers the basic hook
	 * to startup with everything
	 *
	 * @pm-hook	index
	 * @return	void
	 */
	public function initLanguageSystem()
	{
		// register the login function
		Service\HookService::addHook( 'init', array( $this, 'loadLocalization' ) );
	}

	/**
	 * Loads the localization files on startup to
	 * get everything we need for the translations
	 * 
	 * @return	void
	 */
	public function loadLocalization()
	{
		global $aLang;

		// get the current user locale
		$sLocale = Helper\Language::getLocale();

		// load the language files based upon this locale
		// but also load the english language as fallback
		$sLanguageDir = Helper\General::getAssetDirectory( 'lang' );

		// english language file
		$sBasicLanguageFile = $sLanguageDir . 'en_US.php';
		$aLang[ 'en_US' ] = require_once $sBasicLanguageFile;

		// language file based upon the locale
		$sLocaleLanguageFile = $sLanguageDir . $sLocale . '.php';
		if ( file_exists( $sLocaleLanguageFile ) )
		{
			$aLang[ $sLocale ] = require_once $sLocaleLanguageFile;
		}
	}
}