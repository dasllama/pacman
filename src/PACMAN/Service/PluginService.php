<?php
namespace PACMAN\Service;
use PACMAN\Plugins as Plugins;
use PACMAN\Entity as Entity;

class PluginService
{
	/**
	 * Stack holder for the currently installed plugins
	 * 
	 * @var	array
	 */
	public $aPlugins = [];

	/**
	 * Stack holder for the currently registered plugins
	 * 
	 * @var	array
	 */
	public $aRegisteredPlugins = [];



	/**
	 * The instance of this class
	 * 
	 * @var	__CLASS__
	 */
	public static $iInstance;

	/**
	 * Checks if there is an instance of this class
	 * available. If not we init one.
	 * 
	 * @return	__CLASS__
	 */
	public static function getInstance()
	{
		if ( NULL === self::$iInstance )
		{
			self::$iInstance = new self;
		}
		return self::$iInstance;
	}

	/**
	 * Gets all the plugins from the folder
	 * and requires them
	 * 
	 * @return void
	 */
	public function loadPlugins()
	{
		// set the modulefiles
		$aPluginFiles = array();

		// get the module path
		$rPluginDir = @opendir( PLUGIN_DIR );
		if ( $rPluginDir ) {
			while ( ( $sFile = readdir( $rPluginDir ) ) !== FALSE ) {

				// Skip the folders
				if ( substr( $sFile, 0, 1 ) == '.' )
					continue;

				// We only acceppt folder structures
				$aPluginFiles[ $sFile ] = PLUGIN_DIR. $sFile . '/' . $sFile . '.php';
			}
			closedir( $rPluginDir );
		}

		// we don't have modules
		if ( empty( $aPluginFiles ) )
			return;

		// walk the modules
		foreach ( $aPluginFiles as $sPluginId => $sPluginFile ) {
			// we only need to require the file
			// because the file itself contains
			// a hook to kick off at the correct
			// position
			$oPlugin = require_once $sPluginFile;

			// add the plugin to the plugin stack
			$this->aPlugins[] = $oPlugin;
		}
	}

	/**
	 * Registeres a plugin
	 * 
	 * @param  object  $oPlugin   the object of the plugin itself
	 * @param  integer $iPriority the priority in which the plugin should be loaded
	 * @return Plugin[]
	 */
	public function registerPlugin( $oPlugin, $iPriority = 10 )
	{
		// add the plugin to the plugin stack
		$this->aRegisteredPlugins[ $iPriority ][] = $oPlugin;
		return $oPlugin;
	}

	/**
	 * Gets and loads all the plugins
	 * 
	 * @return	void
	 */
	public function initPluginRegistrations()
	{
		// get and walk the plugins through the priorities
		foreach ( $this->aPlugins as $oRegisteredPlugin )
		{

			// start the engines and register the plugin
			$oPlugin = new $oRegisteredPlugin;
			$oPlugin->registerPlugin( $this );
		}
	}

	/**
	 * Inits the plugins in the correct order
	 * 
	 * @return [type] [description]
	 */
	public function initPlugins()
	{
		// first of all, sort the plugin stack by the priorities
		ksort( $this->aRegisteredPlugins );
		foreach ( $this->aRegisteredPlugins as $iPriority => $aPlugins )
		{
			foreach ( $aPlugins as $oPlugin )
			{
				// initialize the plugin by the standard function
				$oPlugin->initPlugin();
			}
		}
	}
}