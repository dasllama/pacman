<?php
namespace PACMAN\Service;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;

class UserService
{
	/**
	 * The instance of this class
	 * 
	 * @var	__CLASS__
	 */
	public static $iInstance;

	/**
	 * Checks if there is an instance of this class
	 * available. If not we init one.
	 * 
	 * @return	__CLASS__
	 */
	public static function getInstance()
	{
		if ( NULL === self::$iInstance )
		{
			self::$iInstance = new self;
		}
		return self::$iInstance;
	}

	/**
	 * Inits the user system, registers the basic hook
	 * to startup with everything
	 *
	 * @pm-hook	index
	 * @return	void
	 */
	public function initUserSystem()
	{
		// register the login function
		Service\HookService::addHook( 'post_login', array( $this, 'performLogin' ) );
		Service\HookService::addHook( 'post_logout', array( $this, 'performLogout' ) );
		Service\HookService::addHook( 'post_register', array( $this, 'performRegister' ) );
		Service\HookService::addHook( 'post_profile', array( $this, 'performProfile' ) );
		Service\HookService::addHook( 'post_confirmRegister', array( $this, 'performConfirmRegister' ) );
		Service\HookService::addHook( 'post_forgotPassword', array( $this, 'performForgotPassword' ) );
		Service\HookService::addHook( 'post_performResetPassword', array( $this, 'performResetPassword' ) );
		Service\HookService::addHook( 'post_confirmEmailChange', array( $this, 'performEmailChange' ) );
		Service\HookService::addHook( 'loginMessages', array( $this, 'loginMessages' ) );
		Service\HookService::addHook( 'registerMessages', array( $this, 'registerMessages' ) );
		Service\HookService::addHook( 'profileMessages', array( $this, 'profileMessages' ) );
		Service\HookService::addHook( 'pre_getTemplateToLoad', array( $this, 'redirectLoginToHomepage' ), 10, 2 );
		Service\HookService::addHook( 'pre_getTemplateToLoad', array( $this, 'redirectRegisterToHomepage' ), 10, 2 );
		Service\HookService::addHook( 'pre_getTemplateToLoad', array( $this, 'redirectProfileToHomepage' ), 10, 2 );
	}

	/**
	 * Checks if we need to be redirected to the homepage because
	 * the user is logged in
	 *
	 * @hook	pre_get_template_to_load
	 * @param	bool $bCheckup
	 * @param	string $sCurrentPath
	 * @return	bool
	 */
	public function redirectLoginToHomepage( $bCheckup, $sCurrentPath )
	{
		// check if we got the path
		if ( ( $sCurrentPath == '/login/' || $sCurrentPath == '/reset-password/' || $sCurrentPath == '/forgot-password/' ) && Helper\User::isUserLoggedIn() )
		{
			Helper\Template::redirect( Helper\Template::getHomeUrl() );
			return;
		}

		return FALSE;
	}

	/**
	 * Checks if we need to be redirected to the homepage because
	 * the user is logged in
	 *
	 * @hook	pre_get_template_to_load
	 * @param	bool $bCheckup
	 * @param	string $sCurrentPath
	 * @return	bool
	 */
	public function redirectRegisterToHomepage( $bCheckup, $sCurrentPath )
	{
		// check if we got the path
		if ( ( $sCurrentPath == '/register/' || $sCurrentPath == '/register-confirm/' ) && Helper\User::isUserLoggedIn() ) {
			Helper\Template::redirect( Helper\Template::getHomeUrl() );
			return;
		}

		return FALSE;
	}

	/**
	 * Checks if we need to be redirected to the homepage because
	 * the user is not logged in
	 *
	 * @hook	pre_get_template_to_load
	 * @param	bool $bCheckup
	 * @param	string $sCurrentPath
	 * @return	bool
	 */
	public function redirectProfileToHomepage( $bCheckup, $sCurrentPath ) {

		// check if we got the path
		if ( ( $sCurrentPath == '/profile/' ) && ! Helper\User::isUserLoggedIn() )
		{
			Helper\Template::redirect( Helper\Template::getHomeUrl() );
			return;
		}

		return FALSE;
	}

	/**
	 * Adds some login messages
	 *
	 * @hook	loginMessages
	 * @return	void
	 */
	public function loginMessages()
	{
		// check for error messages
		$aErrorMessages = Helper\Session::get( 'errors', 'loginMessages' );
		if ( ! empty( $aErrorMessages ) )
		{
			foreach ( $aErrorMessages as $sErrorMessage )
			{
				echo '<div class="note error"><p>' . $sErrorMessage . '</p></div>';
			}
		}

		// remove the erros, we just need them once
		Helper\Session::remove( 'errors', 'loginMessages' );
	}

	/**
	 * Adds some register messages
	 *
	 * @hook	registerMessages
	 * @return	void
	 */
	public function registerMessages()
	{
		// check for error messages
		$aErrorMessages = Helper\Session::get( 'errors', 'registerMessages' );
		if ( ! empty( $aErrorMessages ) )
		{
			echo '<div class="note error"><p><strong>Some errors occured. Please fix following:</strong></p><ul>';
			foreach ( $aErrorMessages as $sErrorMessage )
			{
				echo '<li>' . $sErrorMessage . '</li>';
			}
			echo '</ul></div>';
		}

		// remove the erros, we just need them once
		Helper\Session::remove( 'errors', 'registerMessages' );
	}

	/**
	 * Adds some profile messages
	 *
	 * @hook	profileMessages
	 * @return	void
	 */
	public function profileMessages() {

		// check for error messages
		$aErrorMessages = Helper\Session::get( 'errors', 'profileMessages' );
		if ( ! empty( $aErrorMessages ) )
		{
			echo '<div class="note error"><p><strong>Some errors occured. Please fix following:</strong></p><ul>';
			foreach ( $aErrorMessages as $sErrorMessage )
			{
				echo '<li>' . $sErrorMessage . '</li>';
			}
			echo '</ul></div>';
		}
		// remove the erros, we just need them once
		Helper\Session::remove( 'errors', 'profileMessages' );

		// check for success message
		$sSuccessMessage = Helper\Session::get( 'success', 'profileMessages' );
		if ( ! empty( $sSuccessMessage ) )
		{
			echo '<div class="note success"><p><strong>' . $sSuccessMessage . '</strong></p></div>';
		}
		// remove the erros, we just need them once
		Helper\Session::remove( 'success', 'profileMessages' );
	}


	/**
	 * Performs the login
	 * 
	 * @hook	post_login
	 * @return	void
	 */
	public function performLogin() {

		// check hacks
		if ( ! Helper\Security::verifyNonce( $_POST[ '_NonceUserLogin' ], 'userLogin' ) )
		{
			die( 'Cheater!' );
		}

		// get the redirection URL
		$sReferrerUrl = PM_URL;
		if ( isset( $_POST[ 'referrer' ] ) && ! empty( $_POST[ 'referrer' ] ) )
		{
			$sReferrerUrl = $_POST[ 'referrer' ];
		}

		// set redirect URL if we got one
		if ( isset( $_POST[ 'redirectTo' ] ) && ! empty( $_POST[ 'redirectTo' ] ) )
		{
			$sReferrerUrl = $_POST[ 'redirectTo' ];
		}

		// set the errors
		$bErrors = FALSE;

		// check if we got a user login
		if (
			! isset( $_POST[ 'userLogin' ] )
		 || ! isset( $_POST[ 'userPass' ] )
		 || empty( $_POST[ 'userLogin' ] )
		 || empty( $_POST[ 'userPass' ] )
		) $bErrors = TRUE;

		// okay, we got some data, now get the user by the login
		$oUser = Helper\User::getUser( $_POST[ 'userLogin' ], 'login' );
		if ( $bErrors == FALSE && ( empty( $oUser ) || $oUser == FALSE ) )
		{
			$bErrors = TRUE;
		}

		// check the password
		if ( $bErrors == FALSE )
		{
			$password_check = Helper\Security::checkPassword( $_POST[ 'userPass' ], $oUser->pass, $oUser->id );
		}

		if ( $bErrors == FALSE && ( empty( $password_check ) || $password_check == FALSE ) )
		{
			$bErrors = TRUE;
		}

		// check if user is approved
		if ( $oUser->status != 'approved' )
		{
			// add our error message to the session
			Helper\Session::add( 'errors', array( 'Not approved!' ), 'loginMessages' );

			// redirect with our message
			Helper\Template::redirect( $sReferrerUrl );
			exit;
		}

		// check if we got errors
		if ( $bErrors == TRUE )
		{
			// add our error message to the session
			Helper\Session::add( 'errors', array( 'Password or Username incorrect!' ), 'loginMessages' );

			// redirect with our message
			Helper\Template::redirect( $sReferrerUrl );
			exit;
		}

		// everything is cool so now add the session and the cookie
		Helper\Session::add( 'isUserLoggedIn', TRUE );
		Helper\Session::add( 'currentUserId', $oUser->id );

		// TODO add the session to the database we might need it later


		// redirect to the last referrer page
		Helper\Template::redirect( $sReferrerUrl );
	}

	/**
	 * Performs the confirm register
	 * 
	 * @hook	post_forgot_password
	 * @return	void
	 */
	function performForgotPassword()
	{
		// check hacks
		if ( ! Helper\Security::verifyNonce( $_POST[ '_NonceUserForgotPassword' ], 'userForgotPassword' ) )
		{
			die( 'Cheater!' );
		}

		// get the redirection URL
		$sReferrerUrl = PM_URL;
		if ( isset( $_POST[ 'referrer' ] ) && ! empty( $_POST[ 'referrer' ] ) )
		{
			$sReferrerUrl = $_POST[ 'referrer' ];
		}

		// set the errors
		$aErrors = array();

		// check if we got all needed data
		if ( ! isset( $_POST[ 'email' ] ) || empty( $_POST[ 'email' ] ) )
		{
			// add our error message to the session
			$aErrors[] = 'Please fill in everything!';

			// add our error message to the session
			Helper\Session::add( 'errors', $aErrors, 'loginMessages' );

			// redirect
			Helper\Template::redirect( $sReferrerUrl );
			exit;
		}

		// get a user by this email
		$oUser = Helper\User::getUser( $_POST[ 'email' ], 'email' );
		if ( empty( $oUser ) || $oUser === FALSE )
		{
			// add our error message to the session
			$aErrors[] = 'Username or E-Mail are incorrect!';

			// add our error message to the session
			Helper\Session::add( 'errors', $aErrors, 'registerMessages' );

			// also add the fields to a session to save them in the form
			Helper\Session::add( 'fields', array(
				'email' => $_POST[ 'email' ],
			), 'forgot_password_input' );

			// redirect
			Helper\Template::redirect( $sReferrerUrl );
			exit;
		}

		// build confirmation code
		$sRandomString = $oUser->email . $oUser->email . time() . mt_rand() . uniqid();
		$sConfirmationCode = md5( Helper\Security::getPasswordHash( $sRandomString ) );
		$sConfirmationLink = Helper\Template::getUrl( 'reset-password' );
		$sConfirmationLink = Helper\General::addQueryArg( 'key', $sConfirmationCode, $sConfirmationLink );
		$sConfirmationLink = urldecode( $sConfirmationLink );

		// set the new data
		$aUserData = array(
			'key'    => $sConfirmationCode,
			'status' => 'forgot_password'
		);

		// get the database
		$oDb = Helper\General::getDatabase();
		$oDb->update( 'user', $aUserData, array( 'id' => $oUser->id ) );

		// set confirmation mail
		$sMailTo = $_POST[ 'email' ];
		$sMailFrom = 'from: ' . SITE_MAIL;
		$sMailSubject = Service\HookService::applyHooks( 'forgotPasswordMailSubject', '[' . SITE_TITLE . '] Reset Password!' );
		$sMailContent = Service\HookService::applyHooks( 'forgotPasswordMailContent', sprintf( 'Hello %s!' . "\n\n" . 'You recently forgot your password at our website. To get a new one you need to verify your request by clicking following link:' . "\n\n" . '%s' . "\n\n" . 'If you think that this is a mistake or this registration is scam, please let us know by answering this mail!' . "\n\n" . 'The Team of ' . SITE_TITLE, $oUser->login, $sConfirmationLink ) );
		Helper\General::mail( $sMailTo, $sMailSubject, $sMailContent, $sMailFrom );

		// send user to a thanks for registering page
		Helper\Template::redirect( Helper\Template::getUrl( 'forgot-password-thanks' ) );
		exit;
	}

	/**
	 * Performs the reset password
	 * 
	 * @hook	post_reset_password
	 * @return	void
	 */
	public function performResetPassword()
	{
		// check hacks
		if ( ! Helper\Security::verifyNonce( $_POST[ '_NonceUserResetPassword' ], 'userResetPassword' ) )
		{
			die( 'Cheater!' );
		}

		// get the redirection URL
		$sReferrerUrl = PM_URL;
		if ( isset( $_POST[ 'referrer' ] ) && ! empty( $_POST[ 'referrer' ] ) )
		{
			$sReferrerUrl = $_POST[ 'referrer' ];
		}

		// set the errors
		$aErrors = array();

		// check if we got all needed data
		if (
			! isset( $_POST[ 'pass' ] )
		 || ! isset( $_POST[ 'pass_1' ] )
		 || empty( $_POST[ 'pass' ] )
		 || empty( $_POST[ 'pass_1' ] )
		)
		{
			// add our error message to the session
			$aErrors[] = 'Please fill in everything!';

			// add our error message to the session
			Helper\Session::add( 'errors', $aErrors, 'loginMessages' );

			// redirect
			Helper\Template::redirect( $sReferrerUrl );
			exit;
		}

		// get a user by this key
		$oUser = Helper\User::getUser( $_POST[ 'key' ], 'key' );
		if ( empty( $oUser ) || $oUser === FALSE )
		{
			// add our error message to the session
			$aErrors[] = 'There is no user with this key!';

			// add our error message to the session
			Helper\Session::add( 'errors', $aErrors, 'loginMessages' );

			// redirect
			Helper\Template::redirect( $sReferrerUrl );
			exit;
		}

		// check if the passwords match
		if ( $_POST[ 'pass' ] != $_POST[ 'pass_1' ] )
		{
			$aErrors[] = 'Password missmatch!';
		}

		// check if we got errors
		if ( $aErrors !== FALSE && ! empty( $aErrors ) )
		{
			// add our error message to the session
			Helper\Session::add( 'errors', $aErrors, 'loginMessages' );

			// redirect
			Helper\Template::redirect( $sReferrerUrl );
			exit;
		}

		// set the new data
		$aUserData = array(
			'key'    => '',
			'pass'   => Helper\Security::getPasswordHash( $_POST[ 'pass' ] ),
			'status' => 'approved'
		);

		// get the database
		$oDb = Helper\General::getDatabase();
		$oDb->update( 'user', $aUserData, array( 'id' => $oUser->id ) );

		// send user to a thanks for registering page
		Helper\Template::redirect( Helper\Template::getUrl( 'reset-password-finished' ) );
		exit;
	}

	/**
	 * Performs the logout
	 * 
	 * @hook	post_logout
	 * @return	void
	 */
	public function performLogout() {

		// get the redirection URL
		$sReferrerUrl = PM_URL;
		if ( isset( $_POST[ 'referrer' ] ) && ! empty( $_POST[ 'referrer' ] ) )
		{
			$sReferrerUrl = $_POST[ 'referrer' ];
		}

		// everything is cool so now add the session and the cookie
		Helper\Session::remove( 'isUserLoggedIn' );
		Helper\Session::remove( 'currentUserId' );

		// TODO remove the session from the database


		// redirect to the last referrer page
		Helper\Template::redirect( $sReferrerUrl );
	}

	/**
	 * Performs the register
	 * 
	 * @hook	post_register
	 * @return	void
	 */
	public function performRegister()
	{
		// check hacks
		if ( ! Helper\Security::verifyNonce( $_POST[ '_NonceUserRegister' ], 'userRegister' ) )
		{
			die( 'Cheater!' );
		}

		// get the redirection URL
		$sReferrerUrl = PM_URL;
		if ( isset( $_POST[ 'referrer' ] ) && ! empty( $_POST[ 'referrer' ] ) )
		{
			$sReferrerUrl = $_POST[ 'referrer' ];
		}

		// set the errors
		$aErrors = array();

		// check if we got all needed data
		if (
			! isset( $_POST[ 'login' ] )
		 || ! isset( $_POST[ 'email' ] )
		 || ! isset( $_POST[ 'pass' ] )
		 || ! isset( $_POST[ 'pass_1' ] )
		 || empty( $_POST[ 'login' ] )
		 || empty( $_POST[ 'email' ] )
		 || empty( $_POST[ 'pass' ] )
		 || empty( $_POST[ 'pass_1' ] )
		)  {
			// add our error message to the session
			$aErrors[] = 'Please fill in everything!';

			// add our error message to the session
			Helper\Session::add( 'errors', $aErrors, 'registerMessages' );

			// redirect
			Helper\Template::redirect( $sReferrerUrl );
			exit;
		}

		// validate email
		if ( ! filter_var( $_POST[ 'email' ], FILTER_VALIDATE_EMAIL ) )
		{
			$aErrors[] = 'E-Mail is not valid!';
		}

		// check if we got a user with this email
		$oCheckUserByMail = Helper\User::getUser( $_POST[ 'email' ], 'email' );
		if ( ! empty( $oCheckUserByMail ) )
		{
			$aErrors[] = 'E-Mail is taken!';
		}

		// check if we got a user with this email
		$oCheckUserByLogin = Helper\User::getUser( $_POST[ 'login' ], 'login' );
		if ( ! empty( $oCheckUserByLogin ) )
		{
			$aErrors[] = 'Username exists!';
		}

		// check if the passwords match
		if ( $_POST[ 'pass' ] != $_POST[ 'pass_1' ] )
		{
			$aErrors[] = 'Passwords missmatch!';
		}

		// check if we got errors
		if ( $aErrors !== FALSE && ! empty( $aErrors ) )
		{
			// add our error message to the session
			Helper\Session::add( 'errors', $aErrors, 'registerMessages' );

			// also add the fields to a session to save them in the form
			Helper\Session::add( 'fields', array(
				'login' => $_POST[ 'login' ],
				'email' => $_POST[ 'email' ],
			), 'registerFieldInput' );

			// redirect
			Helper\Template::redirect( $sReferrerUrl );
			exit;
		}

		// build confirmation code
		$sRandomString = $_POST[ 'login' ] . $_POST[ 'email' ] . time() . mt_rand() . uniqid();
		$sConfirmationCode = md5( Helper\Security::getPasswordHash( $sRandomString ) );
		$sConfirmationLink = Helper\Template::getUrl( 'register-confirm' );
		$sConfirmationLink = Helper\General::addQueryArg( 'key', $sConfirmationCode, $sConfirmationLink );
		$sConfirmationLink = urldecode( $sConfirmationLink );

		// set up the user data
		$aUserData = array(
			'login'      => $_POST[ 'login' ],
			'email'      => $_POST[ 'email' ],
			'pass'       => Helper\Security::getPasswordHash( $_POST[ 'pass' ] ),
			'status'     => 'awaiting_confirmation',
			'key'        => $sConfirmationCode,
			'registered' => date( 'Y-m-d H:i:s' ),
		);

		// insert the user data
		$oDb = Helper\General::getDatabase();
		$oDb->insert( 'user', $aUserData );

		// set confirmation mail
		$sMailTo = $_POST[ 'email' ];
		$sMailFrom = 'from: ' . SITE_MAIL;
		$sMailSubject = Service\HookService::applyHooks( 'register_mail_subject', '[' . SITE_TITLE . '] Your account is awaiting confirmation!' );
		$sMailContent = Service\HookService::applyHooks( 'register_mail_content', sprintf( 'Hello %s!' . "\n\n" . 'You recently singed up at our website. To complete the registration you need to confirm your registration. To do so, please click following link:' . "\n\n" . '%s' . "\n\n" . 'If you think that this is a mistake or this registration is scam, please let us know by answering this mail!' . "\n\n" . 'The Team of ' . SITE_TITLE, $_POST[ 'login' ], $sConfirmationLink ) );
		Helper\General::mail( $sMailTo, $sMailSubject, $sMailContent, $sMailFrom  );

		// send user to a thanks for registering page
		Helper\Template::redirect( Helper\Template::getUrl( 'register-thanks' ) );
		exit;
	}

	/**
	 * Performs the confirm register
	 * 
	 * @hook	post_confirmRegister
	 * @return	void
	 */
	public function performConfirmRegister()
	{
		// check hacks
		if ( ! Helper\Security::verifyNonce( $_POST[ '_NonceUserConfirmRegister' ], 'userConfirmRegister' ) )
		{
			die( 'Cheater!' );
		}

		// get the redirection URL
		$sReferrerUrl = PM_URL;
		if ( isset( $_POST[ 'referrer' ] ) && ! empty( $_POST[ 'referrer' ] ) )
		{
			$sReferrerUrl = $_POST[ 'referrer' ];
		}

		// set the errors
		$aErrors = array();

		// check if we got all needed data
		if (
			! isset( $_POST[ 'key' ] )
		 || empty( $_POST[ 'key' ] )
		)  {
			// add our error message to the session
			$aErrors[] = 'Please fill in everything!';

			// add our error message to the session
			Helper\Session::add( 'errors', $aErrors, 'registerMessages' );

			// redirect
			Helper\Template::redirect( $sReferrerUrl );
			exit;
		}

		// get a user by this key
		$oUser = Helper\User::getUser( $_POST[ 'key' ], 'key' );
		if ( empty( $oUser ) || $oUser === FALSE )
		{
			// add our error message to the session
			$aErrors[] = 'There is no user with this key!';

			// add our error message to the session
			Helper\Session::add( 'errors', $aErrors, 'registerMessages' );

			// also add the fields to a session to save them in the form
			Helper\Session::add( 'fields', array(
				'key' => $_POST[ 'key' ],
			), 'registerConfirmInput' );

			// redirect
			Helper\Template::redirect( $sReferrerUrl );
			exit;
		}

		// set the new data
		$aUserData = array(
			'key'    => '',
			'status' => 'approved'
		);

		// get the database
		$oDb = Helper\General::getDatabase();
		$oDb->update( 'user', $aUserData, array( 'id' => $oUser->id ) );

		// send user to a thanks for registering page
		Helper\Template::redirect( Helper\Template::getUrl( 'register-complete' ) );
		exit;
	}

	/**
	 * Performs the profile change
	 * 
	 * @hook	post_profile
	 * @return	void
	 */
	public function performProfile()
	{
		// check hacks
		if ( ! Helper\Security::verifyNonce( $_POST[ '_NonceUserProfile' ], 'userProfile' ) )
		{
			die( 'Cheater!' );
		}

		// get the redirection URL
		$sReferrerUrl = PM_URL;
		if ( isset( $_POST[ 'referrer' ] ) && ! empty( $_POST[ 'referrer' ] ) )
		{
			$sReferrerUrl = $_POST[ 'referrer' ];
		}

		// set the errors
		$aErrors = array();

		// check if we got all needed data
		if (
			! isset( $_POST[ 'email' ] )
		 || empty( $_POST[ 'email' ] )
		)  {
			// add our error message to the session
			$aErrors[] = 'Please fill in everything!';

			// add our error message to the session
			Helper\Session::add( 'errors', $aErrors, 'profileMessages' );

			// redirect
			Helper\Template::redirect( $sReferrerUrl );
			exit;
		}

		// validate email
		if ( ! filter_var( $_POST[ 'email' ], FILTER_VALIDATE_EMAIL ) )
		{
			$aErrors[] = 'E-Mail is not valid!';
		}

		// get the user data
		$oUser = Helper\User::getUser( Helper\User::getCurrentUserId() );
		if ( $oUser->email != $_POST[ 'email' ] )
		{
			// check if we got a user with this email
			$oCheckUserByMail = Helper\User::getUser( $_POST[ 'email' ], 'email' );
			if ( ! empty( $oCheckUserByMail ) )
			{
				$aErrors[] = 'There is an user with this email registered.';
			}
		}


		// check if we need to change the passwords
		if ( ! empty( $_POST[ 'pass' ] ) && $_POST[ 'pass' ] != $_POST[ 'pass_1' ] )
		{
			$aErrors[] = 'Password missmatch!';
		}

		// check if we got errors
		if ( $aErrors !== FALSE && ! empty( $aErrors ) )
		{
			// add our error message to the session
			Helper\Session::add( 'errors', $aErrors, 'profileMessages' );

			// redirect
			Helper\Template::redirect( $sReferrerUrl );
			exit;
		}

		// get the database
		$oDb = Helper\General::getDatabase();

		// change the password if needed
		if ( ! empty( $_POST[ 'pass' ] ) )
		{
			$aUserData = array( 'pass' => Helper\Security::getPasswordHash( $_POST[ 'pass' ] ) );
			$oDb->update( 'user', $aUserData, array( 'id' => $oUser->id ) );
		}

		// check if we need to change the email
		if ( $oUser->email != $_POST[ 'email' ] ) {

			// build confirmation code
			$sRandomString = $oUser->login . $oUser->email . time() . mt_rand() . uniqid();
			$sConfirmationCode = md5( Helper\Security::getPasswordHash( $sRandomString ) );
			$sConfirmationLink = Helper\Template::getUrl( 'profile-change-email' );
			$sConfirmationLink = Helper\General::addQueryArg( 'key', $sConfirmationCode, $sConfirmationLink );
			$sConfirmationLink = urldecode( $sConfirmationLink );

			// update the key
			$aUserData = array( 'key' => $sConfirmationCode );
			$oDb->update( 'user', $aUserData, array( 'id' => $oUser->id ) );

			// update the new email as user data
			$oChangedEmail = $oDb->getRow( $oDb->prepare( 'SELECT * FROM user_data WHERE `user_id` = %d AND `key` = %s', Helper\User::getCurrentUserId(), 'new_email' ) );
			if ( empty( $oChangedEmail ) )
			{
				$oDb->insert( 'user_data', array(
					'user_id' => $oUser->id,
					'key'     => 'new_email',
					'value'   => $_POST[ 'email' ]
				) );
			}
			else
			{
				$oDb->update( 'user_data', array(
					'value'   => $_POST[ 'email' ]
				), array(
					'user_id' => $oUser->id,
					'key'     => 'new_email',
				) );
			}

			// setup the confirmation mail
			$sMailTo = $oUser->email;
			$sMailFrom = 'from: ' . SITE_MAIL;
			$sMailSubject = Service\HookService::applyHooks( 'change_email_mail_subject', '[' . SITE_TITLE . '] Request to change E-Mail!' );
			$sMailContent = Service\HookService::applyHooks( 'change_email_mail_content', sprintf( 'Hello %s!' . "\n\n" . 'You recently updated your profile at our website. To change your E-Mailaddress you need to verify your request by clicking following link:' . "\n\n" . '%s' . "\n\n" . 'If you think that this is a mistake or this registration is scam, please let us know by answering this mail!' . "\n\n" . 'The Team of ' . SITE_TITLE, $oUser->login, $sConfirmationLink ) );
			Helper\General::mail( $sMailTo, $sMailSubject, $sMailContent, $sMailFrom  );
		}

		// send user to a thanks for change page
		Helper\Session::add( 'success', 'Profile saved!', 'profileMessages' );
		Helper\Template::redirect( Helper\Template::getUrl( 'profile' ) );
		exit;
	}

	/**
	 * Performs the email change
	 * 
	 * @hook	post_confirmEmailChange
	 * @return	void
	 */
	function performEmailChange()
	{
		// check hacks
		if ( ! Helper\Security::verifyNonce( $_POST[ '_NonceUserConfirmEmailChange' ], 'userConfirmEmailChange' ) )
		{
			die( 'Cheater!' );
		}

		// check if the user is logged in
		if ( ! Helper\User::isUserLoggedIn() )
		{
			die( 'Cheater!' );;
		}

		// get the redirection URL
		$sReferrerUrl = PM_URL;
		if ( isset( $_POST[ 'referrer' ] ) && ! empty( $_POST[ 'referrer' ] ) )
		{
			$sReferrerUrl = $_POST[ 'referrer' ];
		}

		// set the errors
		$aErrors = array();

		// check if we got all needed data
		if (
			! isset( $_POST[ 'key' ] )
		 || empty( $_POST[ 'key' ] )
		)  {
			// add our error message to the session
			$aErrors[] = 'Please fill in everything!';

			// add our error message to the session
			Helper\Session::add( 'errors', $aErrors, 'profileMessages' );

			// redirect
			Helper\Template::redirect( $sReferrerUrl );
			exit;
		}

		// get a user by this key
		$oUser = Helper\User::getUser( $_POST[ 'key' ], 'key' );
		if ( empty( $oUser ) || $oUser === FALSE ) {
			// add our error message to the session
			$aErrors[] = 'There is no user with this key!';

			// add our error message to the session
			Helper\Session::add( 'errors', $aErrors, 'profileMessages' );

			// also add the fields to a session to save them in the form
			Helper\Session::add( 'fields', array(
				'key' => $_POST[ 'key' ],
			), 'emailChangeConfirmInput' );

			// redirect
			Helper\Template::redirect( $sReferrerUrl );
			exit;
		}

		// check if we got a changed email
		$oDb = Helper\General::getDatabase();
		$oChangedEmail = $oDb->getRow( $oDb->prepare( 'SELECT * FROM user_data WHERE `user_id` = %d AND `key` = %s', get_current_user_id(), 'new_email' ) );

		// set the new email
		$aUserData = array(
			'key'   => '',
			'email' => $oChangedEmail->value
		);

		// update the user
		$oDb->update( 'user', $aUserData, array( 'id' => $oUser->id ) );

		// remove the email from the user data
		$oDb->delete( 'user_data', array(
			'user_id' => $oUser->id,
			'key'     => 'new_email',
		) );

		// send user to a thanks for registering page
		Helper\Session::add( 'success', 'E-Mail changed!', 'profileMessages' );
		Helper\Template::redirect( Helper\Template::getUrl( 'profile' ) );
		exit;
	}
}