<?php
namespace PACMAN\Service;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;

class TemplateService
{
	/**
	 * The instance of this class
	 * 
	 * @var	__CLASS__
	 */
	public static $iInstance;

	/**
	 * Checks if there is an instance of this class
	 * available. If not we init one.
	 * 
	 * @return	__CLASS__
	 */
	public static function getInstance()
	{
		if ( NULL === self::$iInstance )
		{
			self::$iInstance = new self;
		}
		return self::$iInstance;
	}

	/**
	 * Inits the template system, registers the basic hook
	 * to startup with everything
	 *
	 * @pm-hook	index
	 * @return	void
	 */
	public function initTemplateSystem()
	{
		// register the basic stylesheet of the template
		Service\HookService::addHook( 'index', array( $this, 'loadStylesheet' ) );

		// register the basic template system
		Service\HookService::addHook( 'index', array( $this, 'templateLoader' ) );
	}

	/**
	 * Loads the template file based upon the path
	 * and checks if the content exists. If not we
	 * load the 404 page
	 * 
	 * @return	void
	 */
	public function templateLoader()
	{
		// get the current path and check what we
		// should do, maybe it is empty or not
		$sCurrentPath = Helper\Template::getCurrentPath();

		// gets the template
		$sTemplateToLoad = $this->getTemplateToLoad( $sCurrentPath );

		// set up the path to the template and load it
		$sPathToTemplateFolder = Helper\General::getAssetDirectory( 'templates' );
		$sTemplateFile = $sPathToTemplateFolder . $sTemplateToLoad;

		require_once $sTemplateFile;
	}

	/**
	 * Gets the needed template based on the current path
	 * 
	 * @param	string $sCurrentPath
	 * @return	string the template to load
	 */
	public function getTemplateToLoad( $sCurrentPath = '/' )
	{
		/**
		 * Filter the template to load before the template is required
		 *
		 * Passing a non-null value to the filter will short-circuit
		 * checking the DB for the charset, returning that value instead.
		 *
		 * @param	boolean the check if this filter is used
		 * @param	string $sCurrentPath The character set to use. Default null.
		 */
		$mPreTemplateToLoad = Service\HookService::applyHooks( 'pre_getTemplateToLoad', FALSE, $sCurrentPath );
		if ( FALSE !== $mPreTemplateToLoad )
		{
			return $mPreTemplateToLoad;
		}

		if ( empty( $sCurrentPath ) )
		{
			// if the current path is empty we need to load the
			// index.php file which is obviously the homepage
			$sTemplateToLoad = 'index.php';
		}
		else
		{
			// load the content based upon the path
			$sContent = Service\ContentService::getContent( $sCurrentPath );

			// check if we got content and if not we need to show
			// the 404 template. Also add the 404 header
			if ( empty( $sContent ) || $sContent === FALSE )
			{
				header( $_SERVER[ 'SERVER_PROTOCOL' ] . ' 404 Not Found' );
				$sTemplateToLoad = '404.php';
			}
			else
			{
				if ( $sContent->template != '' )
				{
					$sTemplateToLoad = $sContent->template . '.php';
				}
				else
				{
					$sTemplateToLoad = $sContent->type . '.php';
				}
			}
		}

		return Service\HookService::applyHooks( 'templateToLoad', $sTemplateToLoad );
	}

	/**
	 * Loads the basic stylesheet file
	 *
	 * @pm-hook	init
	 * @return	void
	 */
	public function loadStylesheet()
	{
		$oStyleService = Service\StyleService::getInstance();
		$oStyleService->registerStyle( 'style', Helper\General::getAssetDirectoryUrl( 'css' ) . 'style.css' );
	}
}