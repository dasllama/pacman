<?php
namespace PACMAN\Service;

interface DatabaseServiceInterface
{
	/**
	 * Connect to and select database.
	 *
	 * If $bAllowBail is FALSE, the lack of database connection will need
	 * to be handled manually.
	 *
	 * @param	bool $bAllowBail Optional. Allows the function to bail. Default TRUE.
	 * @return	bool True with a successful connection, FALSE on failure.
	 */
	public function DBConnect( $bAllowBail = TRUE );

	/**
	 * Check that the connection to the database is still up. If not, try to reconnect.
	 *
	 * If this function is unable to reconnect, it will forcibly die, or if after the
	 * the template_redirect hook has been fired, return FALSE instead.
	 *
	 * If $bAllowBail is FALSE, the lack of database connection will need
	 * to be handled manually.
	 * 
	 * @param	bool $bAllowBail Optional. Allows the function to bail. Default TRUE.
	 * @return	bool|void True if the connection is up.
	 */
	public function checkConnection( $bAllowBail = TRUE );

	/**
	 * Sets the connection's character set.
	 *
	 * @param	resource $rDBHandle     The resource given by mysqli_connect
	 * @param	string   $sCharset Optional. The character set. Default NULL.
	 * @param	string   $sCollate Optional. The collation. Default NULL.
	 * @return	void
	 */
	public function setCharset( $rDBHandle, $sCharset = NULL, $sCollate = NULL );

	/**
	 * Change the current SQL mode, and ensure its G!Galarm compatibility.
	 *
	 * If no modes are passed, it will ensure the currentserver
	 * modes are compatible.
	 *
	 * @param	array $aModes Optional. A list of SQL modes to set.
	 */
	public function setSqlMode( $aModes = array() );

	/**
	 * Determine if a database supports a particular feature.
	 *
	 * @param	string $sDBCap The feature to check for. Accepts 'collation',
	 *                         'group_concat', 'subqueries', 'set_charset',
	 *                         or 'utf8mb4'.
	 * @return	int|FALSE Whether the database feature is supported, FALSE otherwise.
	 */
	public function hasCap( $sDBCap );

	/**
	 * Selects a database using the current database connection.
	 *
	 * The database name will be changed based on the current database
	 * connection. On failure, the execution will bail and display an DB error.
	 *
	 * @param	string        $sDB database name
	 * @param	resource|null $rDBHandle Optional link identifier.
	 */
	public function select( $sDB, $rDBHandle = NULL );

	/**
	 * gets the database version number.
	 *
	 * @return	null|string Null on failure, version number on success.
	 */
	public function getDBVersion();

	/**
	 * Enables showing of database errors.
	 *
	 * This function should be used only to enable showing of errors. However,
	 * this function can be used to enable and disable showing of database
	 * errors.
	 *
	 * @param	bool $bShow Whether to show or hide errors
	 * @return	bool Old value for showing errors.
	 */
	public function showErrors( $bShow = TRUE );

	/**
	 * Prepares a SQL query for safe execution. Uses sprintf()-like syntax.
	 *
	 * The following directives can be used in the query format string:
	 *   %d (integer)
	 *   %f (float)
	 *   %s (string)
	 *   %% (literal percentage sign - no argument needed)
	 *
	 * All of %d, %f, and %s are to be left unquoted in the query string and they need an argument passed for them.
	 * Literals (%) as parts of the query must be properly written as %%.
	 *
	 * This function only supports a small subset of the sprintf syntax; it only supports %d (integer), %f (float), and %s (string).
	 * Does not support sign, padding, alignment, width or precision specifiers.
	 * Does not support argument numbering/swapping.
	 *
	 * May be called like {@link http://php.net/sprintf sprintf()} or like {@link http://php.net/vsprintf vsprintf()}.
	 *
	 * Both %d and %s should be left unquoted in the query string.
	 *
	 *     DB::prepare( "SELECT * FROM `table` WHERE `column` = %s AND `field` = %d", 'foo', 1337 )
	 *     DB::prepare( "SELECT DATE_FORMAT(`field`, '%%c') FROM `table` WHERE `column` = %s", 'foo' );
	 *
	 * @link	http://php.net/sprintf Description of syntax.
	 *
	 * @param	string      $sQuery    Query statement with sprintf()-like placeholders
	 * @param	array|mixed $aArgs     The array of variables to substitute into the query's placeholders if being called like
	 *                                {@link http://php.net/vsprintf vsprintf()}, or the first variable to substitute into the query's placeholders if
	 *                                being called like {@link http://php.net/sprintf sprintf()}.
	 * @param	mixed       $aArgs,... further variables to substitute into the query's placeholders if being called like
	 *                                {@link http://php.net/sprintf sprintf()}.
	 * @return	string|void Sanitized query string, if there is a query to prepare.
	 */
	public function prepare( $sQuery, $aArgs );

	/**
	 * Perform adatabase query, using current database connection.
	 *
	 * @param	string $sQuery Database query
	 * @return	int|FALSE Number of rows affected/selected or FALSE on error
	 */
	public function query( $sQuery );

	/**
	 * Internal function to perform the mysql_query() call.
	 *
	 * @see		DB::query()
	 * @param	string $sQuery The query to run.
	 * @return	void
	 */
	public function doQuery( $sQuery );

	/**
	 * Retrieve an entire SQL result set from the database (i.e., many rows)
	 *
	 * Executes a SQL query and returns the entire SQL result.
	 *
	 * @param	string $sQuery  SQL query.
	 * @return	object|null Database query results
	 */
	public function getResults( $sQuery = NULL );

	/**
	 * Retrieve one variable from the database.
	 *
	 * Executes a SQL query and returns the value from the SQL result.
	 * If the SQL result contains more than one column and/or more than one row, this function returns the value in the column and row specified.
	 * If $sQuery is null, this function returns the value in the specified column and row from the previous SQL result.
	 *
	 * @param	string|null $sQuery Optional. SQL query. Defaults to null, use the result from the previous query.
	 * @param	int         $iX     Optional. Column of value to return. Indexed from 0.
	 * @param	int         $iY     Optional. Row of value to return. Indexed from 0.
	 * @return	string|null Database query result (as string), or null on failure
	 */
	public function getVar( $sQuery = NULL, $iX = 0, $iY = 0 );

	/**
	 * Retrieve one row from the database.
	 * Executes a SQL query and returns the row from the SQL result.
	 * 
	 * @param	string|null $sQuery  SQL query.
	 * @param	int         $iY      Optional. Row to return. Indexed from 0.
	 * @return	array|object|null|void Database query result in format specified by $output or null on failure
	 */
	public function getRow( $sQuery = NULL, $iY = 0 );

	/**
	 * Retrieve one column from the database.
	 *
	 * Executes a SQL query and returns the column from the SQL result.
	 * If the SQL result contains more than one column, this function returns the column specified.
	 * If $sQuery is null, this function returns the specified column from the previous SQL result.
	 *
	 * @param	string|null $sQuery Optional. SQL query. Defaults to previous query.
	 * @param	int         $iX     Optional. Column to return. Indexed from 0.
	 * @return	array Database query result. Array indexed from 0 by SQL result row number.
	 */
	public function getCol( $sQuery = NULL , $iX = 0 );

	/**
	 * Helper function for insert and replace.
	 *
	 * Runs an insert or replace query based on $type argument.
	 *
	 * @param	string       $sTable  Table name
	 * @param	array        $aData   Data to insert (in column => value pairs).
	 *                                Both $aData columns and $aData values should be "raw" (neither should be SQL escaped).
	 * @param	array|string $mFormat Optional. An array of formats to be mapped to each of the value in $aData.
	 *                                If string, that format will be used for all of the values in $aData.
	 *                                A format is one of '%d', '%f', '%s' (integer, float, string).
	 *                                If omitted, all values in $aData will be treated as strings unless otherwise specified in DB::$field_types.
	 * @param	string $sType         Optional. What type of operation is this? INSERT or REPLACE. Defaults to INSERT.
	 * @return	int|FALSE The number of rows affected, or FALSE on error.
	 */
	public function insertReplaceHelper( $sTable, $aData, $mFormat = NULL, $sType = 'INSERT' );

		/**
	 * Update a row in the table
	 *
	 *     DB::update( 'table', array( 'column' => 'foo', 'field' => 'bar' ), array( 'ID' => 1 ) )
	 *     DB::update( 'table', array( 'column' => 'foo', 'field' => 1337 ), array( 'ID' => 1 ), array( '%s', '%d' ), array( '%d' ) )
	 *
	 * @param	string       $sTable        Table name
	 * @param	array        $aData         Data to update (in column => value pairs).
	 *                                     Both $aData columns and $aData values should be "raw" (neither should be SQL escaped).
	 * @param	array        $aWhere        A named array of WHERE clauses (in column => value pairs).
	 *                                     Multiple clauses will be joined with ANDs.
	 *                                     Both $aWhere columns and $aWhere values should be "raw".
	 * @param	array|string $mFormat       Optional. An array of formats to be mapped to each of the values in $aData.
	 *                                     If string, that format will be used for all of the values in $aData.
	 *                                     A format is one of '%d', '%f', '%s' (integer, float, string).
	 *                                     If omitted, all values in $aData will be treated as strings unless otherwise specified in DB::$field_types.
	 * @param	array|string $mWhereFormat Optional. An array of formats to be mapped to each of the values in $aWhere.
	 *                                     If string, that format will be used for all of the items in $aWhere.
	 *                                     A format is one of '%d', '%f', '%s' (integer, float, string).
	 *                                     If omitted, all values in $aWhere will be treated as strings.
	 * @return	int|FALSE The number of rows updated, or FALSE on error.
	 */
	public function update( $sTable, $aData, $aWhere, $mFormat = NULL, $mWhereFormat = NULL );

	/**
	 * Delete a row in the table
	 *
	 *     DB::delete( 'table', array( 'ID' => 1 ) )
	 *     DB::delete( 'table', array( 'ID' => 1 ), array( '%d' ) )
	 *
	 * @param	string       $sTable        Table name
	 * @param	array        $aWhere        A named array of WHERE clauses (in column => value pairs).
	 *                                     Multiple clauses will be joined with ANDs.
	 *                                     Both $aWhere columns and $aWhere values should be "raw".
	 * @param	array|string $mWhereFormat Optional. An array of formats to be mapped to each of the values in $aWhere.
	 *                                     If string, that format will be used for all of the items in $aWhere.
	 *                                     A format is one of '%d', '%f', '%s' (integer, float, string).
	 *                                     If omitted, all values in $aWhere will be treated as strings unless otherwise specified in DB::$field_types.
	 * @return	int|FALSE The number of rows updated, or FALSE on error.
	 */
	public function delete( $sTable, $aWhere, $mWhereFormat = NULL );

	/**
	 * Print SQL/DB error.
	 *
	 * @global	array $EZSQL_ERROR Stores error information of query and error string
	 *
	 * @param	string $sString The error to display
	 * @return	FALSE|void False if the showing of errors is disabled.
	 */
	public function printError( $sString = '' );

	/**
	 * Escapes content by reference for insertion
	 * into the database, for security
	 *
	 * @param	string $sString to escape
	 */
	public function escapeByRef( &$sString );

	/**
	 * Real escape, using mysqli_real_escape_string()
	 *
	 * @param	string $sString to escape
	 * @return	string escaped
	 */
	public function realEscape( $sString );

	/**
	 * Kill cached query results.
	 *
	 * @return	void
	 */
	public function flush();
}