<?php
namespace PACMAN\Service;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;

class AccessService
{
	/**
	 * Defines the registered roles and cababilities
	 * 
	 * @var array
	 */
	public $aRoles = array();

	/**
	 * The instance of this class
	 * 
	 * @var	__CLASS__
	 */
	public static $iInstance;

	/**
	 * Checks if there is an instance of this class
	 * available. If not we init one.
	 * 
	 * @return	__CLASS__
	 */
	public static function getInstance()
	{
		if ( NULL === self::$iInstance )
		{
			self::$iInstance = new self;
		}
		return self::$iInstance;
	}

	/**
	 * Sets the basic roles and cababilities
	 *
	 * The standard list of capabilities
	 * 		read - can read the website, usually not used
	 * 		admin_login - user is able to login in the backend
	 *
	 * @return	void
	 */
	public function setRolesAndCapabilities()
	{
		$this->aRoles = array(
			'administrator' => array(
				'read',
				'admin_login',
			),
			'user'  => array(
				'read'
			),
		);
		$this->aRoles = Service\HookService::applyHooks( 'setRolesAndCapabilities', $this->aRoles );
	}

	/**
	 * Gets the current roles and capabilities
	 * 
	 * @return	array
	 */
	public function getRolesAndCapabilities()
	{
		return $this->aRoles;
	}
}