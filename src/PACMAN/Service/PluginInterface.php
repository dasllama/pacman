<?php
namespace PACMAN\Service;

/**
 * Interface for a plugin
 * 
 * @author llama
 */
interface PluginInterface
{
	/**
	 * Initializes the plugin, registeres all its hooks
	 * and performes the action which are needed in order
	 * to get a plugin to work
	 *
	 * @param PluginService $oPluginService
	 * 
	 * @return	void
	 */
	public function initPlugin();

	/**
	 * Registers the plugin to the plugin service so
	 * we can use priorities and stuff
	 *
	 * @param	PluginService $oPluginService
	 * 
	 * @return 	void
	 */
	public function registerPlugin( PluginService $oPluginService );
}