<?php
/**
 * Feature Name: English (US)
 * Descriptions: 
 * Version:      1.0
 * Author:       Llama
 * Author URI:   http://dasllama.github.io
 * Licence:      GPLv3
 */

$aStrings[ 'standard' ] = array(
	'test' => 'Test',
	'cheating'                     => 'Cheating, huh?',
	'no_db_connection'             => 'No batabase connection.',
	'db_gone'                      => 'Cannot reconnect to database.',
	'no_database_selection'        => 'Cannot select database.',
	'query_argument_placeholer'    => 'The query argument of %s must have a placeholder.',
	'strip_invalid_text_failure'   => 'Cannot strip invalid text.',
	'db_charset_failure'           => 'Database charset failure.',
	'homepage_title'               => 'Home',
	'404_title'                    => 'Content not found',
	'content_type_login'           => 'Login',
	'nologin'                      => 'Wrong Username or Password',
	'wrong_email'                  => 'E-Mail is not valid!',
	'missing_data'                 => 'Please fill in everything!',
	'no_user_with_this_key'        => 'There is no user with this key!',
	'no_user_with_this_email'      => 'There is no user with this email!',
	'errors_occured'               => 'Some errors occured. Please fix following:',
	'user_with_email_exists'       => 'There is an user with this email registered.',
	'user_with_login_exists'       => 'There is an user with this login registered.',
	'passwords_missmatch'          => 'Password missmatch!',
	'confirmation_mail_subject'    => '[' . SITE_TITLE . '] Your account is awaiting confirmation!',
	'confirmation_mail_content'    => 'Hello %s!' . "\n\n" . 'You recently singed up at our website. To complete the registration you need to confirm your registration. To do so, please click following link:' . "\n\n" . '%s' . "\n\n" . 'If you think that this is a mistake or this registration is scam, please let us know by answering this mail!' . "\n\n" . 'The Team of ' . SITE_TITLE,
	'forgot_password_mail_subject' => '[' . SITE_TITLE . '] Reset Password!',
	'forgot_password_mail_content' => 'Hello %s!' . "\n\n" . 'You recently forgot your password at our website. To get a new one you need to verify your request by clicking following link:' . "\n\n" . '%s' . "\n\n" . 'If you think that this is a mistake or this registration is scam, please let us know by answering this mail!' . "\n\n" . 'The Team of ' . SITE_TITLE,
	'change_email_mail_subject' => '[' . SITE_TITLE . '] Request to change E-Mail!',
	'change_email_mail_content' => 'Hello %s!' . "\n\n" . 'You recently updated your profile at our website. To change your E-Mailaddress you need to verify your request by clicking following link:' . "\n\n" . '%s' . "\n\n" . 'If you think that this is a mistake or this registration is scam, please let us know by answering this mail!' . "\n\n" . 'The Team of ' . SITE_TITLE,
);
return $aStrings;