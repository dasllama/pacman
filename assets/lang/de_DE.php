<?php
/**
 * Feature Name: Deutsch
 * Descriptions: 
 * Version:      1.0
 * Author:       Llama
 * Author URI:   http://dasllama.github.io
 * Licence:      GPLv3
 */

$aStrings[ 'standard' ] = array(
	'foo' => 'Foo (de)',
	'bar' => 'Bar (de)'
);
return $aStrings;