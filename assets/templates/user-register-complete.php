<?php
namespace PACMAN\Templates;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;
?>

<?php Helper\Template::loadTemplate( 'header' ); ?>

Register complete! You now can login!

<?php echo Helper\User::getLoginForm( array( 'redirectTo' => Helper\Template::getUrl( 'login' ) ) ); ?>

<?php Helper\Template::loadTemplate( 'footer' ); ?>