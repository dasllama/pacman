<?php
namespace PACMAN\Templates;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;
?>
<!Doctype html>
<!--[if IE 7]><html class="no-js ie ie7"  lang="<?php echo str_replace( '_', '-', Helper\Language::getLocale() ); ?>"><![endif]-->
<!--[if IE 8]><html class="no-js ie ie8"  lang="<?php echo str_replace( '_', '-', Helper\Language::getLocale() ); ?>"><![endif]-->
<!--[if !IE]><!--><html class="no-js" lang="<?php echo str_replace( '_', '-', Helper\Language::getLocale() ); ?>"><!--<![endif]-->
<head profile="http://gmpg.org/xfn/11">
	<?php thePageTitle(); ?>
	<meta name="application-name" content="Llama">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<?php Helper\Template::loadHead(); ?>
</head>
<body>

<div class="wrapper">
	<header id="main">
		<h1><a href="<?php echo Helper\Template::getHomeUrl(); ?>">Projekt X</a></h1>
		<nav role="navigation">
			<ul>
				<?php if ( ! Helper\User::isUserLoggedIn() ) : ?>
					<li><a href="<?php echo Helper\Template::getUrl( 'login' ); ?>">Login</a></li>
					<li><a href="<?php echo Helper\Template::getUrl( 'register' ); ?>">Register</a></li>
				<?php else : ?>
					<li><a href="<?php echo Helper\Template::getUrl( 'profile' ); ?>">Profile</a></li>
					<li><a href="<?php echo Helper\User::getLogoutUrl(); ?>">Logout</a></li>
				<?php endif; ?>
			</ul>
		</nav>
	</header>
	<main id="main">