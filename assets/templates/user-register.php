<?php
namespace PACMAN\Templates;
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;
?>

<?php Helper\Template::loadTemplate( 'header' ); ?>

<?php echo Helper\User::getRegisterForm(); ?>

<?php Helper\Template::loadTemplate( 'footer' ); ?>