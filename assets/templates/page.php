<?php
namespace PACMAN\Templates;
use PACMAN\Helper as Helper;
use PACMAN\Helper\Language as Language;
?>

<?php Helper\Template::loadTemplate( 'header' ); ?>

<h2><?php echo Helper\Content::getTitle(); ?></h2>
<?php echo Helper\Content::getContent(); ?>

<?php Helper\Template::loadTemplate( 'footer' ); ?>