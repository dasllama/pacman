<?php

// get the basic services
use PACMAN\Service as Service;
use PACMAN\Helper as Helper;
// get the init
require_once 'init.php';

// get the action
if ( isset( $_POST[ 'action' ] ) )
{
	$sAction = $_POST[ 'action' ];
}
else if ( isset( $_GET[ 'action' ] ) )
{
	$sAction = $_GET[ 'action' ];
}
else if ( isset( $_REQUEST[ 'action' ] ) )
{
	$sAction = $_REQUEST[ 'action' ];
}

// kick off PACMAN with the first hook
Service\HookService::execHook( 'post_' . $sAction );